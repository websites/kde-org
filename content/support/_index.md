---
title: "Help & Support for KDE Software"
layout: support
aliases:
- ./help
menu:
  main:
    name: Help
    weight: 4
columns:
- title: Documentation
  text: "These resources can help you learn how to get the most out of KDE software."
  rows:
    - title: General Information
      text: "General information about KDE applications and products from a user perspective can be found here:"
      url: "https://userbase.kde.org"
    - title: For Users
      text: "KDE apps include their own official documentation, available offline. You can access it using the \"[application name] Handbook\" menu item in KDE apps, or by pressing the F1 key. You can also find it online here:"
      url: "https://docs.kde.org"
    - title: For System Administrators
      text: "System administrators deploying KDE software in their organization can find relevant information here:"
      url: "https://develop.kde.org/docs/administration"

- title: Community Support
  text: "These resources can help if you're experiencing an issue with KDE software. If you've purchased a product running KDE software, first contact the company that sold the product, as they may offer a warranty and professional support. Otherwise, read on to learn how to get support from the KDE community. Keep in mind that any such support is provided with no warranty, by passionate volunteers. Please be kind and read through [tips on asking questions](https://userbase.kde.org/Asking_Questions) to learn how to ask for help in a way that maximizes the chances of a positive outcome."
  rows:
    - title: Discussion Forum
      text: "For general KDE questions, or to get help with an issue if you're not sure whether it might be a bug in the software, ask here first:"
      url: "https://discuss.kde.org"
    - title: Chat Channels
      text: "Real-time support is also available in [KDE's Matrix rooms](https://go.kde.org/matrix/#/#kde:kde.org). You can see the [list of rooms here](https://community.kde.org/Matrix#Rooms)."
      url: "https://community.kde.org/Matrix"
    - title: KDE in your country
      text: "Country-specific websites containing information about KDE software in a language other than English can be found here:"
      url: "/support/international"

- title: Account Issues
  text: "If you're having a problem logging into one of your KDE accounts or creating a new one, submit a [sysadmin ticket](https://go.kde.org/u/systickets) about it. If you can't log into the account needed to see that page, [send an email to KDE's sysadmins](mailto:sysadmin@kde.org)."

- title: Security issues and bug reports
  text: "These resources can help you report issues and defects in KDE software."
  rows:
    - title: Security Issues
      text: "If you believe you've discovered a security issue in KDE software, report it here. You can also find an up-to-date list of security advisories to be aware of."
      url: "/info/security/"
    - title: Software Bugs
      text: "If you believe you've discovered a bug in KDE software, report it here. If you're not sure that the issue is in fact a bug (maybe it could be a local setup or configuration issue, or an issue with an update from your operating system), it's best to [discuss it on the forum](https://discuss.kde.org) first, and others can help you determine if it's a bug or not."
      url: "https://community.kde.org/Get_Involved/Issue_Reporting"
---

This page lists different ways to get help and support when using KDE software.
