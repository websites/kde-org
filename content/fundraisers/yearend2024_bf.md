---
title: Blue Friday is Back!
layout: yearend2024_bf
scssFiles:
- /scss/yearend2024.scss
jsFiles:
- /js/yearend2024.js
draft: false
---

<a name="top"></a>Blue Friday celebrates supporting good and humane causes over the unbridled consumerism of Black Friday.

When you fund your favorite free software project, you can give what you want, and get back all you want. Your generosity also
spreads to others, and helps to keep our software free for those with less means. You're not going to find a better deal.

*"I don't care about Black Friday. [Take me directly to the adoptions!](#adopt)"*

### Three ways you can support us:

1. **Make a one-time donation** — use the donation box on this page and donate whatever you want. Every Euro counts and will help sustain our contributors through another year.
2. **Set up periodic donation** — use the donation box and select "Monthly" or "Annually" to set up a recurring donation. You can also become a [Supporting Member](https://kde.org/fundraisers/plasma6member/)!
3. **Adopt an App!** ([See below](#adopt))

Besides, Black Friday is not only bad for your wallet, but [it's also terrible for the environment](https://greenly.earth/en-us/leaf-media/data-stories/how-does-black-friday-affect-the-environment). [Not so for Blue Friday](https://eco.kde.org).

Not convinced? Then you may want to check out our…

## 7 Reasons why you should avoid Black Friday and support Free Software projects instead

Number 5 will blow your mi-- Actually, know what? They're all pretty compelling, so here goes:

<hr />

<div class="container">
    <div id="scam_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>1. Many Bargains are Scams</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/misleading.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Inflating prices just prior to Black Friday and then bringing them down again on the day; or
                simply using misleading labeling are just two of the ways retailers try to make you believe you
                are getting a deal.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('scam')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="scam_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">1. Free Software is All about Transparency</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/glasses.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                <a href="#top">Use your money to support Free Software</a> and you will find that nobody can
                hide anything in this world. The rules are clear from the outset, the licenses are written in a way humans can
                understand, and there is no room for ambiguity. Download your programs, usually for free, and
                they become yours to do with as you please. Even the code is visible.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('scam')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="need_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>2. You're Tempted to Buy Stuff You Don't Need</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/impulse.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Retailers try to trick you into impulse purchasing, and you end up with stuff you didn't want
                and probably won't use once the novelty wears off.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('need')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="need_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">2. You Get What You Want</span></h4>
            </div>
            <div class="row">
                <div class="clearfix">
                    <img src="/fundraisers/yearend2024/gift.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                    <p>
                    <a href="#top">Donate some spare charge to the projects you love</a> and you can help them live on for years to come.
                    </p>
                    <p>
                    <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('need')">Go back</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="co2_blk"> 
        <div class="row">
            <div class="col align-items-center">
                <h4>3. Black Friday Worsens Climate Change</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/truck-smoke.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                The transport of products from warehouses to shops or to your home generates thousands of tons of
                CO<sub>2</sub> that pollutes our atmosphere and contributes to global warming.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('co2')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="co2_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">3. Free Software Curbs Climate Change</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/flower.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Free Software tends to be very lean, as it avoids bloat by not including ads, frivolous features, or spyware. The
                harmful emissions from downloading and running free apps are minimal. <a href="#top">When you support developers</a>, they
                can further optimize the software you use and make it even more environmentally friendly.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('co2')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="costs_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>4. Black Friday "Deals" Often Disguise Hidden Costs</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/pickpocket.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                What looks like a deal today might end up costing you dearly, as many products come with
                anti-features designed to make you pay later. A phone will require monthly payment for cloud
                access if you need to backup your data; a smart TV may advertise channels that, once you
                get home and try to watch them, you find are blocked behind a paywall; a car manufacturer will often
                require a subscription for creature comforts such as heated seats.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('costs')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="costs_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">4. Free Software is an Open Book</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/book.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Free Software communities put tools and applications in your hands that otherwise would cost
                hundreds, sometimes thousands of Euros. They do so at very low or zero cost. Communities like KDE
                are non-profits and have no commercial interest and, <a href="#top">although they do require funding</a> for their survival,
                they have no desire to charge you for anything.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('costs')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="ewaste_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>5. Black Friday Increases E-waste…</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/ewaste.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                During this time of year, manufacturers and retailers encourage you to trash older devices that are
                still perfectly serviceable. These devices end up in landfills, polluting our waterways, oceans, and
                air — their components to never be recycled.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('ewaste')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="ewaste_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">5. Free Software Makes Recycling Possible</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/lab.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Free Software communities keep their products fast and frugal. This allows them to work
                even on older devices, in computer labs, schools, or as servers. This lengthens your hardware's useful life and
                decreases e-waste. <a href="#top">Your donation</a> helps us carry out recycling projects and keep
                toxic elements out of the environment.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('ewaste')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="waste_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>6. …And Also Regular Waste</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/waste.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Online shopping is particularly bad, as it generates tons of difficult-to-recycle waste in the
                form of plastic and styrofoam packaging.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('waste')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="waste_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">6. Free Software Preserves the Environment</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/landscape.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                All Free Software is downloaded from the Internet. <a href="#top">Keep us funded</a> so you can enjoy awesome apps and make the
                world a cleaner place.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('waste')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

<div class="container">
    <div id="own_blk">
        <div class="row">
            <div class="col align-items-center">
                <h4>7. If You Don't Own the Software, You Don't Own The Product</h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/coffee.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                Especially insidious in devices that can go online (often hawked on Black Friday), the software
                can be disabled at any given moment for that cool "connected" coffee maker, fridge, eBook reader,
                or TV. Your device can become non-functional because the company went bust, or because they
                decided to stop supporting your model of the product (to force you to buy a newer model), or
                even because they changed the contract you didn't even read, let alone agreed to. Once the
                software is gone, all you're left with is a pile of junk.
                </p>
                <p>
                <button type="button" class="btn btn-success btn-lg blackfriday-box" onclick="blk4blu('own')">Click to see a better option</button>
                </p>
            </div>
        </div>
    </div>
    <div id="own_blu" style="display: none">
        <div class="row">
            <div class="col align-items-center">
                <h4><span class="blue">7. Your Stuff is Yours</span></h4>
            </div>
        </div>
        <div class="row">
            <div class="clearfix">
                <img src="/fundraisers/yearend2024/nice_coffee.gif" class="col-md-5 float-md-end mb-3 ms-md-3" />
                <p>
                <a href="https://community.kde.org/KDE/Vision">KDE envisions a world where everyone has control over their digital life</a>. We go to great lengths to
                ensure apps do not go online unless absolutely necessary, and, if an app does not need to go online,
                that functionality will not even be built into it. Controlling programs remotely is not a thing for us.
                <a href="#top">Help us keep it that way</a>.
                </p>
                <p>
                <button type="button" class="btn btn-dark btn-lg blackfriday-box" onclick="blk4blu('own')">Go back</button>
                </p>
            </div>
        </div>
    </div>
</div>

<hr />

## <a name="adopt"></a>Adopt an App Instead!

Adopt one of KDE's apps and we can share with the whole world how awesome you are and how much you're doing to support us. Adopting a KDE app is easy:

1. [Make a donation](#top) of €50 or more to our fundraiser.
2. Choose an app you'd like to adopt from the list below. Each app can be adopted by up to 5 adopters. We will add more apps to the list as the adoption slots fill up.
3. During the donation process, use the comment section to write which app you want to adopt and the name you'd like to be known as. This can be your name, nickname, or your social media handle<sup>[*](#anot01)</sup><sup>[†](#anot02)</sup>…
4. Your name will appear on the app's [apps.kde.org](https://apps.kde.org) as a _Supporter_ from the moment of the donation until KDE Gear 25.04 is released in April of next year. We will also give you a shout-out on social media.

### Adoptable Apps

|App|Adopters|
|---|---|
|[Alligator](https://apps.kde.org/alligator)|🟦⬜⬜⬜⬜|
|[ISOImageWriter](https://apps.kde.org/isoimagewriter)|🟦⬜⬜⬜⬜|
|[PartitionManager](https://apps.kde.org/partitionmanager)|🟦🟦⬜⬜⬜|
|[SkanPage](https://apps.kde.org/skanpage)|🟦⬜⬜⬜⬜|
|[Plasma System Monitor](https://apps.kde.org/plasma-systemmonitor/)|🟦⬜⬜⬜⬜|
|[Arianna](https://apps.kde.org/arianna/)|🟦🟦⬜⬜⬜|
|[AudioTube](https://apps.kde.org/audiotube/)|🟦⬜⬜⬜⬜|
|[KRuler](https://apps.kde.org/kruler/)|🟦🟦⬜⬜⬜|
|[LabPlot](https://apps.kde.org/labplot/)|🟦🟦⬜⬜⬜|
|[Kasts](https://apps.kde.org/kasts/)|🟦🟦🟦🟦⬜|
|[Ruqola](https://apps.kde.org/ruqola/)|🟦⬜⬜⬜⬜|
|[Elisa](https://apps.kde.org/elisa/)|🟦🟦🟦⬜⬜|
|[Konqueror](https://apps.kde.org/konqueror/)|🟦🟦🟦🟦⬜|
|[KolourPaint](https://apps.kde.org/kolourpaint/)|🟦🟦⬜⬜⬜|
|[KRFB](https://apps.kde.org/krfb/)|🟦🟦⬜⬜⬜|

### Already Adopted Apps

|App|Adopters|
|---|---|
|~~[GCompris](https://apps.kde.org/gcompris/)~~|🟦🟦🟦🟦🟦|
|~~[Discover](https://apps.kde.org/discover/)~~|🟦🟦🟦🟦🟦|
|~~[KRDC](https://apps.kde.org/krdc/)~~|🟦🟦🟦🟦🟦|
|~~[KWrite](https://apps.kde.org/kwrite/)~~|🟦🟦🟦🟦🟦|
|~~[Gwenview](https://apps.kde.org/gwenview/)~~|🟦🟦🟦🟦🟦|
|~~[Tokodon](https://apps.kde.org/tokodon/)~~|🟦🟦🟦🟦🟦|
|~~[Ark](https://apps.kde.org/ark/)~~|🟦🟦🟦🟦🟦|
|~~[NeoChat](https://apps.kde.org/neochat/)~~|🟦🟦🟦🟦🟦|
|~~[Merkuro](https://apps.kde.org/merkuro/)~~|🟦🟦🟦🟦🟦|
|~~[Itinerary](https://apps.kde.org/itinerary/)~~|🟦🟦🟦🟦🟦|
|~~[KDE Connect](https://apps.kde.org/kdeconnect/)~~|🟦🟦🟦🟦🟦|
|~~[Dolphin](https://apps.kde.org/dolphin/)~~|🟦🟦🟦🟦🟦|
|~~[Kate](https://apps.kde.org/kate/)~~|🟦🟦🟦🟦🟦|
|~~[Okular](https://apps.kde.org/okular/)~~|🟦🟦🟦🟦🟦|
|~~[Konsole](https://apps.kde.org/konsole/)~~|🟦🟦🟦🟦🟦|
|~~[Filelight](https://apps.kde.org/filelight/)~~|🟦🟦🟦🟦🟦|
|~~[Spectacle](https://apps.kde.org/spectacle/)~~|🟦🟦🟦🟦🟦|
|~~[Kontact](https://apps.kde.org/kontact/)~~|🟦🟦🟦🟦🟦|

---
<p class="annotation"><a name="anot01"></a><sup>*</sup> We reserve the right to refuse applicants with offensive names or nicknames, or whose social media accounts contain objectionable material. See KDE's <a href="https://kde.org/code-of-conduct/">Code of Conduct</a>.</p>

<p class="annotation"><a name="anot02"></a><sup>†</sup> This reward is open to individuals only. If you represent a company that would like to support us, we would be happy to <a href="https://ev.kde.org/getinvolved/supporting-members/">welcome you as a patron</a>.</p>

<div>
{{< yearend2024/progress >}}
</div>

<p class="annotation"><sup>*</sup>One more stretch goal coming soon.</p>

## Why donate

KDE is funded mainly by you: our friends, users, and supporters. Thanks to your donations, we can deliver the best free and open software that respects your privacy and gives you control over your devices and digital life. Your donations also help us keep tech corporations in check.

By getting most of our support from people like you, we can preserve our independence and focus on developing what you need. We will never be pressured by any company to include the anti-features used in proprietary software that abuse your trust to maximize owners' profits.

And that's how we like it. We really appreciate your support. Today, we want to show the world how you help keep Free Software projects alive and ethical!


