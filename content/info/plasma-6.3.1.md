---
version: "6.3.1"
title: "KDE Plasma 6.3.1, feature Release"
type: info/plasma6
signer: Jonathan Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
draft: false
---

This is a feature release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 6.3.1 announcement](/announcements/plasma/6/6.3.1).
