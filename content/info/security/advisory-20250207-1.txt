KDE Project Security Advisory
=============================

Title:           Ark: maliciously crafted archive can install files outside the extraction directory.
Risk Rating:     Important
CVE:             CVE-2024-57966
Versions:        ark <= 24.12.0
Date:            7 February 2025

Overview
========

A security issue exists in Ark where a maliciously crafted archive containing
file paths beginning with "/" allows files to be extracted to locations
outside the intended directory.

Impact
======

Users can unwillingly install files like a modified /home/youruser/.bashrc, or a
malicious script placed in the /home/youruser/.config/autostart folder.

Solution
========

The issue is resolved in Ark version 24.12.0, which now ignores the starting "/" during extraction,
preventing files from being placed outside the target directory.

Credits
=======

Thanks to Fabian Vogt for finding and submitting a patch for this issue and thanks to
Elvis Angelaccio for the review of the patch.
