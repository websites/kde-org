<ul>

<!-- ARK LINUX -->
<li><a href="http://www.arklinux.org/">Ark Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/ArkLinux/">i586 RPMs</a></li>
    <li>
      A new version of Ark Linux will be released imminently which includes KDE 3.5.4.
    </li>
  </ul>
  <p />
</li>

<!-- ARCH LINUX -->
<li><a href="http://www.archlinux.org/">Arch Linux</a>
  :
  <ul type="disc">
    <li>Packages: <a href="ftp://ftp.archlinux.org/extra/os/i686">ftp://ftp.archlinux.org/extra/os/i686</a></li>
    <li>
      To install: pacman -S kde
    </li>
  </ul>
  <p />
</li>

<!-- KUBUNTU -->
<li><a href="http://www.kubuntu.org/">Kubuntu</a>
    <ul type="disc">
      <li>
         Kubuntu 6.06 LTS (dapper): <a href="http://kubuntu.org/announcements/kde-354.php">Intel i386, AMD64 and 
PowerPC</a>
      </li>
    </ul>
  <p />
</li>

<!-- Pardus -->
<li>
 <a href="http://www.pardus.org.tr/">Pardus</a>
    <ul type="disc">
      <li>
         1.0: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/Pardus/">Intel i386</a>
      </li>
    </ul>
  <p />
</li>

<!-- kde-redhat -->
<li><a href="http://kde-redhat.sourceforge.net/">KDE RedHat (unofficial) Packages</a>:
(<a href="http://apt.kde-redhat.org/apt/kde-redhat/kde-org.txt">README</a>)
<ul type="disc">

 <li>All distributions: 
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/all/">(noarch,SRPMS)</a></li>

 <li>Fedora Core 4:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/i386/">(i386)</a>
	<!-- not available (yet) -->
 	<!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>
 <li>Fedora Core 5:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/5/i386/">(i386)</a>
        <!-- not available (yet) -->
        <!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>
 <li>Fedora Core 6:
	<a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/kde.repo">(kde.repo)</a>,
        <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/6/i386/">(i386)</a>
        <!-- not available (yet) -->
        <!-- <a href="http://apt.kde-redhat.org/apt/kde-redhat/fedora/4/x86_64/">(x64_64)</a> -->
 </li>

</ul>
 <p />
</li>

<!-- SLACKWARE LINUX  -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution)
 (<a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/contrib/Slackware/10.2/README">README</a>)
   :
   <ul type="disc">
     <li>
        <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/contrib/Slackware/noarch/">Language packages</a>
     </li>
     <li>
        10.1: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/contrib/Slackware/10.1/">Intel i486</a>
     </li>
     <li>
        10.2: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/contrib/Slackware/10.2/">Intel i486</a>
     </li>
   </ul>
  <p />
</li>

<!-- SLAMD64 -->
<li>
 <a href="http://www.slamd64.com/">Slamd64</a>
    <ul type="disc">
      <li>
         slamd64-current: <a href="http://download.kde.org/binarydownload.html?url=/stable/3.5.4/Slamd64/">AMD64</a>
      </li>
    </ul>
  <p />
</li>


<!-- SUSE LINUX -->
<li>
  <a href="http://www.opensuse.org">SuSE Linux</a>
  The SUSE KDE packages will be publicly developed in the openSUSE project from 
now on. This means that the packages are no longer available via the 
supplementary tree from ftp.suse.com, but via the <a 
href="http://en.opensuse.org/Build_Service">Build Service</a> repositories 
from openSUSE. Please read <a 
href="http://en.opensuse.org/Build_Service/User">this page</a> for 
installation instructions. 
  <ul type="disc">
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.1/">SUSE 
Linux 10.1 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_10.0/">SUSE 
Linux 10.0 YUM repository</a> (64bit and 32bit)
    </li>
    <li>
        <a 
href="http://software.opensuse.org/download/repositories/KDE:/KDE3/SUSE_Linux_9.3/">SUSE 
Linux 9.3 YUM repository</a> (64bit and 32bit)
    </li>
  </ul>
  <p /> 
</li>


</ul>
