---
title: KDE For Digital Sovereignty
description: Regaining Independence from Tech Giants Since 1996
sassFiles:
- /scss/for/digital-sovereignty.scss
---

{{< feature-container img="/for/digital/community.jpg" img-class="none" reverse="true" >}}

## Developed by humans, for humans

KDE is a global community of volunteers who create free and open source
software for everyone to use and enjoy. Unlike other software creators, we are
a non-profit organization that does not seek to make money, but to make the
world a better place. We envision a world where monopolistic corporations and
repressive governments don't have so much power over our digital lives. We
actually care about privacy and digital well-being, and we fight to protect
user rights. [KDE e.V.](https://ev.kde.org), the association behind KDE, is a
non-profit based in Germany. 

{{< /feature-container >}}

{{< feature-container img="/for/digital/code.png" img-class="none" >}}

## Open Source and Transparency

KDE software is entirely open-source, meaning its code is publicly accessible
and can be examined, modified, or redistributed by anyone. This transparency
helps users and researchers verify that the software is free of
malicious features sometimes found in proprietary software, such as
spyware, backdoors, and advertising.

We also host our entire infrastructure ourselves, relying only on open-source
software.

[Browse our codebase on GitLab!](https://invent.kde.org/)

{{< /feature-container >}}

{{< feature-container img-class="none" img="/for/digital/applications.svg" reverse="true" >}}

## Independence from Big Tech Ecosystems

KDE software runs on multiple operating systems (like Linux, BSD, Android, and even
Windows), giving users the freedom to choose a platform that aligns with their
needs and values. This flexibility is particularly valuable for digital sovereignty, as
it enables governments and organizations to avoid being locked into specific
operating systems, hardware, or cloud platforms that could lead to dependency
on foreign companies. KDE also supports open standards and protocols as much as
possible.

By avoiding lock-in with large tech companies, KDE users retain the ability to
migrate systems and data as they see fit, which is a key factor in maintaining
control over digital assets.

{{< /feature-container >}}

{{< feature-container img-class="none" img="/for/digital/ukranian.png" img-caption="System Settings in Ukrainian" >}}

## Strong Internationalization and Localization

KDE has extensive language support and can be easily localized for different
countries and regions, making it accessible and practical for governments and
organizations worldwide. This is particularly relevant for digital sovereignty
initiatives in non-English-speaking regions, as it allows them to adopt KDE
software adapted to their linguistic and cultural needs.

Janayugom is a Malayalam (a South-Indian language spoken in Kerala) daily
newspaper who switched to KDE and Scribus for their support for unicode and
Malayalam fonts and stopped relying on old version of Adobe software. [Read the full
story.](https://web.archive.org/web/20191111143006/https://poddery.com/posts/4691002)

In 2000 the Norwegian Language Council encouraged people to
[boycott Microsoft and to use KDE instead](https://dot.kde.org/2000/10/20/norwegian-language-movement-says-boycott-microsoft-use-kde-instead),
as they could contribute translations for both written standards of Norwegian language Bokmål and
Nynorsk, which wasn't the case for Windows at that time.

{{< /feature-container >}}

{{< feature-container img-class="none" video="/for/resources/observatoir.mp4" reverse="true" >}}

## Adoption in Research Institutions

KDE’s software is increasingly being adopted in research institutions and
universities around the world. Academic environments benefit from KDE's
open-source, customizable, and privacy-focused software, which supports a wide
range of educational, research, and administrative activities.

KDE’s highly customizable software allows universities and research
institutions to tailor the environment to their specific needs. This allows them to configure
desktops for specialized lab setups, adjust privacy
settings, and install necessary software packages for different research
disciplines.

[Read more!](https://kde.org/for/scientists/)

{{< /feature-container >}}

{{< feature-container img-class="none" img="https://apps.kde.org/screenshots/kleopatra/certificates.png" >}}

## Adoption in the Public Sector

Large KDE Plasma deployment like
[Lliurex](https://interoperable-europe.ec.europa.eu/collection/open-source-observatory-osor/news/120000-lliurex-desktops)
in Valencia (Spain) and [LiMux](https://en.wikipedia.org/wiki/LiMux) in Munich
(Germany) show the viability of KDE software in public administrations and
education.

Kleopatra is another prime example of KDE software adoption in the public
sector. Kleopatra is bundled as part of [GnuPG
VS-Desktop](https://gnupg.com/gnupg-vs-desktop.html)® which is compliant for
use with EU and NATO **RESTRICTED** material and the German VS-NfD. It is
used by many companies and administrations in the European Union to securely
communicate with others.

[Read more!](https://apps.kde.org/kleopatra/)

{{< /feature-container >}}

{{< feature-container img-class="none" img="/for/digital/mbition_header.png" reverse="true" >}}

## Adoption in the Private Sector

[MBition](https://mbition.io/) designs and implements the infotainment system
for future generations of Mercedes-Benz cars. MBition uses KDE's technology and
know-how for its products to avoid being reliant on a third party automotive system.

[Read more about MBition collaboration with KDE](https://ev.kde.org/2024/05/07/mbition-becomes-kde-patron/)

[Valve’s Steam Deck](https://store.steampowered.com/steamdeck/), a handheld
gaming console that runs on a custom Linux-based OS (SteamOS), uses KDE Plasma
as its desktop environment. KDE Plasma’s lightweight and flexible nature makes
it an excellent choice for the Steam Deck.

{{< /feature-container >}}

{{< container class="text-small" >}}

## This technology is available to you

Whether you run a business, a government department, or are an individual in
need of a secure, privacy-preserving solution, KDE has something for you.

All of KDE's technology is available as free software to businesses,
institutions, and individuals. You can [deploy it yourself](/distributions),
[purchase devices](/hardware) with KDE software pre-installed, [get help from
experts](https://ev.kde.org/consultants/), or choose from
[powerful frameworks](https://develop.kde.org/products/frameworks/) for your
own development.

There are no licenses, no subscriptions, and no requirements to use KDE
services. However, if you find our technology useful, you can support KDE by
[becoming a patron](https://ev.kde.org/getinvolved/supporting-members/) or
[donating to our community](https://kde.org/donate/) and help ensure that KDE can
continue to provide free, ethical software that everyone can use.

{{< /container >}}
