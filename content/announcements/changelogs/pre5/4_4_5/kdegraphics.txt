------------------------------------------------------------------------
r1134263 | aacid | 2010-06-04 07:44:23 +1200 (Fri, 04 Jun 2010) | 4 lines

Backport r1134257 | aacid | 2010-06-03 20:38:29 +0100 (Thu, 03 Jun 2010) | 3 lines

Accessing pdfdoc needs to hold the lock

------------------------------------------------------------------------
r1135732 | scripty | 2010-06-08 14:33:01 +1200 (Tue, 08 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1136892 | mmrozowski | 2010-06-11 14:34:55 +1200 (Fri, 11 Jun 2010) | 3 lines

Backport r1098734, initialize jpeg_transform_info fields

CCBUG: 218107
------------------------------------------------------------------------
r1136895 | mmrozowski | 2010-06-11 14:41:51 +1200 (Fri, 11 Jun 2010) | 1 line

Setting those is no longer necessary (a'ka failed backport)
------------------------------------------------------------------------
r1137955 | cfeck | 2010-06-15 07:21:51 +1200 (Tue, 15 Jun 2010) | 6 lines

Fix "Undo" crash after changing selection mode (backport r1137954)

Patch by Igor Poboiko, thanks!
CCBUG: 211481
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1138343 | aacid | 2010-06-16 06:35:17 +1200 (Wed, 16 Jun 2010) | 5 lines

backport r1138341 | aacid | 2010-06-15 19:27:44 +0100 (Tue, 15 Jun 2010) | 3 lines

properly initialize m_vectorIndex
BUGS: 202213

------------------------------------------------------------------------
r1139705 | scripty | 2010-06-19 14:11:28 +1200 (Sat, 19 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141602 | scripty | 2010-06-23 14:07:28 +1200 (Wed, 23 Jun 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1141932 | cfeck | 2010-06-24 08:48:28 +1200 (Thu, 24 Jun 2010) | 5 lines

Fix crash caused by signal emission in destructor (backport r1141930)

CCBUG: 221611
FIXED-IN: 4.4.5

------------------------------------------------------------------------
r1142683 | pino | 2010-06-26 00:13:35 +1200 (Sat, 26 Jun 2010) | 3 lines

bump okular version to 0.10.5
bump okular-poppler version to 0.3.1, as it got a bugfix

------------------------------------------------------------------------
