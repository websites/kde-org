------------------------------------------------------------------------
r933552 | pino | 2009-03-01 10:50:29 +0000 (Sun, 01 Mar 2009) | 6 lines

Backport SVN commit 933550 by pino:
Get the file list from the document, instead of spoofing them from the TOC.
FIlter out files not ending with ".html", to avoid loading non-HTML contents as pages.
Will be in KDE 4.2.2.
CCBUG: 183542

------------------------------------------------------------------------
r933584 | pino | 2009-03-01 12:11:14 +0000 (Sun, 01 Mar 2009) | 5 lines

backport various fixes:
- put, if valid, the home url as first page
- accept .htm files too
- connect also to the signal for non-successful loadin

------------------------------------------------------------------------
r933659 | pino | 2009-03-01 13:45:54 +0000 (Sun, 01 Mar 2009) | 3 lines

there were changes for kde 4.2.1, but i missed to increase the version
now there are changes for kde 4.2.2, so take the opportunity and increase okular_chm's version _now_

------------------------------------------------------------------------
r933725 | pino | 2009-03-01 14:38:04 +0000 (Sun, 01 Mar 2009) | 4 lines

backport:
- turn a sequence of if() into a single switch()
- add Up (previous page) and Down (next page) keys for presentation mode

------------------------------------------------------------------------
r935099 | pino | 2009-03-04 14:31:42 +0000 (Wed, 04 Mar 2009) | 2 lines

backport: focus the text when showing an annotation popup

------------------------------------------------------------------------
r935441 | scripty | 2009-03-05 07:49:55 +0000 (Thu, 05 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935461 | mlaurent | 2009-03-05 08:05:26 +0000 (Thu, 05 Mar 2009) | 2 lines

Backport: fix mem leak

------------------------------------------------------------------------
r935554 | bero | 2009-03-05 15:59:36 +0000 (Thu, 05 Mar 2009) | 2 lines

Don't try to delete something that doesn't exist

------------------------------------------------------------------------
r935557 | gateau | 2009-03-05 16:14:29 +0000 (Thu, 05 Mar 2009) | 1 line

Bumped version number and copyright year. Updated email address.
------------------------------------------------------------------------
r935634 | gateau | 2009-03-05 21:01:17 +0000 (Thu, 05 Mar 2009) | 6 lines

Some KImageIO plugins (jpeg2000, grrr) return mimetype aliases instead of the real mimetype. Resolv alias for now.

BUG:183903

CCMAIL:dfaure@kde.org
David, should we fix the .desktop files instead?
------------------------------------------------------------------------
r935770 | scripty | 2009-03-06 07:43:36 +0000 (Fri, 06 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r935791 | cas | 2009-03-06 09:25:09 +0000 (Fri, 06 Mar 2009) | 1 line

Fix bug #186187: crash when switching to parent directory when in View mode
------------------------------------------------------------------------
r935812 | gateau | 2009-03-06 10:39:43 +0000 (Fri, 06 Mar 2009) | 1 line

Backported crop fix for bug 184876.
------------------------------------------------------------------------
r935813 | gateau | 2009-03-06 10:39:49 +0000 (Fri, 06 Mar 2009) | 1 line

Check those pointers, M. Gâteau!
------------------------------------------------------------------------
r936561 | pino | 2009-03-07 22:13:59 +0000 (Sat, 07 Mar 2009) | 5 lines

backport: make sure to attach to the document also when a resize event is received prior of a paint event
will be in KDE 4.2.2

CCBUG: 180291

------------------------------------------------------------------------
r936957 | darioandres | 2009-03-08 18:18:24 +0000 (Sun, 08 Mar 2009) | 9 lines

Backport to 4.2branch of:
SVN commit 936956 by darioandres:

Fix timer rectangle size using fontMetrics width/height and translated text lengths
Patch from http://forum.kde.org/kourse-patch-bug-175186-timer-rectangle-too-small-t-22850.html
+ Small fix (margins)

CCBUG: 175186

------------------------------------------------------------------------
r936988 | cgilles | 2009-03-08 20:01:25 +0000 (Sun, 08 Mar 2009) | 2 lines

updated internal libraw to 0.6.14. BC preserved

------------------------------------------------------------------------
r936991 | cgilles | 2009-03-08 20:04:55 +0000 (Sun, 08 Mar 2009) | 2 lines

polish

------------------------------------------------------------------------
r937024 | lueck | 2009-03-08 21:46:41 +0000 (Sun, 08 Mar 2009) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r937239 | scripty | 2009-03-09 08:23:21 +0000 (Mon, 09 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r938064 | scripty | 2009-03-11 08:38:38 +0000 (Wed, 11 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939097 | cgilles | 2009-03-13 20:23:28 +0000 (Fri, 13 Mar 2009) | 3 lines

libkdcraw from KDE4.2 branch : update internal libraw to 0.6.15
BUG: 187015

------------------------------------------------------------------------
r939195 | scripty | 2009-03-14 08:01:53 +0000 (Sat, 14 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939533 | scripty | 2009-03-15 07:49:31 +0000 (Sun, 15 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r939961 | scripty | 2009-03-16 07:54:21 +0000 (Mon, 16 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r942138 | scripty | 2009-03-21 08:16:59 +0000 (Sat, 21 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r943106 | pino | 2009-03-23 12:19:14 +0000 (Mon, 23 Mar 2009) | 7 lines

backport the improvements to inverse search:
- handle relative file names in source reference links correctly
- read correctly the destination file name for non-grouped items in pdfsync links
- add functions to get the nearest object rect to a point in a page, and return its distance, and the accessor functions for object rects
- improve the source reference picking by looking for the nearest object in case there's none right under the mouse
- use -1 to indicate an unspecified coordinate now (in dvi links)

------------------------------------------------------------------------
r943375 | woebbe | 2009-03-23 18:27:51 +0000 (Mon, 23 Mar 2009) | 8 lines

slotAboutBackend(): use the document's mimetype to get an icon instead of the unknown icon.
this way you don't have to specify redundant icon names yourself.

CCMAIL: pino@kde.org

I hope the patch is OK for you. If so I'll forwardport it.

As I only have PDF files, could you please test the other plugins?
------------------------------------------------------------------------
r943944 | gateau | 2009-03-24 16:58:22 +0000 (Tue, 24 Mar 2009) | 1 line

Use QUndoStack::cleanChanged() rather than indexChanged() as it matches better with what we need
------------------------------------------------------------------------
r943945 | gateau | 2009-03-24 16:59:35 +0000 (Tue, 24 Mar 2009) | 1 line

Add Q_OBJECT macro (useful to display class name)
------------------------------------------------------------------------
r943946 | gateau | 2009-03-24 16:59:42 +0000 (Tue, 24 Mar 2009) | 1 line

Make sure mUndoStack does not cause the document to emit signals when it's deleted.
------------------------------------------------------------------------
r943947 | gateau | 2009-03-24 16:59:49 +0000 (Tue, 24 Mar 2009) | 3 lines

Do not keep a pointer to the document, just store its url.

This avoids increasing document refcount, preventing it from being garbage collected.
------------------------------------------------------------------------
r943948 | gateau | 2009-03-24 16:59:58 +0000 (Tue, 24 Mar 2009) | 1 line

Garbage collect when a document is saved, so that user gets his memory back faster.
------------------------------------------------------------------------
r943949 | gateau | 2009-03-24 17:00:06 +0000 (Tue, 24 Mar 2009) | 1 line

Let's maintain a NEWS file again.
------------------------------------------------------------------------
r943954 | gateau | 2009-03-24 17:15:41 +0000 (Tue, 24 Mar 2009) | 1 line

Update
------------------------------------------------------------------------
r943980 | pino | 2009-03-24 19:21:00 +0000 (Tue, 24 Mar 2009) | 2 lines

bump version to 0.8.2

------------------------------------------------------------------------
r944113 | reed | 2009-03-25 04:09:15 +0000 (Wed, 25 Mar 2009) | 1 line

Mac OS X 10.4 gets an error if stdio.h is included without sys/types.h first
------------------------------------------------------------------------
r944172 | scripty | 2009-03-25 08:46:19 +0000 (Wed, 25 Mar 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
