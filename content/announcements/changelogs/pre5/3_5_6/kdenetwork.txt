2006-10-07 16:50 +0000 [r593368]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/webcamtask.cpp:
	  Fix bug where only the first image of a webcam transmission was
	  shown. BUG:131218

2006-10-07 18:36 +0000 [r593402-593400]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/main.cpp: Update my
	  email address and blog in Kopete credits

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/Makefile.am: Revert
	  last commit

2006-10-08 12:55 +0000 [r593598]  jkekkonen

	* branches/KDE/3.5/kdenetwork/kopete/kopete/eventsrc: Fix typo,
	  thanks kubuntuers for pointing out. BUG: 135287

2006-10-08 13:31 +0000 [r593608]  jkekkonen

	* branches/KDE/3.5/kdenetwork/kopete/kopete/eventsrc: revert it,
	  forgot string freeze. CCBUG: 135287

2006-10-08 17:06 +0000 [r593669]  mueller

	* branches/KDE/3.5/kdenetwork/kppp/accounts.cpp: fix && / &
	  confusion

2006-10-11 01:14 +0000 [r594360]  mattr

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemetacontact.cpp:
	  Apply patch from Paulo Fidalgo to resize the image to 96x96 if it
	  is larger than 96x96. Thanks for the patch! Sorry it took so long
	  to get this committed CCMAIL: kanniball@zmail.pt

2006-10-13 12:55 +0000 [r595163]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemessagehandler.cpp:
	  I don't quite understand how, but the iterator tends to become
	  invalid, and then it crashes. optimizing memory and stability and
	  the same time :)

2006-10-14 17:53 +0000 [r595529]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopetemetacontact.cpp:
	  Preserve aspect ratio when resizing the image to 96x96 if it is
	  larger than 96x96.

2006-10-15 10:15 +0000 [r595652]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/plugins/nowlistening/nowlisteningplugin.cpp:
	  Add a space before the listening message in parenthesis.

2006-10-18 19:51 +0000 [r596874]  pfeiffer

	* branches/KDE/3.5/kdenetwork/kget/kget_plug_in/kget_linkview.cpp:
	  trivial patch to add a KListViewSearchLine to the KGet "List all
	  links" view BUG: 114361

2006-10-23 14:36 +0000 [r598419]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/pollsearchresultstask.cpp:
	  Fix crash when searching contacts and they have more than one
	  department, where we expected only one previously. b.n.c #210577

2006-10-31 00:52 +0000 [r600575]  charis

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/ifconfigpagebase.ui:
	  Commiting small layout fix for kwifimanager.I hope i don't break
	  anything.. BUG:113742

2006-10-31 16:29 +0000 [r600742]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwprotocol.cpp:
	  Make the UI consistent

2006-10-31 18:48 +0000 [r600778]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Fix bug 136566: Connecting to ICQ doesn't work anymore. OSCAR
	  server stops sending own user info at logon, we have to send own
	  user info request like ICQ5. BUG: 136566

2006-11-01 15:13 +0000 [r600976]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/oscaraccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/client.cpp:
	  Fix bug when going online with any other status than online i.e.
	  away, n/a... Kopete indicates the account as online. The status
	  on a server is correct. Reported by Jan Ritzerfeld, thanks.
	  Please test it. CCBUG: 136566

2006-11-02 16:15 +0000 [r601236]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/history/historydialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/statistics/statisticsdialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/chatmessagepart.cpp:
	  Security fix, only load local references in our use of KHTMLPart.

2006-11-02 16:26 +0000 [r601242]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/ui/userinfodialog.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimuserinfo.h:
	  Security fix, only load local references in our use of KHTMLPart.
	  Pressed Enter too soon on the previous commit.

2006-11-02 16:49 +0000 [r601250-601249]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/transport.cpp:
	  Security: check size correctly

	* branches/KDE/3.5/kdenetwork/kopete/plugins/history/converter.cpp:
	  Use correct size limit when reading history

2006-11-03 13:09 +0000 [r601500]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindowstylemanager.cpp:
	  Now we can remove style

2006-11-03 14:12 +0000 [r601513]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindowstylemanager.cpp:
	  Fix inform when we add new style. Otherwise when "Style"
	  directory is not created (you didn't add new style before) we
	  don't test this directory and when we add new style it doesn't
	  add to interface => we don't know that new style added. => before
	  it was necessary to close kopete and restart kopete

2006-11-03 15:42 +0000 [r601549]  mlaurent

	* branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarkspreferences.cpp:
	  Fix signal/slot When plugins is not loaded we can't connect on
	  it.

2006-11-04 21:12 +0000 [r601975]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksprefssettings.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarkspreferences.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksplugin.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksprefssettings.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarkspreferences.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksprefsui.ui:
	  Clean ups in addbookmarks plugin. Add security check that
	  received URLs are http(s), and default to not adding any URL from
	  contacts not on our contact list.

2006-11-04 22:51 +0000 [r601994]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksprefssettings.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarkspreferences.cpp:
	  Add safe default even when config file not present, and remove
	  debug

2006-11-05 11:24 +0000 [r602095]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/alias/aliaspreferences.cpp:
	  Fix bug 135813: parameters aren't accepted by the alias plugin
	  BUG:135813

2006-11-05 15:56 +0000 [r602278]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/autoreplaceprefs.ui,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/autoreplaceplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/autoreplacepreferences.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/autoreplace/autoreplaceplugin.h:
	  Make sentence options (Capitalize and full stop.) work, and
	  handle inbound messages as advertised. NB Hariz Kouzinopoulos: I
	  did not use your patch, because while it is technically correct,
	  the job of saving the widgets' values is performed automatically
	  by KCAutoConfigModule, if it knows about the widgets. A previous
	  author neglected to change the widgets passed to
	  KCAutoConfigModule when adding the sentence options, so that is
	  why these were not read/written. Thank you for submitting a patch
	  though, it was the impetus to solve the bug properly. BUG:116381

2006-11-05 16:25 +0000 [r602283]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarksplugin.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/addbookmarks/addbookmarkspreferences.cpp:
	  Less sucky version of my previous patch.

2006-11-08 11:11 +0000 [r603246]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/client.cpp:
	  That should fix the crash after duplicate login. BUG: 136390

2006-11-13 18:59 +0000 [r604683]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/interface_wireless_wirelessextensions.cpp:
	  Argh! The Kernel 2.6.19 Update lost the one line of code where
	  the SSID actually gets read from the kernel :-((( BUG:131527

2006-11-17 10:21 +0000 [r605584]  cartman

	* branches/KDE/3.5/kdenetwork/filesharing/advanced/kcm_sambaconf/Makefile.am:
	  Don't add lib prefix to kcm module name

2006-11-17 14:45 +0000 [r605617]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete.kdevelop,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/client.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/libgroupwise/tasks/keepalivetask.h,
	  branches/KDE/3.5/kdenetwork/kopete/plugins/cryptography/cryptographyplugin.cpp:
	  Start sending keepalives as the upcoming SP2 requires them

2006-11-19 07:32 +0000 [r606058]  seb

	* branches/KDE/3.5/kdenetwork/kopete/plugins/nowlistening/nlamarok.cpp:
	  Correct spelling

2006-11-19 12:43 +0000 [r606122]  hermier

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircnetworks.xml:
	  Fix wrong irc address for OFTC. CCBUG: 137567

2006-11-22 15:16 +0000 [r606974]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/config/appearance/appearanceconfig.cpp:
	  Add explanatory text for those users who can't read right-to-left
	  languages. Ill-educated oafs, the lot of them

2006-11-23 14:05 +0000 [r607167]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/kopete/chatwindow/kopetechatwindow.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/groupwise/gwaccount.h:
	  remove pointless return type qualifiers

2006-11-25 11:53 +0000 [r607621]  swinter

	* branches/KDE/3.5/kdenetwork/wifi/status.cpp: correcting 5 GHz
	  channel <-> frequency mappings thanks to LANCOM Systems for
	  gratiously providing me with a 11a Access Point to try stuff out
	  no need to forward-port, KDE4 uses iwstats mapping and no static
	  hard-coded tables I wonder if there are 11a bands between 5.32
	  and 5.5 GHz BUGS: CCMAIL:eckhart.traber@lancom.de

2006-11-28 11:38 +0000 [r608770]  mueller

	* branches/KDE/3.5/kdenetwork/kopete/protocols/irc/ircaccount.cpp:
	  convert it into a warning

2006-12-02 11:22 +0000 [r609808]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimcontact.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/aim/aimaccount.cpp:
	  Fix bug 132247: Line breaks in AIM not sent correctly. AIM only
	  recognises <BR> tag and not <BR />. BUG: 132247

2006-12-12 15:18 +0000 [r612769]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/kopete/eventsrc,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnnotifysocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnswitchboardsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteutils.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnsocket.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteutils.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnsocket.h:
	  Improve error handling in MSN plugin to use notifications instead
	  of messages box. To do this I needed to introduce 1 new
	  string(and I could introduce more but I stepped out) and do BIC
	  changes in Kopete::Utils. BUG: 138600 BUG: 125186 BUG: 137395

2006-12-15 21:06 +0000 [r613961]  pino

	* branches/KDE/3.5/kdenetwork/wifi/kcmwifi/kcmwifi.desktop,
	  branches/KDE/3.5/kdenetwork/wifi/kwifimanager.desktop,
	  branches/KDE/3.5/kdenetwork/krdc/krdc.desktop,
	  branches/KDE/3.5/kdenetwork/knewsticker/knewstickerstub/knewstickerstub.desktop,
	  branches/KDE/3.5/kdenetwork/krfb/krfb/krfb.desktop: Make them a
	  bit more freedesktop.org compliant.

2006-12-16 00:04 +0000 [r614005]  wstephens

	* branches/KDE/3.5/kdenetwork/doc/kopete/index.docbook: Finally
	  update the manual for Kopete 0.12.

2006-12-16 00:12 +0000 [r614008]  wstephens

	* branches/KDE/3.5/kdenetwork/kopete/kopete/kopeteiface.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/kopete/kopeteiface.h: Highly
	  important dcop methods - why didn't we think of these before?

2006-12-16 01:27 +0000 [r614033]  gj

	* branches/KDE/3.5/kdenetwork/kopete/protocols/gadu/gaduaccount.cpp:
	  server list has changed, update also priorities

2006-12-18 14:45 +0000 [r614639]  duffeck

	* branches/KDE/3.5/kdenetwork/kopete/protocols/yahoo/libkyahoo/messagereceivertask.cpp:
	  Gaim sends key->value pairs in different order. Fix parsing of
	  these packets. BUG:138826

2006-12-25 21:32 +0000 [r616492]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/oscarmessage.cpp:
	  Fix bug 139199: strange sign after every message from icq2go BUG:
	  139199

2006-12-28 12:19 +0000 [r617203]  mkoller

	* branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.cpp:
	  BUG: 89509 avoid endless loop in multi selection

2006-12-28 14:16 +0000 [r617246]  mkoller

	* branches/KDE/3.5/kdenetwork/filesharing/simple/fileshare.cpp:
	  correctly pass path to KURL

2007-01-05 14:24 +0000 [r620253]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/avdevice/videodevice.h:
	  compile

2007-01-06 15:27 +0000 [r620548]  lueck

	* branches/KDE/3.5/kdenetwork/krfb/kcm_krfb/kcmkrfb.desktop: fixed
	  wrong link in khelpcenter, thanks to Richard Johnson for the
	  bugreport BUG:139195

2007-01-07 14:11 +0000 [r620836]  rjarosz

	* branches/KDE/3.5/kdenetwork/kopete/protocols/oscar/liboscar/changevisibilitytask.cpp:
	  Fix bug 139701: Away msg polling too frequently. When user or
	  Auto-Away plugin change status in ICQ we change invisible status
	  even though invisible status didn't change at all and as a result
	  of this ICQ server sends us packets that indicate status changes
	  for all contacts. BUG: 139701

2007-01-08 10:33 +0000 [r621124]  mueller

	* branches/KDE/3.5/kdenetwork/ksirc/iocontroller.cpp: add fix for
	  CVE-2006-6811

2007-01-09 03:13 +0000 [r621565]  aseigo

	* branches/KDE/3.5/kdenetwork/ksirc/iocontroller.cpp: prevent a
	  crash with lines that start with ~~

2007-01-09 13:17 +0000 [r621650]  ogoffart

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/webcam.cpp,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/webcam.h: Speed
	  up the webcam rate. Default to 25fps, configurable with an hidden
	  entry in the config file. (which should be documented somewhere)
	  Patch by Xavier FACQ, thanks BUG: 131861

2007-01-10 22:51 +0000 [r622184]  hermier

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteonlinestatusmanager.h:
	  export OnlineStatusManager, needed for meanwhile plugin (at
	  least)

2007-01-11 08:10 +0000 [r622257]  hermier

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/connectionmanager.h:
	  Missing export necessary for smpppdcs plugin (reported by
	  Arkadiusz Miskiewicz)

2007-01-14 09:45 +0000 [r623157]  hermier

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/connectionmanager.cpp:
	  Fix indent. What is the purpose of unused i18n calls ?

2007-01-14 09:49 +0000 [r623158]  hermier

	* branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jingle/libjingle/talk/p2p/base/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jingle/libjingle/talk/xmpp/Makefile.am,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/jabber/jingle/libjingle/talk/base/Makefile.am:
	  Workaround fix to allow libjingle to compile with enable-final.
	  Thanks to crazy from furgalware.

2007-01-14 11:51 +0000 [r623202]  jritzerfeld

	* branches/KDE/3.5/kdenetwork/kopete/libkopete/connectionmanager.cpp:
	  Make it compile again with Qt3: Replaced QLatin1String (Qt4) by
	  QString::fromLatin1 (Qt3).

2007-01-14 20:08 +0000 [r623428]  mlarouche

	* branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.h,
	  branches/KDE/3.5/kdenetwork/kopete/protocols/msn/msnaccount.cpp:
	  Always apply the global identity photo even if the account
	  doesn't export a picture. Based on patch by Luigi Toscano. BUG:
	  131412

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

