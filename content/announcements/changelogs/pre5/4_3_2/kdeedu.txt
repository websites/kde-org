------------------------------------------------------------------------
r1017238 | annma | 2009-08-30 08:46:45 +0000 (Sun, 30 Aug 2009) | 4 lines

add Hungarian support - replace training with new one (better labelled)
thanks a lot for your work Szabo! I will commit to trunk as well 
CCMAIL=szabozoltan69@gmail.com

------------------------------------------------------------------------
r1017926 | hedlund | 2009-08-31 22:14:26 +0000 (Mon, 31 Aug 2009) | 3 lines

Backport of fix for bug 205606.


------------------------------------------------------------------------
r1018061 | scripty | 2009-09-01 03:25:08 +0000 (Tue, 01 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1019501 | lueck | 2009-09-03 18:50:17 +0000 (Thu, 03 Sep 2009) | 1 line

edu doc backport for 4.3
------------------------------------------------------------------------
r1024453 | scripty | 2009-09-16 16:16:15 +0000 (Wed, 16 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1025878 | markuss | 2009-09-19 23:29:37 +0000 (Sat, 19 Sep 2009) | 1 line

Fix a missing icon
------------------------------------------------------------------------
r1025924 | scripty | 2009-09-20 03:20:00 +0000 (Sun, 20 Sep 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1027708 | annma | 2009-09-24 13:59:27 +0000 (Thu, 24 Sep 2009) | 2 lines

fix hint behavior: enable it globally instead of per word as kids don't want to always click on it to show it.

------------------------------------------------------------------------
r1028128 | lueck | 2009-09-25 23:00:18 +0000 (Fri, 25 Sep 2009) | 2 lines

extract element names and use them in the engine, makes 238 new messages for 4.3
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r1028135 | lueck | 2009-09-25 23:10:03 +0000 (Fri, 25 Sep 2009) | 1 line

revert message extraction, these strings are already in libkdede
------------------------------------------------------------------------
r1028488 | lueck | 2009-09-27 11:39:32 +0000 (Sun, 27 Sep 2009) | 1 line

loading the catalog is sufficient to make it translated
------------------------------------------------------------------------
r1029614 | sengels | 2009-09-30 11:24:16 +0000 (Wed, 30 Sep 2009) | 1 line

fix linking of library
------------------------------------------------------------------------
