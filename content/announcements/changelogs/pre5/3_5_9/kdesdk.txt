2007-11-03 05:26 +0000 [r732208-732207]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/uml.lsm: update

	* branches/KDE/3.5/kdesdk/umbrello/umbrello/codegenerators/csharpwriter.cpp,
	  branches/KDE/3.5/kdesdk/umbrello/ChangeLog,
	  branches/KDE/3.5/kdesdk/umbrello/umbrello/umlwidgetcontroller.cpp:
	  apply r728591 and r732152 from trunk

2007-11-03 05:44 +0000 [r732209]  okellogg

	* branches/KDE/3.5/kdesdk/umbrello/make-umbrello-release.sh: use
	  scripts from $origdir/..

2007-12-19 08:15 +0000 [r750422]  kkofler

	* branches/KDE/3.5/kdesdk/kompare/kompare_shell.cpp: Don't reset
	  encoding to "Default" for no good reason, it breaks diffs of e.g.
	  UTF-8 files. Thanks to George Goldberg
	  <grundleborg@googlemail.com>. CCMAIL: kompare-devel@kde.org
	  CCMAIL: grundleborg@googlemail.com (backport rev 750413 from
	  trunk)

2007-12-19 08:42 +0000 [r750429]  kkofler

	* branches/KDE/3.5/kdesdk/kompare/libdiff2/komparemodellist.cpp,
	  branches/KDE/3.5/kdesdk/kompare/libdiff2/kompareprocess.cpp:
	  Default to locale encoding. CCBUG: 112729 (backport rev 112729
	  from trunk)

2008-01-10 11:00 +0000 [r759301]  bero

	* branches/KDE/3.5/kdesdk/kioslave/svn/configure.in.in: Fix build
	  with apr 1.2.11 (apr-config -> apr-1-config rename)

2008-02-13 09:53 +0000 [r774475]  coolo

	* branches/KDE/3.5/kdesdk/kdesdk.lsm: 3.5.9

2008-02-13 10:54 +0000 [r774494]  woebbe

	* branches/KDE/3.5/kdesdk/cervisia/Makefile.am,
	  branches/KDE/3.5/kdesdk/cervisia/version.h: bump version number

