commit cf6cf2284603e8256dbdf6612ebe9ab98df28a87
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Mon Mar 28 17:22:07 2011 +0200

    Align fixed background when printing
    
    Fixes the problem with background images with attachment 'fixed',
    which was unaligned after page 1 when printed.

commit eec82efa754e0218aeb3f2f8da885408ee639fe8
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Mar 31 21:03:52 2011 +0200

    bump version to 4.6.2

commit b7806af9c70ab544802b4ab54c154230cc51f8da
Author: Dominik Haumann <dhdev@gmx.de>
Date:   Wed Mar 30 12:05:14 2011 +0200

    change swap file name to .<filename>.kate-swp
    
    see bug #269518

commit 31893af80d425159fbc129029a674c5379e3af6a
Author: Matthias Fuchs <mat69@gmx.net>
Date:   Sun Mar 27 19:05:42 2011 +0200

    KNS overwrites existing files automatically on an update
    
    The existing code already suggests that the user should not be asked for overwriting files if they do an update.
    This patch makes this finally work now by also checking if the status is Entry::Updating.
    
    (cherry picked from commit d7d64eded27ab02f9021441f61f0484779549330)

commit 411e33cb35449fd71a54b519396fc1c4c00bb574
Author: Matthias Fuchs <mat69@gmx.net>
Date:   Sat Mar 26 22:15:42 2011 +0100

    Shares the KNS3::Cache amongst every user in a program instance.
    
    Thus insuring that the cache is always consistent.
    This way it can't happen, that e.g. DownloadManager updates some KNS3::Entries and a call to DownloadWidget later on would overwrite the updated ones as updateable.
    
    (cherry picked from commit f3f98752c2c6cbb5d46cb41ea9fdae82e6a4b5ae)

commit b18b43d9c904a2ba794ac5802045294a23e35e69
Author: Matthias Fuchs <mat69@gmx.net>
Date:   Sun Mar 27 17:35:17 2011 +0200

    KNS3 correctly stores updated entries as installed.
    
    * AtticaProvider also changes the cachedEntry in (!) the cache and it does that for Installed and for Updateable entries.
    The later is important, since sometimes updates are not done.
    * Installation only stores updateReleaseDate if it is valid.
    
    CCBUG:256473
    FIXED-IN:4.6.2
    (cherry picked from commit 5627f9a9d2bcc84a83d4b9415796889f9758ff39)

commit 3d44cb1fa7a4cc935813f4bcacc132d3bace54d7
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Mon Mar 28 16:38:12 2011 +0200

    Fix serious regression in table printing
    
    Old behavior restored after RenderRow has gotten their own coordinates.
    BUG:259163
    BUG:190071

commit 44401745dbf91e8212b1f5367d67956c921d993e
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Sun Mar 27 23:45:30 2011 -0300

    Reverting commit c956b436f63fa957a866ad049f7dbf14ee708968 since it does
    not solve the problem in question.

commit 9d04d522afe1ec3c550a019f0720c495d47d4763
Author: Script Kiddy <scripty@kde.org>
Date:   Sun Mar 27 16:08:52 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 8b06e2c7f52935c279a89f7fd0a1d8d6c77fda3a
Author: Jeff Mitchell <mitchell@kde.org>
Date:   Sun Mar 27 09:08:58 2011 -0400

    Fix CVE-2011-1168. Credit to Tim Brown of Nth Dimension.

commit a1597d87a46e951fd62b632f250ee7ef223b51e4
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 24 18:19:55 2011 -0400

    Emit the finished signals after calling ftpCloseCommand.
    
    This fixes the problem where clicking on a file while browsing an FTP site does
    not show its content. Instead only the associated application is launched and
    either an error message, in case of kate, and/or a notification about the completion
    of file copying is shown.
    
    BUG:203445
    BUG:181393
    REVIEW:100941
    FIXED-IN:4.6.2

commit 61d75c0c2ab77e8d0773b260f3ae5b8da67d00db
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Fri Mar 25 00:12:30 2011 +0100

    Revert "Corrected backgroundlayer operators"
    
    This reverts commit eb453701c361437c8003333fb138cf88be821e6b.
    
    Conflicts:
    
    	khtml/rendering/render_style.cpp

commit 81a2920b539b72e6f09e12eb85e03c357a2c326c
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Thu Mar 24 21:15:27 2011 +0100

    Fix printing wide pages
    
    Scaling was applied but promptly forgotten

commit eea857f2ff624fa72e34a74fa5421f00213c911b
Author: Allan Sandfeld Jensen <carewolf@cinex.carewolf.com>
Date:   Thu Mar 24 21:02:25 2011 +0100

    Respect print images setting for background images

commit 77b16da27b4eb0d4cd735d1a08d05ed5ec1842ef
Merge: 1e7795b fdd95ee
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Thu Mar 24 21:30:50 2011 +0100

    Merge remote-tracking branch 'origin/KDE/4.6' into KDE/4.6

commit fdd95ee87dcb60bc91729414d3555ddcad124352
Author: Lukas Tinkl <lukas@kde.org>
Date:   Thu Mar 24 16:16:04 2011 +0100

    fix excessive spinning of optical drives just to get the content type
    (introduce a special cache for that)
    
    BUG: 264487

commit c956b436f63fa957a866ad049f7dbf14ee708968
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Tue Mar 22 22:15:14 2011 -0300

    References to local variables are not safe to use after function return,
    so we have better copy the value instead.
    
    BUG: 226596, 258706, 269118, 268157, 258706, 226596, 268473, 269118, 268597, 268157, 268871, 268436, 268886, 268518, 269147, 267691, 268858, 268180, 268874
    FIXED-IN: 4.6.2

commit 1e7795b992455bbbadf44dd74a8af9135f8ef9c9
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Mon Mar 21 18:10:57 2011 +0100

    Compile

commit 3cee6a3b0b500f1579b7ec5c6c12d7346453b1cf
Merge: eb45370 77029f7
Author: Allan Sandfeld Jensen <kde@carewolf.com>
Date:   Mon Mar 21 16:13:07 2011 +0100

    Merge remote-tracking branch 'origin/KDE/4.6' into KDE/4.6

commit eb453701c361437c8003333fb138cf88be821e6b
Author: Allan Sandfeld Jensen <carewolf@cinex.carewolf.com>
Date:   Mon Mar 21 16:08:18 2011 +0100

    Corrected backgroundlayer operators
    
    Operator= and operator== lacked handling some parameters

commit 77029f7f10c5f163e1fb98d52745f19459b7b065
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Mar 20 16:51:17 2011 -0400

    sizeof -> strlen

commit 04b6451e6449c706d5b4d1265e4ac172e3de07b8
Author: Frank Reininghaus <frank78ac@googlemail.com>
Date:   Sun Mar 20 13:10:17 2011 +0100

    KNewFileMenu: remove spaces at the end of the default file name
    
    In some languages, there is a space between the file type and "..." in
    the menu entry of the "Create New" menu. This space should be removed
    from the menu entry text in order to get the default file name.
    
    BUG: 268895
    FIXED-IN: 4.6.2

commit 0a0368bac3e1586328f6d0869f62ef14687906ea
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Mar 18 12:57:55 2011 -0400

    Allow copying of zero byte sized files.
    
    BUG:184594
    FIXED-IN:4.6.2

commit 75bb906e09ada25ad60ccdf3552196b27aed1002
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Mar 18 12:57:30 2011 -0400

    Do not double encode directory names.
    
    BUG:213496
    FIXED-IN:4.6.2

commit 85878a6acfb5f03ec06f58340296582ba9aab7e3
Author: Frank Reininghaus <frank78ac@googlemail.com>
Date:   Fri Mar 18 13:17:12 2011 +0100

    KNewFileMenu: Add clear buttons to dialogs asking for a name
    
    This commit adds a clear button to the KLineEdits in the dialogs
    that ask the user for a file or folder name when a file or folder
    is to be created. Note that the KLineEdit text is set after the
    clear button is enabled. If it is done the other way around, the
    clear button is not shown initially.
    
    BUG: 267581
    FIXED-IN: 4.6.2

commit b3154f7de05e17bf33c456e52e121a89e985c915
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 17 22:25:25 2011 -0400

    Revert the header changes in previous commit 7622ef

commit 7622ef42bc895cdbf30f15098445f48b684e1314
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 17 22:10:29 2011 -0400

    Correctly handle FTP urls with type code in their path as specified in RFC 1738,
    e.g. ftp://ftp.kernel.org/pub/README;type=a
    
    BUG:92589
    REVIEW:100872
    FIXED-IN:4.6.2

commit 0b952035a61707fb870ea7239160a57fdeac631d
Author: Maks Orlovich <maksim@kde.org>
Date:   Sun Mar 6 10:02:53 2011 -0500

    Fix regression in charset parsing.
    
    Implicit LWS is permitted between tokens in key/value pairs.
    
    BUG: 260885

commit af94d3aecfa5edf4796eb7b16eed061b8a33a2c9
Author: David Faure <faure@kde.org>
Date:   Thu Mar 17 18:09:12 2011 +0100

    Make it compile with KDE_NO_DEBUG_OUTPUT
    
    Patch by Fabrice Ménard <menard.fabrice@orange.fr>
    (cherry picked from commit 5637246501a9cdd87b2c2a1d869ae8974b70d4c0)

commit 8265e88d5864baffcb50b473543404e68f506742
Author: Aaron Seigo <aseigo@kde.org>
Date:   Thu Mar 17 16:46:35 2011 +0100

    protect with a weak pointer
    
    BUG:268752

commit 49b1ee18a9560fa9c6eb38150ae54f5d5fc5eac2
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 17 00:24:25 2011 -0400

    Check the HTTP status code only for http and webdav protocols

commit 4de5f634c8d520bb30aad9653a20b42c35fe00ae
Author: Albert Astals Cid <aacid@kde.org>
Date:   Wed Mar 16 18:59:35 2011 +0000

    Export the class and fix the since
    (cherry picked from commit 9e0dbe60755093a001ab46c25a37881789314a54)

commit 8ba376c12e3549a10ec334fb0049f7614efd8716
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 16 14:34:32 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit b5067ac5aaaabaa15fe31c9a1b461b0e47e71bad
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 15 15:50:48 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit 4695c212576aa9f3b1bc5f31d7609b34241cdb10
Author: Sebastian Kügler <sebas@kde.org>
Date:   Tue Mar 15 03:03:18 2011 +0100

    Fix icon handling in package metadata

commit c7132a2bdd0bc409b4f208d3ca5429daefa471c6
Author: Albert Astals Cid <aacid@kde.org>
Date:   Sun Mar 13 14:34:53 2011 +0000

    mobipocket mime
    
    Forgot to backport this long time ago when commiting to trunk

commit 87ab0af84c3d87942892b8a024a252efe9cf4546
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sat Mar 12 20:34:30 2011 +0100

    Use Qt::escape() instead of a temporary QTextDocument

commit 98566496a7ba0c0793e7bddcb6ae17ab5222d40b
Author: Peter Penz <peter.penz19@gmail.com>
Date:   Sat Mar 12 18:28:55 2011 +0100

    KCommentWidget: Don't interpret HTML-tags in comments
    
    Thanks to Wolfgang Mader for the patch!
    
    BUG: 267648

commit 979a19afe93d1e4a5be684c1139b6ab242e6b9b6
Author: David Faure <faure@kde.org>
Date:   Fri Mar 11 21:01:24 2011 +0100

    Add paths to qt plugins (from kstandarddirs) before qapp is created.
    
    Necessary for apps started through d-bus activation, so that the kde widget
    style is loaded. The QCoreApp::instance test was "so that qt.conf is read" (db852684)
    but the whole idea of 9d7eb8f7 is that this doesn't rely on the qt config files anymore.
    
    FIXED-IN: 4.6.2
    BUG: 267770

commit 2bca9e66d0f894325745814c6f796ded3c545044
Author: Patrick Spendrin <ps_ml@gmx.de>
Date:   Sun Feb 20 21:39:20 2011 +0100

    we need to cast by hand from wchar to ushort
    needed for compilation on Windows with mingw4
    (cherry picked from commit fd16288cedadf8ccf469df8a7be296763f5003d8)

commit c7ed1a01fc32c999664fd316613a09f87d16482d
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 10 17:10:00 2011 -0500

    Make sure that unknown protocols are not returned as a valid slave protocols.
    This change prevents things like a FTP over SOCKS proxy request from being
    incorrectly handled by the http ioslave.

commit ebbe3a9ec4e111d698d01e7170262a00f6c59496
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Thu Mar 10 16:59:57 2011 -0500

    Revert commit 7b61621c023db80af888a12be7fcb9fd6c9a6fb8
    
    With this commit, the fixes to KParts::BrowserRun::scanFile to address
    the "FTP over HTTP proxy" bug should work properly.
    
    CCBUG:236630

commit a4f369f3102a30cc52921d112e0bb698810b85fb
Author: Stephen Kelly <stephen.kelly@kdab.com>
Date:   Thu Mar 10 11:22:34 2011 +0100

    Remove debug output.

commit c21ec206e3b19a4104dd4fe89b53539dc5020412
Author: Stephen Kelly <stephen.kelly@kdab.com>
Date:   Thu Mar 10 01:28:01 2011 +0100

    Workaround QTreeView bug.

commit 65488a7cdc06b1475f1b726f3403eee225282522
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Mar 8 21:21:30 2011 -0500

    Do not ignore reparse configuration notifications that originate from an
    application's own configuration dialog.
    
    REVIEW:100819

commit 1bc22e9af060c4626bebd7a091678f2ca5b40ae4
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Mar 8 08:57:16 2011 -0500

    Fix for connecting to FTP sites through HTTP proxy servers.
    
    BUG:236630
    REVIEW:100814
    FIXED-IN:4.6.2

commit 528fb4202ff7d55d693b6f175322f2acf0d6e753
Author: David Faure <faure@kde.org>
Date:   Tue Mar 8 13:49:38 2011 +0100

    Fix handling of unknown mimetypes.
    
    If there are no associated apps, or the mimetype is wrong/unknown, then use
    KRun to show an open-with dialog or determine the mimetype correctly.
    Same logic as in KonqMainWindow.
    My testcase was an attached MESSAGE/RFC822 (uppercase, so unknown) file in Jira.

commit 51707e7154082b549216b8a8ecde73505302fadc
Author: David Faure <faure@kde.org>
Date:   Tue Mar 8 11:23:47 2011 +0100

    Fix stop() killing the list job even if another dirlister needs it.
    
    Regression introduced by me in bef0bd3e3ff.
    Symptom: "dolphin $HOME" showed up empty.
    
    In the case of concurrent listings, I made the use of the cached items job
    conditional (only created if there's anything to emit) so that we can join
    the current listjob without killing it (updateDirectory) if it hasn't emitted
    anything yet.
    The unittest also uncovered inconsistencies in the emission of the cancelled
    signal, now cacheditemsjob behaves like the listjob in this respect.
    
    FIXED-IN: 4.6.2
    BUG: 267709

commit 7bb4e5237f9cdcc454d02270479abc2dc4447f0c
Author: David Faure <faure@kde.org>
Date:   Mon Mar 7 18:49:07 2011 +0100

    Fix the writing out of the Content-Length header.
    
    Regression introduced by 79d7273aff384fa6d1a6a209182fd0ad42ac691a's use
    of .data() instead of .toLatin1().

commit ceed40f86ec32544accf3999ebea187e7777dc0a
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Mar 6 15:27:22 2011 -0500

    Reverted commit 9f7fa757926b163f979116baf714577e4a99ef00. Only applies to the version in master

commit 0672bbd10827a95b81fc3358c29fb10cab12ec02
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sun Mar 6 15:09:07 2011 -0500

    Allow unicode characters in unquoted filename parameters of the
    content-disposition header.
    
    BUG:261223
    FIXED-IN:4.6.2

commit 1c7706bf5365375e817ad7fe1e320ad4175c4677
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat Mar 5 19:43:05 2011 -0500

    Honor the user's preference for persistent proxy connections by not overwriting
    the value of m_request.isKeepAlive once it has been set.

commit 9f7fa757926b163f979116baf714577e4a99ef00
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat Mar 5 19:28:24 2011 -0500

    Make sure m_iPostDataSize is always reset to NO_SIZE to avoid incorrect use.

commit 22d26943fe03d6e4c584a8a1e2b67fe55c1e066e
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Sat Mar 5 16:29:43 2011 -0500

    Use "PASS" instead "pass" when sending password to workaround broken ftp server.
    
    BUG:26154
    FIXED-IN:4.6.2

commit caa5ead18c00c8924f5b03d6495148e51e8f3cdf
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Mar 4 18:58:28 2011 -0500

    Fix the issue of HTTPS over proxy not working properly when connecting to two or more secure sites.
    
    BUG:155707
    FIXED-IN:4.6.2

commit 79d7273aff384fa6d1a6a209182fd0ad42ac691a
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Fri Mar 4 12:20:26 2011 -0500

    Retry the request if server disconnected before the content body could be sent to it.
    
    This is the same protection as the one in ::sendQuery and protects against crashes like the one that prints out the following:
    
    kio_http(3980)/kio (kioslave): Got cmd  77  while waiting for an answer!
    kioslave: ####### CRASH ###### protocol = https pid = 3980 signal = 6
    
    Conflicts:
    
    	kioslave/http/http.cpp

commit c12802a067c53d9e00f207afd26e578d19da975f
Author: Lukas Tinkl <lukas@kde.org>
Date:   Thu Mar 3 16:10:25 2011 +0100

    USB drive is still powered on after disconnecting it in Device Manager
    
    BUG: 267398

commit 9c9bf7286d74f3df64958607fd7b7251231cb455
Author: Marco Martin <notmart@gmail.com>
Date:   Wed Mar 2 22:04:53 2011 +0100

    after applet's dataupdated is called, dirty=false
    
    in DataEnginePrivate::connectSource, if it's an immediate call, and QMetaObject::invokeMethod(visualization, "dataUpdated" is called, means the datacontainers' dirty bit must be set to false, otherwise we will get two subsequent dataUpdated
    
    can it have countereffects?
    
    CCMAIL:plasma-devel@kde.org

commit 01db212fbdbb5008744bcb0dc5cea48561e57f5f
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Mar 2 14:46:04 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit eddc5487895fdfe0f8bec4bb127a586237873852
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue Mar 1 16:23:42 2011 -0500

    Correctly parse the parameter portion of the content-type header based on
    RFC 2616 section 3.7.
    
    BUG:266823

commit 73c84d7c6f0584fb49136727a1f8cdd6a3c697f5
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Mar 1 15:52:42 2011 +0100

    SVN_SILENT made messages (.desktop file)

commit f0db79826892817b7fb931b0418e2282f3efe054
Author: Lukas Tinkl <lukas@kde.org>
Date:   Mon Feb 28 17:33:39 2011 +0100

    report Vendor for AudioInterface devices

commit 9a920f6ef9c1a8649c69cd8112ef38caa1f0a7b6
Author: Marco Martin <notmart@gmail.com>
Date:   Mon Feb 28 13:47:09 2011 +0100

    set main object size after loading

commit 645885859430c03de5e41da91bd3edf00c85f0f3
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Mon Feb 28 01:48:12 2011 -0300

    Verify if DBus reply if valid before using it. Prevents potential crash.
    
    BUG: 252490

commit 87b0f2bee7863c87ae0c17062cea7f961234f6dc
Merge: ad7b88d 65d3890
Author: Ian Monroe <imonroe@kde.org>
Date:   Sun Feb 27 11:54:53 2011 -0600

    Merge remote branch 'origin/4.6' into origin/KDE/4.6

commit ad7b88dbcae4281b31d2ddf3f5735b8bb84ef6b8
Author: Marco Martin <notmart@gmail.com>
Date:   Sun Feb 27 16:59:35 2011 +0100

    correctly reposition the main item when resizes

commit 44b33646d35a096ac595cdb4ee597af28063aa59
Author: David Faure <faure@kde.org>
Date:   Sat Feb 26 01:19:15 2011 +0100

    Fix support for user-specified mimetype icons.

commit 99aa43c71c0595a4e4ea206fc298238cad3426d0
Author: David Faure <faure@kde.org>
Date:   Sat Feb 26 00:41:48 2011 +0100

    Fix parsing of user-modified mimetype comments
    
    We read global-then-local, so don't ignore newly found comment tags.
    Was a bug I introduced when refactoring this code in 24676ce.
    Unittest: FileTypesTest::testModifyMimeTypeComment in kde-runtime/keditfiletype.

commit 65d3890a41735b498e4fb338269ed12dae4eb35e
Author: Vishesh Handa <handa.vish@gmail.com>
Date:   Thu Feb 24 17:48:24 2011 +0530

    Donot create sparql expressions for empty literals
    
    This is a quick fix for the desktop query "hasTag:A and hasTag:B", which has an empty literal in the expression.
    Empty literals should not contribute to the sparql query.
    
    Conflicts:
    
    	nepomuk/query/literalterm.cpp

commit 497e3ee14b3d375984cce9d56feb09cb13782df1
Author: Vishesh Handa <handa.vish@gmail.com>
Date:   Thu Feb 24 17:46:57 2011 +0530

    Changes -
    * Renamed some variables. It's easier to read this way
    * Some comments
    * Minor optimization by moving the regular expression outside the loop
    
    svn path=/trunk/KDE/kdelibs/; revision=1217289
    
    Conflicts:
    
    	nepomuk/query/literalterm.cpp
