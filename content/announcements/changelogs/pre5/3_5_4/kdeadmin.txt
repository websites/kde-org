2006-05-23 15:51 +0000 [r544098]  mueller

	* branches/KDE/3.5/kdeadmin/ksysv/ServiceDlg.cpp,
	  branches/KDE/3.5/kdeadmin/ksysv/TopWidget.h,
	  branches/KDE/3.5/kdeadmin/ksysv/main.cpp,
	  branches/KDE/3.5/kdeadmin/ksysv/TopWidget.cpp: make it possible
	  to close ksysv BUG: 119631

2006-05-23 16:03 +0000 [r544103]  mueller

	* branches/KDE/3.5/kdeadmin/ksysv/ksv_core.cpp,
	  branches/KDE/3.5/kdeadmin/ksysv/ksv_core.h,
	  branches/KDE/3.5/kdeadmin/ksysv/TopWidget.cpp: fix crash on
	  startup BUG: 81927

2006-05-29 10:18 +0000 [r546152]  mueller

	* branches/KDE/3.5/kdeadmin/kcron/main.cpp: fix KApplication
	  invocation

2006-05-30 00:24 +0000 [r546425-546416]  jriddell

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  Fix WEP key https://launchpad.net/bugs/24516 Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kadddnsserverdlg.ui.h:
	  fix wrong warning about missing alias in DNS server add dialog
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/35507
	  Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconf.cpp:
	  before enable/disable interface check if settings have been
	  changed and ask user to apply
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/35509
	  Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kaddressvalidator.cpp:
	  fix crash in netmask/broadcast calculation in case of
	  empty/uninitialized fields
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/30775
	  Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  fix wrong gateway due to byte order on PPC
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/23750
	  Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  fix problem where interface without IP address was considered as
	  down/disabled and therefore preventing it do be enabled with ifup
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/38578
	  Patch by Luka Renko

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/knetworkconfigparser.cpp:
	  fix problem with unamanaged interface partially written to config
	  file
	  https://launchpad.net/distros/ubuntu/+source/kdeadmin/+bug/18069
	  Patch by Luka Renko <lure.net@gmail.com>

2006-06-10 15:06 +0000 [r549990]  lueck

	* branches/KDE/3.5/kdeadmin/doc/knetworkconf/index.docbook:
	  documentation backport from trunk CCMAIL:kde-doc-english@kde.org
	  CCMAIL:kde-i18n-doc@kde.org

2006-06-11 06:27 +0000 [r550121]  thiago

	* branches/KDE/3.5/kdeadmin/knetworkconf/knetworkconf/kaddressvalidator.cpp:
	  Fix the address validator. 255 is obviously a valid part of an IP
	  address. One should also note that the netmask validator is
	  incomplete. It should check for a sequence of 1's followed by a
	  sequence of 0's. Anything else is not a valid netmask. BUG:121991

2006-06-11 22:11 +0000 [r550478]  jbaptiste

	* branches/KDE/3.5/kdeadmin/knetworkconf/backends/platform.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/service.pl.in,
	  branches/KDE/3.5/kdeadmin/knetworkconf/backends/network-conf.in:
	  FEATURE: Added support for FC5. Thanks to Than Ngo.

2006-07-23 14:01 +0000 [r565464]  coolo

	* branches/KDE/3.5/kdeadmin/kdeadmin.lsm: preparing KDE 3.5.4

2006-07-24 07:57 +0000 [r565699]  kling

	* branches/KDE/3.5/kdeadmin/kpackage/packageProperties.cpp: Fixed a
	  double ++iterator bug introduced in r433729. Found by Robert
	  Kovacs. BUG: 131281

