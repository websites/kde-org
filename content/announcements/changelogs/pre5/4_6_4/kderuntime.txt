commit a33c02f58dc704bd9f74a1118ea774f2126f9ee4
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Jun 2 19:27:29 2011 +0200

    KDE 4.6.4

commit c3bc4dcce0eb678e536296e08a933dd1fbbd9add
Author: Dirk Mueller <mueller@kde.org>
Date:   Thu Apr 28 15:19:45 2011 +0200

    bump version to 4.6.3

commit f6f6d5458e9dc882b3c51e17fef23dc771c6754f
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 31 18:06:04 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit e5124596ff3812329f5d2f7699d18dc7fbd805e7
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue May 31 08:04:58 2011 -0400

    Correctly handle '%' in the typed-in string
    
    BUG: 246285
    FIXED-IN: 4.6.4

commit 9584f878ef1ddc19ed0410ec456d23ca5a946cda
Author: Sebastian Trueg <trueg@kde.org>
Date:   Wed May 25 12:00:09 2011 +0200

    Backport: Fixed folder updating - no more "all resources removed" signals.

commit f90b436732af68bcdffc6cbe21d37070e3c153b8
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 24 17:55:07 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit e7228bde4c4b257c53f370fe0d1c606e01aec551
Author: Script Kiddy <scripty@kde.org>
Date:   Mon May 23 16:07:49 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 01bef0f2b3ba4caad48e3ccc4b5d18e22045387f
Author: Script Kiddy <scripty@kde.org>
Date:   Sun May 22 16:44:48 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 9878c878911b907f341037b1028e82a4143b9436
Author: Script Kiddy <scripty@kde.org>
Date:   Fri May 20 16:07:15 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit ce90528a75e8a8a1a4ed695ae6a2d034a08a890a
Author: John Layt <john@layt.net>
Date:   Sun May 15 01:28:39 2011 +0100

    Fix KCM Locale crash when invalid language codes in kdeglobals
    
    The check for invalid or uninstalled language codes was not happening
    early enough as the language code was being used in the initial merge
    resulting in a null config which later crashed the am/pm init.
    BUG: 262488
    FIXED-IN: 4.6.4

commit 7fc267b306d7a6488c5e1ad55c27f3e0885fef20
Author: Script Kiddy <scripty@kde.org>
Date:   Fri May 6 16:15:11 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 87e6a5427759199a7c51d18c62f15eaec9a4df39
Author: Script Kiddy <scripty@kde.org>
Date:   Wed May 4 15:47:03 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 39b75552f89e6eef6e6ad6c637dc4663d58facd9
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Tue May 3 14:01:34 2011 -0400

    Don't call del when asked to overwrite a destination file in rename. Otherwise,
    the finished signal from calling del will cause the bug reported in 255296.
    
    BUG: 255296
    FIXED-IN: 4.6.4

commit aecf46446d2956473e216074f78304dabe7cb5dc
Author: Script Kiddy <scripty@kde.org>
Date:   Tue May 3 17:40:02 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit b0b7dfdfa7051dce7ed5c7b3a6e1921de30166de
Author: Sebastian Trueg <trueg@kde.org>
Date:   Tue May 3 10:47:52 2011 +0200

    Watch for file modifications in indexed folders.
    
    Added the KInotify::Modify flag for all indexed folders so that file
    modifications will result in the file indexer to update the file
    metadata in Nepomuk.
    
    REVIEW:101279

commit 659cef39d5997d5f85a8cfa5dc19c06ec0937ba5
Author: Sebastian Trueg <trueg@kde.org>
Date:   Fri Apr 8 14:21:16 2011 +0200

    Improved Strigi DBus API allowing to recursively update a folder.
    
    Previously there was no way for a client to specify how a folder should
    be updated. Now there is a new boolean parameter for indexFolder and
    updateFolder which specifies if the folder should be updated recursively
    or not.

commit d710e965ee78a2f10e93ce833b98c72206e549f5
Author: Script Kiddy <scripty@kde.org>
Date:   Sat Apr 30 16:09:09 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 7efdd945cc7ab4f0573b5561217724adbbcb85bd
Author: Dario Andres Rodriguez <andresbajotierra@gmail.com>
Date:   Fri Apr 29 15:25:17 2011 +0300

    Improve duplicates count on the Report Information Dialog
    
    In the Report Information dialog (duplicate selection page):
    - Do not show the "bug XXX was marked as duplicate of this one" comments.
      That should help people to read the important information (other cases explanation).
    - Count the crashes attached inline (by DrKonqi) as duplicates
      (when showing the duplicate count on the top of the report).
    
    (cherry picked from commit af4328e86343e50ddb8e1776a4797523cadf6f7c)

commit 49bab9f5a832deed08d302d48053ebf8d9cacd94
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Apr 29 16:44:49 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 2702e763ebf0de7b4bc7f3c3cf2ee506f65949c3
Author: George Kiagiadakis <kiagiadakis.george@gmail.com>
Date:   Thu Apr 28 20:37:09 2011 +0300

    Add drkonqi mappings for kde-telepathy applications.
    
    (Cherry-picked from a1a52cc1a0e9e948165c3a8f27e5a2e80d205c2b)

commit cedc262892865c8812305af030e5a33d8bf9a2dd
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 26 17:32:48 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit d704896a86b670da1894667f40ab0afbfe5153cf
Author: Script Kiddy <scripty@kde.org>
Date:   Mon Apr 25 15:20:48 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 574d6c753d663ec7bd628b7c49679623c30b8bf3
Author: Script Kiddy <scripty@kde.org>
Date:   Fri Apr 22 16:24:49 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 49a219564e97d55d2e1cd81218027dcea7d131ef
Author: Friedrich W. H. Kossebau <kossebau@kde.org>
Date:   Fri Apr 22 00:52:08 2011 +0200

    Fixes: remove device if last service is removed in UpnpNetworkBuilder
    
    was shadowed by a useless local var

commit b1fbfbb2bcc42c62bef982344f52f1c55670fb95
Author: Marco Martin <notmart@gmail.com>
Date:   Sun Apr 17 20:01:45 2011 +0200

    make connection to connectedSourcesChanged queued
    
    Changing to Qt::QueuedConnection delays setupData().
    this makes the dataChanged() signal arrive to qml only when the full
    batch of setData has been done.
    this in turn doesn't let incomplete data arrive to the model, making an
    incomplete role mapping.
    should fix the QML version of Lionmail
    CCMAIL:sebas@kde.org

commit d76640d967057e93f277e9631e1431ffb95152b5
Author: Burkhard Lück <lueck@hube-lueck.de>
Date:   Thu Apr 14 15:48:26 2011 +0200

    fix broken nepomuk ui files to make messages translatable
    Some nepomuk ui files have strings like <string extracomment="@info">some message</string>
    xgettext extracts this properly to msgctxt "@info" msgid "some message"
    ui compiler generates tr2i18n("some message", 0) -> that does not match xgettext, therefore no translated msg is found
    
    With <string comment="@info">some message</string>
    the ui compiler generates tr2i18n("some message", "@info"), that does match xgettext and the translated msg is found and shown
    REVIEW: 101112
    (cherry picked from commit 398f4964691e76f8d60e399c294aec282f3d6036)

commit 696c84245183acd9428d868b31743d74d23b5521
Author: Lamarque V. Souza <lamarque@gmail.com>
Date:   Thu Apr 14 06:57:40 2011 -0300

    Fix context list parsing in knotify.
    
    CCBUG: 230022
    (cherry picked from commit 48d5ef94a7b57d3bc4a218f719a187fcab0d863f)

commit 6101e998e76874341f96e15ca90b982962760af9
Author: Aaron Seigo <aseigo@kde.org>
Date:   Sat Apr 9 21:01:10 2011 +0200

    update API v#

commit 3e57c7352736919420b4ad46a106e079ed2e73c7
Author: Dawit Alemayehu <adawit@kde.org>
Date:   Wed Apr 6 10:15:40 2011 -0400

    Delete the item after removing it from the QList, not before.

commit 22b884ea6a836493b1e205679697bb921eec4ca8
Author: Script Kiddy <scripty@kde.org>
Date:   Wed Apr 6 14:31:39 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 6a6e2856d1f4f63649189feb216fb44e0fa9cffb
Author: Script Kiddy <scripty@kde.org>
Date:   Tue Apr 5 15:51:22 2011 +0200

    SVN_SILENT made messages (.desktop file)

commit 19a24c59d821d4bba840a6a65d88017d3ad35312
Author: Aaron Seigo <aseigo@kde.org>
Date:   Mon Apr 4 14:42:25 2011 +0200

    give connectAllSources the same magic sauce connectSource does
    
    thanks to emilsedgh for having the special super power of using precisely
    what hasn't been properly implemented ... and then reporting it with
    nice simple examples we can test with! THAT, my friends, is how it is done.

commit 83d0bdbb53fb27c0ce26c1875e3a3b75e9d323e1
Author: Andrius Štikonas <stikonas@gmail.com>
Date:   Sat Apr 2 22:06:02 2011 +0300

    Use possesive month name in long date format in Lithuanian language.
