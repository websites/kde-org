---
aliases:
- ../../fulllog_applications-18.04.2
hidden: true
title: KDE Applications 18.04.2 Full Log Page
type: fulllog
version: 18.04.2
---

<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Hide]</a></h3>
<ul id='ulcantor' style='display: block'>
<li>In R backend try to open files with unknown mimetype in Cantor script editor and add handling for command without results in rserver. <a href='http://commits.kde.org/cantor/3a57d5c04c20c4cb69a94559d18a311b92a16c0b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/296814'>#296814</a></li>
<li>Avoid segfault in backends destructor, when terminate related process. <a href='http://commits.kde.org/cantor/46238045f997f849d3f2d45cbacf5cd215e4578e'>Commit.</a> </li>
<li>Make Julia variables format in variables model more consistent with the other backends. <a href='http://commits.kde.org/cantor/8c7a0c37f0c7e7deb9f035339d0aecb2f5a819e4'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377771'>#377771</a></li>
<li>Change juliahighligter's regex for nonSeparatingCharacters. <a href='http://commits.kde.org/cantor/2462614eae755e295611c816afbaf357ffe6760d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377772'>#377772</a></li>
<li>Add unicode support in juliaserver (because julia can work with unicode, so juliaserver must too) and fix missing highlighting for functions for julia 0.6 and higher. <a href='http://commits.kde.org/cantor/384e51e1f9cf13b5e50729bf19883d60a15a26d4'>Commit.</a> </li>
<li>Fix julia output: change display function from jl_static_show to 'display'. <a href='http://commits.kde.org/cantor/26a0e4106a0d4633f9590bd182e44c3d5fb2171d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377773'>#377773</a></li>
<li>In LatexEntry::updateEntry path to esp file is constructed invalid. This commit fix it. During debuging, it was found, that EpsRenderer don't check, that input is valid, at all. So, the commit also add checking, that input file is valid eps file, and warning message printing, if it is not true. <a href='http://commits.kde.org/cantor/8f382c721828565a2893027b33ea081442cd6982'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/330834'>#330834</a></li>
<li>Clazy warnings, part 1. <a href='http://commits.kde.org/cantor/538559dd0ae5aa14d5187971a3072819265e6efe'>Commit.</a> </li>
<li>With this changes, maxima backend don't show source latex code for entries while they are generated. <a href='http://commits.kde.org/cantor/94db9695da931f228ed70bc96bea29d59ffffb9f'>Commit.</a> </li>
<li>Add support for "application/x-extension-html" mimetype in R expression, because some systems use this mimetype for html, instead of "text/html". <a href='http://commits.kde.org/cantor/50c130c20f984cb7f17e4d3081d72f45560cb2e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394018'>#394018</a></li>
<li>Fix creating temporary directory for R backend. <a href='http://commits.kde.org/cantor/fa3ffe28ced951ee22d7c887ab9311a93d6d983d'>Commit.</a> </li>
<li>Add html escaping for ampersand in rexpression. <a href='http://commits.kde.org/cantor/f6fe63b4054972273eb8b019f75d0315d4ef45a7'>Commit.</a> </li>
<li>Fix minor misspelling in debug message of maxima backend. <a href='http://commits.kde.org/cantor/e9bd5a076f7e25de1d9f5b546d0f243c0079d3cf'>Commit.</a> </li>
<li>Improve LaTeX worksheet export. <a href='http://commits.kde.org/cantor/c87f658564045387cb5e2426cce59a6973300196'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/387304'>#387304</a></li>
<li>Add GUI editor for R backend. <a href='http://commits.kde.org/cantor/b2ba96b6425a2a9ed84ee48dd938bc35af0a0c41'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339127'>#339127</a></li>
</ul>
<h3><a name='dolphin' href='https://cgit.kde.org/dolphin.git'>dolphin</a> <a href='#dolphin' onclick='toggle("uldolphin", this)'>[Hide]</a></h3>
<ul id='uldolphin' style='display: block'>
<li>Fix crash in PlacesItem::setUrl(). <a href='http://commits.kde.org/dolphin/588abbf1b6994245d987c0d07c2b074cb76fc034'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394507'>#394507</a></li>
<li>Add missing i18n call. <a href='http://commits.kde.org/dolphin/2e1cc199c3c2ef2eea5abd630821f80dfc9e0b16'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394194'>#394194</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Hide]</a></h3>
<ul id='ulgwenview' style='display: block'>
<li>Fix share menu not showing the first time it is used. <a href='http://commits.kde.org/gwenview/bb8522f62679d2df9d430248549bb58f87b796d1'>Commit.</a> </li>
<li>Watch for installation of kipi-plugins only when needed. <a href='http://commits.kde.org/gwenview/3a772173e287ef024e097fc87663f75300c47597'>Commit.</a> </li>
<li>Update thumbnail de-/select hover button on selection change. <a href='http://commits.kde.org/gwenview/5d3d973593c7cae19110952ba2944365a1bab623'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394406'>#394406</a></li>
<li>Update paste action on current directory and selection changes. <a href='http://commits.kde.org/gwenview/358ff25a4ab9855a6727546f355db428dd74f8cf'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/276255'>#276255</a></li>
<li>Cleanup code for finishing undo/redo of image operations. <a href='http://commits.kde.org/gwenview/e8e6d40b3d5978139c9ab1b37b9f4044db8553a5'>Commit.</a> </li>
<li>Enable redo for undone image operations. <a href='http://commits.kde.org/gwenview/cc6f795d88910d7516da85dfa548a6a0597f23ba'>Commit.</a> </li>
<li>Disable View mode shortcuts outside of View mode. <a href='http://commits.kde.org/gwenview/f0edb69f3063ab51c4d7a9f0e03d26223c1405b1'>Commit.</a> </li>
</ul>
<h3><a name='kamoso' href='https://cgit.kde.org/kamoso.git'>kamoso</a> <a href='#kamoso' onclick='toggle("ulkamoso", this)'>[Hide]</a></h3>
<ul id='ulkamoso' style='display: block'>
<li>Webcamcontrol: Solve crash when calling gst_element_set_state twice. <a href='http://commits.kde.org/kamoso/70d5cf3756608e8f295ceb731bfdf21183d02833'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394881'>#394881</a></li>
</ul>
<h3><a name='kcalutils' href='https://cgit.kde.org/kcalutils.git'>kcalutils</a> <a href='#kcalutils' onclick='toggle("ulkcalutils", this)'>[Hide]</a></h3>
<ul id='ulkcalutils' style='display: block'>
<li>Fix Bug 394410 - Copy link for the participants should be a url not a uid in the incidence viewer. <a href='http://commits.kde.org/kcalutils/2c4ef81fa75a230c5ddb3251056c79450ff69c56'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394410'>#394410</a></li>
</ul>
<h3><a name='kde-dev-scripts' href='https://cgit.kde.org/kde-dev-scripts.git'>kde-dev-scripts</a> <a href='#kde-dev-scripts' onclick='toggle("ulkde-dev-scripts", this)'>[Hide]</a></h3>
<ul id='ulkde-dev-scripts' style='display: block'>
<li>Tell releaseme to skip po install as we use no po files and this confuses KDE neon builds. <a href='http://commits.kde.org/kde-dev-scripts/f01ef869ecc7d254e1a72d395ab488c7aa5ac281'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Hide]</a></h3>
<ul id='ulkdenlive' style='display: block'>
<li>Don't display Wizard on every AppImage startup. <a href='http://commits.kde.org/kdenlive/bcfff134ad5cff3e1e1115c7012e2a5d330d418b'>Commit.</a> </li>
<li>Fix message extraction. <a href='http://commits.kde.org/kdenlive/2f329dd44fae1c5f2df7a5753da6cac5a731217d'>Commit.</a> </li>
<li>Prepare for 18.04.2 release. <a href='http://commits.kde.org/kdenlive/1ffb8e8fec2d439d230ef4f031f43b99b4600011'>Commit.</a> </li>
<li>Rename mime type file to make flatpak happy. <a href='http://commits.kde.org/kdenlive/47aaa52f440c22a0c97a2d5a07d8e0cb5b5ef787'>Commit.</a> </li>
<li>Fix crash with MLT 6.8.0 and some keyfameable parameters (box blur). <a href='http://commits.kde.org/kdenlive/9b09dbb2b0de78d4eaf4011c8a462fdb3c6916b6'>Commit.</a> </li>
<li>Consistent behavior for mouse wheel: down goes forward, up goes backwards. <a href='http://commits.kde.org/kdenlive/cdc60f75750a66664463a4a674a8fa131109ce82'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394341'>#394341</a></li>
<li>Update readme with correct infos. <a href='http://commits.kde.org/kdenlive/4ee96a0c704cc0c42e314f5e0ee1b15fac4f8795'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Hide]</a></h3>
<ul id='ulkdepim-runtime' style='display: block'>
<li>Resources/ews: Fix saving passwords to KWallet. <a href='http://commits.kde.org/kdepim-runtime/b5ee6ff11cd3a7e0c28305b351d28c5f244f101e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/393002'>#393002</a></li>
</ul>
<h3><a name='kfourinline' href='https://cgit.kde.org/kfourinline.git'>kfourinline</a> <a href='#kfourinline' onclick='toggle("ulkfourinline", this)'>[Hide]</a></h3>
<ul id='ulkfourinline' style='display: block'>
<li>Fix layouting so the contents are actually not lost. <a href='http://commits.kde.org/kfourinline/9685f6aef21bba3d139995c43710f9e447501ad3'>Commit.</a> </li>
<li>Fix crash when opening the network config dialog. <a href='http://commits.kde.org/kfourinline/60396093e4070bf0e19f3a47823caf39b1c2a057'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394728'>#394728</a></li>
</ul>
<h3><a name='kgpg' href='https://cgit.kde.org/kgpg.git'>kgpg</a> <a href='#kgpg' onclick='toggle("ulkgpg", this)'>[Hide]</a></h3>
<ul id='ulkgpg' style='display: block'>
<li>Fix KEY_CONSIDERED lines always resulting in TS_MSG_SEQUENCE errors. <a href='http://commits.kde.org/kgpg/2fe9414e2d4b14741ace4dd03781f36e436330f9'>Commit.</a> </li>
<li>Resolve decryption failure when message has no version header. <a href='http://commits.kde.org/kgpg/63b10dfb5fd93bf3dd22e04c62be83c8cf2d15f5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/357462'>#357462</a></li>
<li>Resolve decryption failure when message has no version header. <a href='http://commits.kde.org/kgpg/0d8c782c159cfcb51f78c7655029b72c1b7034c2'>Commit.</a> </li>
<li>Avoid needless string copy. <a href='http://commits.kde.org/kgpg/6118976ce725ddbfec268bc982de714f87eea80e'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Hide]</a></h3>
<ul id='ulkig' style='display: block'>
<li>Fix bug 394676. <a href='http://commits.kde.org/kig/31157d7d5850237cae555622a55cc7769d63de95'>Commit.</a> </li>
<li>Fix the value range of the rgb color pen in asymptote exporter. <a href='http://commits.kde.org/kig/0cb6f2b1d587cc9d84839924f0138aab66cf9daf'>Commit.</a> </li>
</ul>
<h3><a name='kimap' href='https://cgit.kde.org/kimap.git'>kimap</a> <a href='#kimap' onclick='toggle("ulkimap", this)'>[Hide]</a></h3>
<ul id='ulkimap' style='display: block'>
<li>Fix STARTTLS support detection. <a href='http://commits.kde.org/kimap/822a28901672efb01af5c7b9ee2c27f9fd35a2a1'>Commit.</a> </li>
<li>Fix TLS vs STARTTLS handling. <a href='http://commits.kde.org/kimap/b6c4ee82160da39ad7cff4c54360912f393aabd2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394769'>#394769</a></li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Hide]</a></h3>
<ul id='ulkio-extras' style='display: block'>
<li>Scale up folder icon before creating preview overlays. <a href='http://commits.kde.org/kio-extras/bafc90d730f2ad018ff440060e19b8affa074d31'>Commit.</a> See bug <a href='https://bugs.kde.org/315983'>#315983</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Hide]</a></h3>
<ul id='ulkmail' style='display: block'>
<li>Bug 232911 - "Reply Without Quote" does not open message composer anymore. <a href='http://commits.kde.org/kmail/0240ed9fab4cc229d4485c36adebf595db30c097'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/232911'>#232911</a></li>
<li>Fix show two icon when we use kontact. <a href='http://commits.kde.org/kmail/e727d739373aecdc37fef9948ff0f2f88dcffaa7'>Commit.</a> </li>
<li>Fix warning about shortcut. <a href='http://commits.kde.org/kmail/10ab845b8c86ddcc02a676b665b4886186e50df9'>Commit.</a> </li>
</ul>
<h3><a name='kolourpaint' href='https://cgit.kde.org/kolourpaint.git'>kolourpaint</a> <a href='#kolourpaint' onclick='toggle("ulkolourpaint", this)'>[Hide]</a></h3>
<ul id='ulkolourpaint' style='display: block'>
<li>Fix color palette cell size. <a href='http://commits.kde.org/kolourpaint/612ad92cdf872addab087ca23730a6daedec06a3'>Commit.</a> </li>
</ul>
<h3><a name='konsole' href='https://cgit.kde.org/konsole.git'>konsole</a> <a href='#konsole' onclick='toggle("ulkonsole", this)'>[Hide]</a></h3>
<ul id='ulkonsole' style='display: block'>
<li>Backspace without CTRL should send '^?'. <a href='http://commits.kde.org/konsole/c86aa6a0cb6e4d9e21d44589cab45875dae2dd9e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394276'>#394276</a></li>
</ul>
<h3><a name='kontact' href='https://cgit.kde.org/kontact.git'>kontact</a> <a href='#kontact' onclick='toggle("ulkontact", this)'>[Hide]</a></h3>
<ul id='ulkontact' style='display: block'>
<li>Fix Bug 394767 - Error message at start with Qt 5.11. <a href='http://commits.kde.org/kontact/b78bb7307dab374a5d97aad691c8faf9902db27a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394767'>#394767</a></li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Hide]</a></h3>
<ul id='ulkopete' style='display: block'>
<li>Cmake: remove extra "plugins". <a href='http://commits.kde.org/kopete/df00be6f07de8148a3b5aa74a803a20e40747478'>Commit.</a> </li>
<li>Fix installation of notifyrc file. <a href='http://commits.kde.org/kopete/824cca1ca3eb2f6936bca4a9914c0a964dae324b'>Commit.</a> </li>
<li>Remove the WLM protocol. <a href='http://commits.kde.org/kopete/c21991982f42e2b7be44fa6b3670208632efe842'>Commit.</a> </li>
</ul>
<h3><a name='korganizer' href='https://cgit.kde.org/korganizer.git'>korganizer</a> <a href='#korganizer' onclick='toggle("ulkorganizer", this)'>[Hide]</a></h3>
<ul id='ulkorganizer' style='display: block'>
<li>Koviewmanager.cpp - use the fallback when selecting the date for Day View. <a href='http://commits.kde.org/korganizer/7a2df1e6ed39cc5a752898bd1b90099418a0ce58'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394416'>#394416</a></li>
</ul>
<h3><a name='ksmtp' href='https://cgit.kde.org/ksmtp.git'>ksmtp</a> <a href='#ksmtp' onclick='toggle("ulksmtp", this)'>[Hide]</a></h3>
<ul id='ulksmtp' style='display: block'>
<li>Fix TLS vs STARTTLS handling. <a href='http://commits.kde.org/ksmtp/98b575dceadaed6faeb7fa302b75f1d08959126f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394770'>#394770</a></li>
</ul>
<h3><a name='libksieve' href='https://cgit.kde.org/libksieve.git'>libksieve</a> <a href='#libksieve' onclick='toggle("ullibksieve", this)'>[Hide]</a></h3>
<ul id='ullibksieve' style='display: block'>
<li>Don't add an item with same name. <a href='http://commits.kde.org/libksieve/64d73a3902129c15e30139610a0f271cb0030a6c'>Commit.</a> </li>
<li>Allow to select multi emails. <a href='http://commits.kde.org/libksieve/a71d9a446b25fb1fe2bcb929ccc3aeec2db1fbb4'>Commit.</a> </li>
<li>Use KSieveUi::AbstractSelectEmailLineEdit. <a href='http://commits.kde.org/libksieve/3b4912eda55eeebf102f898e9fd8228e3f2d3060'>Commit.</a> </li>
<li>Parse script here too. Otherwise it's always active even if we disable code. <a href='http://commits.kde.org/libksieve/a15b219b187b55b45214e95608b58b4149d5664e'>Commit.</a> </li>
<li>Don't create two connect/slot. <a href='http://commits.kde.org/libksieve/c6733c628a2068280abb6ac0081597cc89bcbfe9'>Commit.</a> </li>
</ul>
<h3><a name='lokalize' href='https://cgit.kde.org/lokalize.git'>lokalize</a> <a href='#lokalize' onclick='toggle("ullokalize", this)'>[Hide]</a></h3>
<ul id='ullokalize' style='display: block'>
<li>Fix alignment of numeric columns for RTL languages. <a href='http://commits.kde.org/lokalize/83590be3996e2c2013f69ce8a850f508c4db9d53'>Commit.</a> </li>
<li>Right-align all translation count columns in Project Overview. <a href='http://commits.kde.org/lokalize/c97fa50b0b31c905cfb7a2bfacb679e747c92547'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394512'>#394512</a></li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Hide]</a></h3>
<ul id='ulmessagelib' style='display: block'>
<li>Ensure we always reset the external reference override. <a href='http://commits.kde.org/messagelib/9669e2622ee26ac748d64b567562889ad5f190ef'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394554'>#394554</a></li>
<li>QtWebEngine 5.11 can intercept more messages include local and data ones. <a href='http://commits.kde.org/messagelib/14063ca517b2c4a370e60049b2a37da86e32c11c'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Hide]</a></h3>
<ul id='ulokular' style='display: block'>
<li>Force release of free memory. <a href='http://commits.kde.org/okular/95bc29a76fc1f93eaabe5383d934644067dfc884'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394834'>#394834</a></li>
<li>Use a target in FindDiscount (modern cmake) rather than variables. <a href='http://commits.kde.org/okular/3e48663fb764f60db63c9272f28258fcf06eb7fb'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Hide]</a></h3>
<ul id='ulumbrello' style='display: block'>
<li>Fix missing keyboard shortcut for "line with" label in style page. <a href='http://commits.kde.org/umbrello/3740b442c2f908dae928a0227c281fe20d964d88'>Commit.</a> See bug <a href='https://bugs.kde.org/86084'>#86084</a></li>
<li>Disable deprecated warnings. <a href='http://commits.kde.org/umbrello/0f752a9779e183d79f616669cefe487d94718a7f'>Commit.</a> </li>
<li>Add <launchable> entry to umbrello appdata xml file. <a href='http://commits.kde.org/umbrello/163a0bed29d443a3456eeb019da2e0bc53535149'>Commit.</a> See bug <a href='https://bugs.kde.org/391559'>#391559</a></li>
<li>Fix 'Artifact subdir name includes root path in tree view' on Windows. <a href='http://commits.kde.org/umbrello/e56b74070b2a1bf56a78b76edbfc3fdec1e235b0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/394446'>#394446</a></li>
</ul>