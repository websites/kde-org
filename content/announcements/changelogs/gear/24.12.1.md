---
aliases:
- ../../fulllog_releases-24.12.1
title: KDE Gear 24.12.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Send entire Tags in ItemsTagsChanged notification. [Commit](http://commits.kde.org/akonadi/56575c4bc623c63ab4bc8a686fc973b27769cdb5).
+ Group ItemsTagsChanged notification on tag deletion by collection. [Commit](http://commits.kde.org/akonadi/48a31af869ea0c28803de468f2adf2c90d89bb09).
+ TagDeleteHandler: perform deletion in DB transaction. [Commit](http://commits.kde.org/akonadi/e09935cea6607a562bfdf2e4b00628819e770f3e).
{{< /details >}}
{{< details id="akonadiconsole" title="akonadiconsole" link="https://commits.kde.org/akonadiconsole" >}}
+ Add back removedTags. [Commit](http://commits.kde.org/akonadiconsole/2413cb2574cdcab38f338aea1adb95c8988c5a5a).
+ Make compile without akonadi change. [Commit](http://commits.kde.org/akonadiconsole/77d439e99b930dd2c310e555cc163aa3763d2bee).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix focus changing when unminimising on X11. [Commit](http://commits.kde.org/dolphin/a57ffc9f1ece7cf196691ec878383bcc20903da7). Fixes bug [#497803](https://bugs.kde.org/497803).
+ Dolphinview: Update thumbnail on filename change. [Commit](http://commits.kde.org/dolphin/0fe6544079a23769e8f19ee2f09b5c149f404d5a). Fixes bug [#497555](https://bugs.kde.org/497555).
+ ViewProperties: Return nullptr if viewPropertiesString is empty. [Commit](http://commits.kde.org/dolphin/86609f89358243c08ebe4de8498a0fa6dff8370e). Fixes bug [#495878](https://bugs.kde.org/495878).
+ DolphinViewContainer: make sure searchbox is initialized before setSearchPath. [Commit](http://commits.kde.org/dolphin/edfd598446cfd6d4348a3bfecd72b5665eef5bfc). Fixes bug [#497021](https://bugs.kde.org/497021).
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix out of bounds access on MediaPlayListProxyModel::mapRowToSource. [Commit](http://commits.kde.org/elisa/c5d6eee204718bc4b6215b05ea601bf61c1d977d).
+ Fix APK build with Gradle Android plugin 8.6.0. [Commit](http://commits.kde.org/elisa/a5303defb75c49bba81d67cce1cffda096893b8a).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Ignore cancelled elements when determining weather forecast length. [Commit](http://commits.kde.org/itinerary/6d1aa7bd7bee79e8ecdceb7b036eefef9220ec9f).
+ TripGroupPage: Add separator in first FormCard. [Commit](http://commits.kde.org/itinerary/21182d94c97624b075f25023fed6b7e90a97b2b4).
+ Override instead of merge destination names when applying new journeys. [Commit](http://commits.kde.org/itinerary/b0c53098e1f1a5155887abb617521a30dfefe55f). Fixes bug [#474096](https://bugs.kde.org/474096).
+ Fix adding DST info elements in single trip group mode. [Commit](http://commits.kde.org/itinerary/a3e2723a5887fc0479271b75ca1737d2ac02a59b).
+ Fix crash on importing health certificates. [Commit](http://commits.kde.org/itinerary/825ba858f63e8beef8a1c987468a681a836bc6b9).
+ Use FormCardButton in SheetDrawer. [Commit](http://commits.kde.org/itinerary/a9cf29637bfd1c7a51b00be0f9ec784574379006).
+ Optimize CountryComboBox. [Commit](http://commits.kde.org/itinerary/78a3c6821fcdab21064ca925cbbf5f115e1344ad).
+ Allow to add transfers in more cases. [Commit](http://commits.kde.org/itinerary/034ea7f5782068187e73bad327309a38d7e1336e).
+ Unify checks for *.itinerary files. [Commit](http://commits.kde.org/itinerary/567608379d141bc197a72ad6ce43360f5b811f08).
+ Don't show the welcome page again when there's already a trip group. [Commit](http://commits.kde.org/itinerary/278ba626f1323b2fac1bfd7cece8d54ab1d2cdd4).
+ Reduce Appium reservation test even more aggressively in 24.12. [Commit](http://commits.kde.org/itinerary/e2b954fc8534835ff4ccf1d6202d8cdcf3a1f73b).
+ Make Appium reservationtest reliably pass again. [Commit](http://commits.kde.org/itinerary/4cad36e3f6415dd8719b1ea94a4442faa4bd68b9).
+ Exclude FluentWinUI3 QtQuick Controls style from the APK as well. [Commit](http://commits.kde.org/itinerary/0e2f2fbca37d9e893a7fd3900bb3e1e68ec19e1f).
+ Exclude unused Qt translation catalogs. [Commit](http://commits.kde.org/itinerary/0f22fa5f1e7d52d9eb1f5f81cf5da43c3d0fac9c).
+ Build release packages against released dependencies. [Commit](http://commits.kde.org/itinerary/2e8167bb9f1e7a8361eb55a7f48785ba0f9f0049).
+ Improve look of RadioSelector. [Commit](http://commits.kde.org/itinerary/0e546f34e8495aed9cad57b8cc93a10fe5fde142).
+ Remove useless spacing. [Commit](http://commits.kde.org/itinerary/33e5dfba8ab542037520269113e9493189402e02).
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Fix build on FreeBSD if using MPV audio. [Commit](http://commits.kde.org/kalarm/1fa6b4ced6a6ec526cceea9d3eee9f11836b0035).
+ Update appstream. [Commit](http://commits.kde.org/kalarm/87eeb6dc40267afe78186b774edba1a80599fb27).
+ Bug 497960: Fix Edit Alarm dialog error setting time if user's time zone is UTC. [Commit](http://commits.kde.org/kalarm/e44424ad52e2875dea167da6a7db4e8b7f5e14c7).
+ Make fade work each time a repeated sound alarm is played. [Commit](http://commits.kde.org/kalarm/0833113fe6904db03b6dbd70b5c62907807e59e3).
+ Fix Akonadi plugin not being recognised. [Commit](http://commits.kde.org/kalarm/330ee3dde93b074fb3cedddd9a7f9ad09abd3f85).
+ Improve diagnostic messages. [Commit](http://commits.kde.org/kalarm/05e22a841e4baea79e72a99288d5c48360e0437f).
+ Update version. [Commit](http://commits.kde.org/kalarm/a7bcf9422d706fe004c018fa9e3e92f2d499153f).
+ Remove hack to enforce message window visibility on X11. [Commit](http://commits.kde.org/kalarm/3fc9ea40d3ff3e7c4e67d0f8747080d5fe057a2e).
{{< /details >}}
{{< details id="kalm" title="kalm" link="https://commits.kde.org/kalm" >}}
+ Fix Linux CI build for GCC < 14. [Commit](http://commits.kde.org/kalm/05a5509934a330bce204dc4b398315a50aef6761).
+ Fix Linux CI build. [Commit](http://commits.kde.org/kalm/6419d19e3a31605eac5f5ea05cd6be173fb7ceca).
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Retrieve duration from file as soon as it's downloaded. [Commit](http://commits.kde.org/kasts/e67b9ed3bbbfc52b5c835981cebbf4a2635d4d41). Fixes bug [#497448](https://bugs.kde.org/497448).
+ [snap] Add password-manager-service plug. [Commit](http://commits.kde.org/kasts/6a765c90005701729b13c4f04d8ee661ded8d1cf). Fixes bug [#497049](https://bugs.kde.org/497049).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Build plugin: Add the \<pre\> tag to all lines. [Commit](http://commits.kde.org/kate/72cd809d53fae3615b909b36fd0b0577d0d4b913).
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Plugins/findthisdevice: fix broken qml config page. [Commit](http://commits.kde.org/kdeconnect-kde/cb8a3ae35ba3308a1aba6195367862b00a7914ad).
+ [kio] Don't query mountpoint twice. [Commit](http://commits.kde.org/kdeconnect-kde/47ad9d182d0f2650b464a81659d7f4250aceedc7).
+ [kio] Handle error when getting mount point. [Commit](http://commits.kde.org/kdeconnect-kde/be86637a4f71693b29288e93f7f3bedde95ac4a7). See bug [#490827](https://bugs.kde.org/490827).
+ Add icons to the app plugin config list. [Commit](http://commits.kde.org/kdeconnect-kde/44079d65e980a6054e1b6feb9f182c8e6718fe76).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Ensure sequence clips in timeline are not resized to smaller when hiding a track. [Commit](http://commits.kde.org/kdenlive/48130ff9d32c9927c82d40efa96353d5816c6039). Fixes bug [#498178](https://bugs.kde.org/498178).
+ Fix crash moving build-in effect with feature disabled. [Commit](http://commits.kde.org/kdenlive/185bbd3c3596da3454d74e2cd380f4c7e10aebb1).
+ Fix crash saving effect stack. [Commit](http://commits.kde.org/kdenlive/df78e4d743131b9eb4f100406678589dc4ca6cba). Fixes bug [#498124](https://bugs.kde.org/498124).
+ Fix layout order with > 9 layouts. [Commit](http://commits.kde.org/kdenlive/194abb6c66605211c80032bbc2023a500cbbc2d9).
+ Fix use after free from last commit. [Commit](http://commits.kde.org/kdenlive/d8a10d0066c6d68861606dc6e15e189dda22c48e).
+ Fix reload or proxy clip losing tags, markers, force aspect ratio. [Commit](http://commits.kde.org/kdenlive/a55916bb13b7665b0534b1a8226e5bf381f7640b). Fixes bug [#498014](https://bugs.kde.org/498014).
+ Fix bin clips effects sometimes incorrectly applied to timeline instance. [Commit](http://commits.kde.org/kdenlive/0247732d34a2ed24abf6a6a97dffe2f3b7b5b89a).
+ Fix typo. [Commit](http://commits.kde.org/kdenlive/73d32a83c5c10eb664e17e5a257f973c1fce7bde). Fixes bug [#497932](https://bugs.kde.org/497932).
+ Fix title widget braking text shadow and typewriter settings. [Commit](http://commits.kde.org/kdenlive/b937e269a4a6bf240cb9918b7f3de21bde723f63). Fixes bug [#476885](https://bugs.kde.org/476885).
+ Math operators not supported in xml params. [Commit](http://commits.kde.org/kdenlive/abc7fe554dc84c07d3fb077029085a9435821b64). Fixes bug [#497796](https://bugs.kde.org/497796).
+ Fix track resizing. [Commit](http://commits.kde.org/kdenlive/e0720e6f3b9b1215297f774c75a2962d31049b8f).
+ Fix bin effects cannot be removed from timeline instance. [Commit](http://commits.kde.org/kdenlive/1917dadb7a5e9f01b46fecfab2e60a9fa9fd233f).
+ Fix crash trying to move bin effect before builtin effect. [Commit](http://commits.kde.org/kdenlive/f4df94f8829f7bda8ce69148343a3528ff74850b).
+ Fix venv packages install on some distros. [Commit](http://commits.kde.org/kdenlive/e9f4bdc68d5a6c77023ead33fe6c1cfdff754fb0).
+ Fix Whisper models download. [Commit](http://commits.kde.org/kdenlive/38136c9afbe3921db76233833f48009134c385e9).
+ Fix delta display when resizing clip, add duration info when resizing from start. [Commit](http://commits.kde.org/kdenlive/114618350ac10783a2510643069c7eb708b9b0fd).
+ Fix line return when pasting text with timecodes inside project notes. [Commit](http://commits.kde.org/kdenlive/27e4850cb62bc205cee8dce7930cbfd9a02dda3b).
+ Fix transparent rendering ffv1 profile. [Commit](http://commits.kde.org/kdenlive/93731dc0dba4223cc73eea3c66cb364f7ee9f748).
+ Bring back presets to build in effects. [Commit](http://commits.kde.org/kdenlive/3ed05ebe2a3b5b18e6c3cdf19a9e9aa73bafc878).
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Use libmessageviewer6. [Commit](http://commits.kde.org/kdepim-addons/abc5f0f3b9a6108f0a8833737edb6b11eae0191a).
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Remove superfluous debug messages. [Commit](http://commits.kde.org/kdepim-runtime/f8b745f45dd82f093aa940a0167efe4c6d7f29e9).
+ Dav: fix updating categories on tags change. [Commit](http://commits.kde.org/kdepim-runtime/798ce14fada1b1f9f051135945274b60e1058480).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Recentlyused: avoid fastInsert twice UDS_ACCESS_TIME. [Commit](http://commits.kde.org/kio-extras/1e5c0ffdeaa3b2bfdb90439d180ad2d33909ed32).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Fix: fix bus number extract and brand for SlovakLines extractor. [Commit](http://commits.kde.org/kitinerary/d94c0ca5bd0cc8bdac94ef29c26a4e91256ab888).
+ Add: slovak locale for SlovakLines pdf ticket parser. [Commit](http://commits.kde.org/kitinerary/3f150e5d6faa0aeeb108aa43e68f5d1f29b083c2).
+ Add: Fix support for Slovak Lines Partner tickers. [Commit](http://commits.kde.org/kitinerary/4bebdf0a6a61c7072957cc8c1a40ab7c2c4a2ac9).
+ Fix compilation against Poppler 25.01. [Commit](http://commits.kde.org/kitinerary/7e121d05f294d910c6a5e85cad37cb4f133085b8).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Fix build on macOS due to not having Wayland (variant 2). [Commit](http://commits.kde.org/kleopatra/15fd4482574fd364610a81f3541acb651f5aa125). Fixes bug [#497136](https://bugs.kde.org/497136).
+ Fix crash when output directory for decryption doesn't exist. [Commit](http://commits.kde.org/kleopatra/82ed3b5bc40e951e41534d3d850df009c15a332b).
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Clean up. [Commit](http://commits.kde.org/kmail/e4deccfc5c8fb6ce023a47e3563b678ce1450b37).
+ Fix typo. [Commit](http://commits.kde.org/kmail/2bb5cbb21715ceb28b2ea495f925ce18e33bae35).
+ We don't have invitation support in 24.12. [Commit](http://commits.kde.org/kmail/4a21a925f0a4f2a4b9240352c642efbb3a91b190).
+ Fix support for invitation. [Commit](http://commits.kde.org/kmail/ab43b556505123c93ef760e5001e0e0db7400819).
+ Use patternNew directly. [Commit](http://commits.kde.org/kmail/02454dcc70cb2f662031a602207e6d57933329e5).
+ Implement slotRequestFullSearchFromQuickSearch. [Commit](http://commits.kde.org/kmail/8d314ac1e729fca1df09941ed148d0ee3b8779ad).
+ Start to use list of infos. [Commit](http://commits.kde.org/kmail/beac5c8d7d8bf338ceaa8222024ba9a0594b90ab).
+ Prepare to use MessageList::Core::SearchLineCommand::SearchLineInfo. [Commit](http://commits.kde.org/kmail/f30fee1f78e3cb32d4364f3a46c09dda0cd09234).
{{< /details >}}
{{< details id="kongress" title="kongress" link="https://commits.kde.org/kongress" >}}
+ Ignore the new Fluent QQC style in APKs. [Commit](http://commits.kde.org/kongress/2927761996ec3bda2c771e35f9ad4d696ae4bf6b).
+ Show speaker name(s). [Commit](http://commits.kde.org/kongress/77b334907e65258809ee2d03621a03d2f09d7b54).
{{< /details >}}
{{< details id="kontrast" title="kontrast" link="https://commits.kde.org/kontrast" >}}
+ Mark one screenshot as default. [Commit](http://commits.kde.org/kontrast/784c21db316e8e99442c01d53099fd1bfa2f1f56).
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Prevent crash when updating Watched Nicks. [Commit](http://commits.kde.org/konversation/5da1a32c95f24ea5743ff932f320a6cbc55beaab). Fixes bug [#497799](https://bugs.kde.org/497799).
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Don't draw unintended outlines around waterway areas. [Commit](http://commits.kde.org/kosmindoormap/b6849261b52ecfc33990bab86d7532fd30471d0a).
+ Add infrastructure for determining close way types. [Commit](http://commits.kde.org/kosmindoormap/18deaf0282a192f924e955242780ecf35230ea82).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Fix determining station name for arrival queries. [Commit](http://commits.kde.org/kpublictransport/1d70cb884219f7c78af5f875552719b16b19af17).
+ Add support for DB's proxied Hafas API. [Commit](http://commits.kde.org/kpublictransport/0c2aed18364bbebada3e9a8e05a2407aa5a64be9).
+ Namespace Hafas identifiers for locations. [Commit](http://commits.kde.org/kpublictransport/53bc5d8832122393fc98cf452834cb81ab0adb57).
+ Make Hafas remark metadata externally accessible. [Commit](http://commits.kde.org/kpublictransport/07022ea16f2cb833cc8735ecc8f842bfeaf1e12f).
+ Isolate test from cached data on the host. [Commit](http://commits.kde.org/kpublictransport/7785a9d7a7939134e5173ad0412f6c0132640781).
+ Factor out Hafas load level parsing. [Commit](http://commits.kde.org/kpublictransport/262e428be5a93621ede8909e2ac80caf00d1cffd).
+ Add Hafas location identifier parser. [Commit](http://commits.kde.org/kpublictransport/2d23f737427961365146401839c6f74392c0a195).
+ Adapt to changes in the MOTIS v2 vehicle parking modes. [Commit](http://commits.kde.org/kpublictransport/e26de6ab3688bdea7c5af4aecbcd2ed1e912729c).
+ Ensure WALK is always in the access/egress modes for MOTIS v2 requests. [Commit](http://commits.kde.org/kpublictransport/aa3579122e69a3bf02df77760abd76b5d7697ec6).
+ Adapt to MOTIS v2 rental vehicle form factor API changes. [Commit](http://commits.kde.org/kpublictransport/2e8b76af132540c276fc8fa175a98e713215a50a).
+ Improve merging of features with conditional/limited availability. [Commit](http://commits.kde.org/kpublictransport/3fd63673318f47b6c8485c18bfaf66d58ea2106c).
+ Propagate disruption effects when setting arrival/departure stopovers. [Commit](http://commits.kde.org/kpublictransport/3583ec37d9bfca6f3378cf70cd235ec40577f847).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Cherry pick 334126b0 and fix most frequent crash on 24.12. [Commit](http://commits.kde.org/krdc/549a3a310ab8de41f383356f8dda86d797ce5b9e).
+ Add the nativeVirtualKey value to fake keyrelease events generated to unpress modifiers. [Commit](http://commits.kde.org/krdc/e1576b40e0fe36c9eda610c74fc524bbffd2b7d4). Fixes bug [#497111](https://bugs.kde.org/497111).
+ Wayland: fix "grab keys" when switching from/to fullscreen. [Commit](http://commits.kde.org/krdc/06ac905e493a3c72ecd6d0fa750b3e39bc8130b0).
+ Link vnc and rdp plugin with libnetwork on Haiku. [Commit](http://commits.kde.org/krdc/c842122763dc8c2cff32f4620e6572a640e4c529).
{{< /details >}}
{{< details id="libkdepim" title="libkdepim" link="https://commits.kde.org/libkdepim" >}}
+ Fix crash on exit if mailcheck is happening. [Commit](http://commits.kde.org/libkdepim/e7435bc6d80b5a9c170d209928586c5295e646b1). Fixes bug [#491769](https://bugs.kde.org/491769).
{{< /details >}}
{{< details id="lokalize" title="lokalize" link="https://commits.kde.org/lokalize" >}}
+ Remove workaround for bug 494500 and add an assert. [Commit](http://commits.kde.org/lokalize/48e21ad8abd61776bf38929ce84fc56fa5470c69).
+ Do not return QVector with an empty AltTrans. [Commit](http://commits.kde.org/lokalize/b79e41273df1360bb11fa94dc953c6cf8b25bbff). Fixes bug [#494500](https://bugs.kde.org/494500).
{{< /details >}}
{{< details id="marble" title="marble" link="https://commits.kde.org/marble" >}}
+ Fix translation for Marble Maps. [Commit](http://commits.kde.org/marble/86c5f857e69885501d5da23e9f57caee29e7e69a).
+ Remove usage of some Qt CMake usage with ECMQmlModule. [Commit](http://commits.kde.org/marble/67b576cd11531ad4ee91767a634e8de61f405a33).
+ Don't build behaim or marble-maps targets in non-KF builds. [Commit](http://commits.kde.org/marble/3b7b361883bc8120108868ed779d31aa49335ca0).
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix some translations not being properly used. [Commit](http://commits.kde.org/messagelib/3e141ca2041c7a0cd0d916a41a600b70aec689ab).
+ Add IsSpam/IsHam. [Commit](http://commits.kde.org/messagelib/6b621269edd69139c20f7273e83b6c41e04215c4).
+ Add IsRead/IsIgnored. [Commit](http://commits.kde.org/messagelib/0811e3519060a92ba2b4ace457711a502b9ff8e2).
+ Fix typo. [Commit](http://commits.kde.org/messagelib/9ed3defafe97d6701095cc78495df3af6047d5d1).
+ Create SearchLineCommand::SearchLineInfo from mStatus. [Commit](http://commits.kde.org/messagelib/96aee3ce5e675b27ec83454f2ca4a3f95eb84e5b).
+ Use status/option. [Commit](http://commits.kde.org/messagelib/841ae9005f4f0598d63bb3872033f400e5011ba4).
+ Return essageList::Core::SearchLineCommand::SearchLineInfo. [Commit](http://commits.kde.org/messagelib/fbc9698adb1aa51c343445e2ad59d89cf6377749).
+ Prepare to fix get search info from messagesearchline. [Commit](http://commits.kde.org/messagelib/bc7d37d12fa0c4527d3e1cc36de9b2274fc54228).
+ Allow to install SearchLineCommand. [Commit](http://commits.kde.org/messagelib/29a07249296815de5e1aa6de32ff12437537314e).
+ We use search command line by default now. [Commit](http://commits.kde.org/messagelib/c1c60645f8146040a032f0be279f0a0871883b91).
+ Add current folder. [Commit](http://commits.kde.org/messagelib/3cf23ca728da416cf47afc86167bbb27a6f520af).
+ Don't duplicate searched text. [Commit](http://commits.kde.org/messagelib/71d0b737271562ed53dc4cf14e90e6036b4dd73f).
{{< /details >}}
{{< details id="mimetreeparser" title="mimetreeparser" link="https://commits.kde.org/mimetreeparser" >}}
+ Use mimetreeparser6. [Commit](http://commits.kde.org/mimetreeparser/2ed040c953eec99e95859f09ea4df07472c742db).
{{< /details >}}
{{< details id="minuet" title="minuet" link="https://commits.kde.org/minuet" >}}
+ Call onExited in a way that is compatible with Qt5 and Qt6. [Commit](http://commits.kde.org/minuet/73302b562e87ecb33cb95d29cadc0201f8d38a5f).
{{< /details >}}
{{< details id="neochat" title="neochat" link="https://commits.kde.org/neochat" >}}
+ Use symbolic icon for purpose plugin. [Commit](http://commits.kde.org/neochat/d89019d75256a733d21729ed055a5cfec573ca98).
+ Explicitly set the parent in QuickSwitcher. [Commit](http://commits.kde.org/neochat/d7451834f35d0b20a351cb022c2228d49e9e9096).
+ Port away from methods removed in libquotient. This fixes BUG: 497458. [Commit](http://commits.kde.org/neochat/4c43869fd4ae29da39f8d783ccfa63bc95f3d8b4).
+ [UserInfo] Fix shortcut. [Commit](http://commits.kde.org/neochat/369242ab31954b499fdc048ada0c4e9d9a678ec4).
+ Add better support for colored text (and shrugs) from other clients. [Commit](http://commits.kde.org/neochat/a046e3ed27afc8a29c37bf03837080dcb342dedd).
+ Fix web shortcuts not doing anything. [Commit](http://commits.kde.org/neochat/5b935c1d33197d3e1217f6bb9819b894df240702). Fixes bug [#496434](https://bugs.kde.org/496434).
+ Don't set emoji size to font size. [Commit](http://commits.kde.org/neochat/63206ef1dd076cda9bcbf5097c49c009fba1ea35).
+ Fix crash when sending messages. [Commit](http://commits.kde.org/neochat/0d286db0c28beddf165e5b3e3a5d9a1510e1d357).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Fix file name of the mobile djvu desktop file. [Commit](http://commits.kde.org/okular/f80a0038af7f4077dddc78685138004bc13de780).
+ Only install relevant desktop files. [Commit](http://commits.kde.org/okular/32484a7a2cfaa9e076fa7f4ecb91842489b57368).
+ Fix scroll down/up at the last/first page in single page, non-continuous mode. [Commit](http://commits.kde.org/okular/9f37dd5d9b7e7b2023bb511c191d45870c2117d4). Fixes bug [#498038](https://bugs.kde.org/498038).
+ Fix Q_UNREACHABLE being reached with poppler 24.12. [Commit](http://commits.kde.org/okular/76d1c9794d8de3b970d4027a0cfbd79b63a1527e).
+ Disable performance-enum-size clang-tidy warning. [Commit](http://commits.kde.org/okular/89d38448abeb90fffc34e1b4afa6ddae4c186d76).
+ Move to avoid copy. [Commit](http://commits.kde.org/okular/84bed7443d202ecd6bfc1e3f38d7dd6e37cc302b).
+ Make clang-tidy happy when using string_view.data(). [Commit](http://commits.kde.org/okular/53a345565e1bf12cea1c73d38a463c41b1b86e52).
+ Cast pointers to pointer to void to make clang-tidy happy. [Commit](http://commits.kde.org/okular/8169f909754b024956e9469565f9fdefd5616971).
+ Add missing braces. [Commit](http://commits.kde.org/okular/a2546bc48000796c00ed76c16087863e5b7841c5).
+ Fix parameter name. [Commit](http://commits.kde.org/okular/bd332bc585ea9b8ba4dcdc70d4c0e0de2bea98d5).
+ Allow void as a way to silence bugprone-unused-return-value. [Commit](http://commits.kde.org/okular/9e60dc12dc1681b00308eddf0e5669353e8aeefc).
+ Remove deref no-op function. [Commit](http://commits.kde.org/okular/00fa7383b6970684045faa706bc10de1db15f837).
+ Fix clang-tidy warning. [Commit](http://commits.kde.org/okular/209f670a664508506c311117a1289447f4a4ab78).
+ Remove std::move() for raw pointer. [Commit](http://commits.kde.org/okular/708d94b2d73df7b358e23f997f29316ce4da8134).
+ Add braces around single-line if/else. [Commit](http://commits.kde.org/okular/caa893ee7fe6ffbed062a039121a0ce042b29bab).
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Assume we're online if there's no valid QNetworkInformation. [Commit](http://commits.kde.org/pimcommon/b7fb4271cc331795b980efb31628d0d01214caf4).
{{< /details >}}
{{< details id="step" title="step" link="https://commits.kde.org/step" >}}
+ Enable qalculate, which is not a Qt library. [Commit](http://commits.kde.org/step/32ef71cac432dd61d7f631e88dece23721a793e9).
{{< /details >}}
{{< details id="telly-skout" title="telly-skout" link="https://commits.kde.org/telly-skout" >}}
+ Add 24.12.1 release description. [Commit](http://commits.kde.org/telly-skout/cdc74c20ff64ca162d8061b1648525725242096b).
+ TV Spielfilm fetcher: fix fetching of channels. [Commit](http://commits.kde.org/telly-skout/94cf5d1db196bb32e98b4a8efd17fdbde9212c4e).
+ Fix loading placeholder when loading channels. [Commit](http://commits.kde.org/telly-skout/1736a91d1c8477158204180c72f70bbca302d5bd).
+ "Favorites" page: do not show refetch button while loading. [Commit](http://commits.kde.org/telly-skout/ab24c59689db052fec947f3bade27ae5bd3e9ed4).
+ TV Spielfilm fetcher: use i18n placeholders for description. [Commit](http://commits.kde.org/telly-skout/6d6a322d0538752d2452b220b2891e3ef8a7fe7a).
+ TV Spielfilm fetcher: fix description with multiple paragraphs. [Commit](http://commits.kde.org/telly-skout/58514161b5e51c0e56aaa2a053474e9d26e8be1c).
+ TV Spielfilm fetcher: fix description with multiple names for one role. [Commit](http://commits.kde.org/telly-skout/0de535318cc07eab259ab554113120d88fe65681).
+ Speed up program description update. [Commit](http://commits.kde.org/telly-skout/56fc3b93748ab23d67c6dfaca47a79e5bd9cfd7e). Fixes bug [#497954](https://bugs.kde.org/497954).
+ "Favorites" page: wrap header. [Commit](http://commits.kde.org/telly-skout/075e0db981fc16272095b1bc34387f59852a7fc7). Fixes bug [#497747](https://bugs.kde.org/497747).
+ TV Spielfilm fetcher: fix program stop time before start time. [Commit](http://commits.kde.org/telly-skout/5af350212d08ca5eaf65213143ad9178ce7c66b3). Fixes bug [#497738](https://bugs.kde.org/497738).
+ "Favorites" page: add refetch button. [Commit](http://commits.kde.org/telly-skout/fe256013488fdcf2da73092dd59dca5878c6c5fd). Fixes bug [#497598](https://bugs.kde.org/497598).
+ "Favorites" page: fix placeholder message not shown. [Commit](http://commits.kde.org/telly-skout/f833204fd9b160f87447c4e2096915fb3c64533c).
+ Set loading percentage to 100% if there are no favorites. [Commit](http://commits.kde.org/telly-skout/c56251a9a15d7be0f0da8498125c2bcc6216dfeb).
+ Fix Linux CI build for GCC < 14. [Commit](http://commits.kde.org/telly-skout/02fd5a6a8fd0e5a6ecbfb450fd2657b206dfce6c).
+ Fix Linux CI build. [Commit](http://commits.kde.org/telly-skout/1d7528dfd39bce2f8fda0bb62267d701a0a67035).
+ Fix spacing in ChannelTableDelegate. [Commit](http://commits.kde.org/telly-skout/f53fb3c9c872a16500e95db2bd7a2e59c5180d09).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Fix interacting with grouped notifications. [Commit](http://commits.kde.org/tokodon/b8c8bb852f96e345b9e36a0e83c850522cde05f6).
+ Fix typo in NotificationPage. [Commit](http://commits.kde.org/tokodon/d3915461633f1b56947c37c15cee44f3084fa0bd).
+ Improve the "More" menu on the notifications page. [Commit](http://commits.kde.org/tokodon/fc83730b6095a50e15fe5e229cab694e51237816).
+ Improve the cross-account action dialog. [Commit](http://commits.kde.org/tokodon/1e0c13dada3db353241dcfc1b0cbe3ab6ea4bc83).
+ Don't allow managing following/followers of other accounts. [Commit](http://commits.kde.org/tokodon/c5646d756c043bac41e95c6dc6ddc2dc033efc52).
+ Don't show the navigation tab bar when loading. [Commit](http://commits.kde.org/tokodon/8fc3aeef6d34d7fc5dbb4bd3c71a227bd7b96818). Fixes bug [#497920](https://bugs.kde.org/497920).
+ Fix the width of the content notice. [Commit](http://commits.kde.org/tokodon/3693d82e2055e050d4195d866eaf5a8b131d262a).
+ Don't call Q_UNREACHABLE in NotificationHandler. [Commit](http://commits.kde.org/tokodon/00aae6bec19a7b32581ae4db834901718b8c935a).
+ Fix copying link to clipboard. [Commit](http://commits.kde.org/tokodon/e3fcef96e8f4ac0243b07c774032641c9b6deae3).
+ Remove .craft.ini for 24.12. [Commit](http://commits.kde.org/tokodon/88ee6c6fe91f3ffff128ec5485105c3e901834cb).
+ Reduce the amount of required Android permissions. [Commit](http://commits.kde.org/tokodon/68888382a9a28dd8afe33e734f791531fc9ecc17).
+ ListsPage: Fix undefined warning. [Commit](http://commits.kde.org/tokodon/173de798a120283c0328d2c47ba0cefc767b7192).
+ Load cross action prompt asynchronously. [Commit](http://commits.kde.org/tokodon/6b2a014ffd36152acbf218563af83fdf29e578eb).
+ Load the cute elephant mascot asynchronously. [Commit](http://commits.kde.org/tokodon/df6e0b2e2b884f2601e365916a15f5c21d4af7f6).
+ Don't load the initial timeline twice. [Commit](http://commits.kde.org/tokodon/753836d3cfc2ab7f86405b6766a13cd986f2a26c).
+ Remove unused ReportDialog. [Commit](http://commits.kde.org/tokodon/26c21bcf8e2bae1a3963b411460d2a2ccf5c1ce7).
+ Inherit Item in AttachmentGrid instead of Control. [Commit](http://commits.kde.org/tokodon/15b1377bdd60c0098f88beecce4625736c0d7754).
+ Fix runtime warning in firstAttachmentAspectRatio. [Commit](http://commits.kde.org/tokodon/39f0f478451fa0a19460121c8e232033c94092b2).
+ Don't create a content notice for every post. [Commit](http://commits.kde.org/tokodon/a6c6e6aa7cd9a2492736422c52c0e5a857114894).
+ Fix the safety page not doing much on mobile. [Commit](http://commits.kde.org/tokodon/49f8c921a89c4ab0173da1b19f1e803f7cbbf088).
+ Fix opening the Settings page on the Welcome screen on mobile. [Commit](http://commits.kde.org/tokodon/f5803b6a800c45e5f1958e45b9328853c28f9aef).
+ Pass TokodonApplication to the WelcomePage. [Commit](http://commits.kde.org/tokodon/928d5609a4375bc5be095ebdd5beae60618385cb).
+ Reduce spacing of LinkPreview. [Commit](http://commits.kde.org/tokodon/272fd28f6900125a8c1151e0a285e62351c9fe29).
+ Reload the lists page when you delete a list. [Commit](http://commits.kde.org/tokodon/1bba1fc6bca616206e17029320b7403db1c9d4b4).
+ Reload the lists page when you add a list. [Commit](http://commits.kde.org/tokodon/cf2da2df1df1a6bb5f38d56f546f096d581dc269).
+ Make sidebar scrollable on small screen. [Commit](http://commits.kde.org/tokodon/aac2aa38303c6445bad0cd456206a1f9c574218f).
+ Fix sidebar being shifted by one pixel. [Commit](http://commits.kde.org/tokodon/27ce3338816ac728976e9ab27da349fd5ed6a717).
{{< /details >}}
