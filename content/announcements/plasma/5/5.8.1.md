---
aliases:
- ../../plasma-5.8.1
changelog: 5.8.0-5.8.1
date: 2016-10-11
layout: plasma
youtube: LgH1Clgr-uE
figure:
  src: /announcements/plasma/5/5.8.0/plasma-5.8.png
  class: text-center mt-4
asBugfix: true
---

- Fix modifier-only-shortcut after using Alt+Tab
- Support for EGL_KHR_platform_x11
- Fix crash when moving panel between two windows
- Many Wayland fixes, e.g. resize-only-borders
