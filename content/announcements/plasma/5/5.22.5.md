---
date: 2021-08-31
changelog: 5.22.4-5.22.5
layout: plasma
youtube: HdirtBXW8bI
asBugfix: true
draft: false
---

+ KDE GTK Config: Make sure to actually commit GSettings changes. [Commit.](http://commits.kde.org/kde-gtk-config/9e55858a64808b8a56511126662a10de462d71ed)
+ KSystemStats: Fix handling of IPV6 addresses. [Commit.](http://commits.kde.org/ksystemstats/f8523a922f3f6032aa7e06a4dcbebd9f94065c9d) Fixes bug [#436296](https://bugs.kde.org/436296)
+ [applets/digitalclock] Let long timezones list scroll. [Commit.](http://commits.kde.org/plasma-workspace/97320f2f74f5a275f4dc8ccd3ac765955af058f9) Fixes bug [#439147](https://bugs.kde.org/439147)
