---
date: 2022-07-11
changelog: 5.24.5-5.24.6
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Desktop package: fix unable to apply wallpaper settings after switching layout. [Commit.](http://commits.kde.org/plasma-desktop/29c79a56fd0f79e0b9b35130b2ea899b5d26ce7c) Fixes bug [#407619](https://bugs.kde.org/407619)
+ KWin: Ignore fake input devices when detecting tablet mode. [Commit.](http://commits.kde.org/kwin/fd7ba1560d4557b71fa19c9f6cb73779c6ac1eb0)
+ KSystemStats: Set proper initial values for many SensorProperties. [Commit.](http://commits.kde.org/ksystemstats/118c4bdf3b46bf20e93d02bdc4470eac1fa80baa) Fixes bug [#446414](https://bugs.kde.org/446414)
