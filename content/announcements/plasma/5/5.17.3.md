---
aliases:
- ../../plasma-5.17.3
changelog: 5.17.2-5.17.3
date: 2019-11-12
layout: plasma
peertube: 5a315252-2790-42b4-8177-94680a1c78fc
figure:
  src: /announcements/plasma/5/5.17.0/plasma-5.17.png
asBugfix: true
---

- Fix binding loop in lockscreen media controls. <a href="https://commits.kde.org/plasma-workspace/419b9708ee9e36c4d5825ff6f58ca71f61d32d83">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413087">#413087</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25252">D25252</a>
- [GTK3/Firefox] Fix scrollbar click region. <a href="https://commits.kde.org/breeze-gtk/be9775281fa9018e69588174856da34c96fab82e">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413118">#413118</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25180">D25180</a>
- [effects/startupfeedback] Fallback to small icon size when no cursor size is configured. <a href="https://commits.kde.org/kwin/87f36f53b3e64012bce2edc59f553341fc04cfc2">Commit.</a> Fixes bug <a href="https://bugs.kde.org/413605">#413605</a>. Phabricator Code review <a href="https://phabricator.kde.org/D25065">D25065</a>
