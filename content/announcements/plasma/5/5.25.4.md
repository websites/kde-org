---
date: 2022-08-02
changelog: 5.25.3-5.25.4
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KWin Effects/colorpicker: Fix picking colors. [Commit.](http://commits.kde.org/kwin/420636a2a1300423212bda751097d267bd214d7d) Fixes bug [#454974](https://bugs.kde.org/454974)
+ Plasma Workspace: Fix unable to remove manually added wallpaper. [Commit.](http://commits.kde.org/plasma-workspace/d6d47393bab32dc60b43e0eeac16c035000a0358) Fixes bug [#457019](https://bugs.kde.org/457019)
+ Update battery notifications if they remain open. [Commit.](http://commits.kde.org/powerdevil/ee3d3ea6e0d3019c6585608d3a0d14b71556dc7b)
