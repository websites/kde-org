---
date: 2023-02-21
changelog: 5.27.0-5.27.1
layout: plasma
video: false
asBugfix: true
draft: false
---

+ KWin: Add support for Lima, V3D, VC4 (based on https://github.com/OpenMandrivaAssociation/kwin/blob/master/kwin-5.21.4-add-support-for-panfrost-driver.patch made by  Bernhard Rosenkraenzer) and update list of supported devices for Panfrost. [Commit.](http://commits.kde.org/kwin/eef9bd5c4e564e9cbca6188c52cb3cee5ab9c825) 
+ KScreen: Fix potential crash setting new configs. [Commit.](http://commits.kde.org/libkscreen/d42dea00822c1cfb7478d1d55746e0750156918b) Fixes bug [#464590](https://bugs.kde.org/464590)
+ Plasma Workspace: Better screen removal handling. [Commit.](http://commits.kde.org/plasma-workspace/8bc6be2f1af41274d8209272fca58ec7fe454416) Fixes bug [#465892](https://bugs.kde.org/465892)
