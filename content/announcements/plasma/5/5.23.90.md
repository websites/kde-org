---
aliases:
  - ../../plasma-5.23.90
authors:
  - SPDX-FileCopyrightText: 2022 Paul Brown <paul.brown@kde.org>
  - SPDX-FileCopyrightText: 2022 Jonathan Esk-Riddell <jr@jriddell.org>
SPDX-License-Identifier: CC-BY-4.0
date: 2022-01-13
layout: plasma-5.24
title: 'Plasma 5.24 Beta'
scssFiles:
  - /scss/plasma-5-24.scss
---

{{< container >}}

As is traditional, today we are bringing you the testing version of KDE's Plasma 5.24. Plasma 5.24 Beta is aimed at testers, developers, and bug-hunters.

To help KDE developers iron out bugs and solve issues, install Plasma 5.24 Beta and test run the features listed below. Please report bugs to our bug tracker.

The final version of Plasma 5.24 will become available for the general public on the 8th of February.

<b>DISCLAIMER:</b> This release contains untested and unstable software. Do not use in a production environment and do not use as your daily work environment. You risk crashes and loss of data.

{{< feature src="breeze-focusframes-mrvid-2021-09-22_16.28.02.webm" class="round-top-right" >}}

## Breeze

We made several improvements to the Breeze theme to make Plasma even more pleasant to use:

* Folders now respect your accent color or the color scheme's “Selection” color: [LINK](https://bugs.kde.org/show_bug.cgi?id=395569)
* The focus effect for buttons, text fields, checkboxes, radio buttons, comboboxes, and spinboxes has been enlarged into a “focus ring” that should be much easier to visually distinguish at a glance: [LINK](https://invent.kde.org/plasma/breeze/-/merge_requests/147)
* The Breeze color scheme has been renamed to “Breeze Classic”, to better distinguish it from the Breeze Light and Breeze Dark color schemes: [LINK](https://invent.kde.org/plasma/breeze/-/merge_requests/145)
* The “Breeze High Contrast” color scheme has been deleted, as it actually offered lower contrast than the very similar-looking Breeze Dark. Existing users will be migrated to Breeze Dark: [LINK](https://invent.kde.org/plasma/breeze/-/merge_requests/141)

{{< /feature >}}

{{< /container >}}

{{< diagonal-box color="purple" >}}

## Notifications

![Critical Notifications](notifications-wee.png)

* Critical notifications now have a little orange strip on the side to visually distinguish them from background clutter and help them stand out so that you will be more likely to notice them: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1113)
* Header and title text in notifications received better contrast and visibility: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1110?)
* Notifications about video files now display a thumbnail in the notification, just like for image files: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1109)
* When sending or receiving files via Bluetooth, a system notification is now always shown: [LINK](https://invent.kde.org/plasma/bluedevil/-/merge_requests/51/diffs)
* The “Annotate” button that appears in screenshot notifications has been moved to the same row as the hamburger menu button: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1256)

{{< /diagonal-box >}}

{{< container >}}

## System Tray and Widgets

![Plasma Pass](plasma-pass.png)

* The “Plasma Pass” password manager has a modernized design: [LINK](https://invent.kde.org/plasma/plasma-pass/-/merge_requests/5)
* Scrollable areas in the system tray now use a more consistent style: [LINK](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/96)
* The Weather widget prompts you to configure it when first added and automatically searches through all available weather sources: [LINK](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/77)/[LINK](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/97)
* We have added a “Date always below time” option for the Digital Clock to complement its “Date always beside time” and “Automatic” options: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1114)
* Battery & Brightness has had its user interface for blocking sleep and screen locking improved again for more clarity: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1257)
* Clipboard and Network management are now fully keyboard navigable: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1094)
* You now have the option to see network speeds in bits per second: [LINK](https://bugs.kde.org/show_bug.cgi?id=418968)
* The Kickoff Application Launcher’s sidebar no longer shows arrows, to be consistent with how sidebars are typically presented elsewhere: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/737)
* Battery and Brightness now turns into just Brightness controls on computers with no batteries: [LINK](https://bugs.kde.org/show_bug.cgi?id=415073)
* The Free Space Notifier no longer pointlessly monitors read-only volumes: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1117)
* The Audio Volume's sliders highlight the part before the slider: [LINK](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/88)
* Bluetooth calls a phone a phone: [LINK](https://invent.kde.org/plasma/bluedevil/-/merge_requests/55)
* The Media Player correctly shows “nothing playing” when the last playing app is closed: [LINK](https://bugs.kde.org/show_bug.cgi?id=446098)

## Desktop and Panel

* It is now possible to set your wallpaper using the context menu of any image: [LINK](https://bugs.kde.org/show_bug.cgi?id=358038)
* You can now drag a panel from anywhere on its Edit Mode toolbar, not just from a tiny button, with a label making this even more obvious: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/764)
* Users of the “Picture of the Day” wallpaper plugin can now pull images from <http://simonstalenhag.se> which is full of cool and creepy sci-fi images: [LINK](https://invent.kde.org/plasma/kdeplasma-addons/-/merge_requests/87)
* There is a new “Configure Display Settings” item in the Desktop's context menu and Edit Mode toolbar, replacing the “Open in Dolphin” item by default: [LINK](https://bugs.kde.org/show_bug.cgi?id=355679)/[LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/613)
* Those of you who like really huge icons can now make your desktop icons twice as large as the previous maximum size: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/683)
* The previews in the wallpaper chooser now respect your aspect ratio: [LINK](https://bugs.kde.org/show_bug.cgi?id=399354)
* When you drag-and-drop widgets, they now smoothly animate moving to their final position rather than instantly teleporting there: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/680)

## Task Manager

* It is now possible to reverse the alignment of tasks, which can be useful for example for having a Task Manager next to a Global Menu: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/738)
* Tasks have a new “Move to Activity” context menu item: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/231)
* Task Manager tooltips for windows that are playing audio now show a volume slider under the playback controls: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/788)
* The “Start New Instance” context item has been renamed to “Open New Window” for clarity and no longer appears for apps marked as having a single main window or that already provide a “New Window” action of their own: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/133)
* The “More Actions” item in the context menu has been moved to the bottom and renamed to just “More”: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/769)
* Tooltips for large number of open windows for the same app are now significantly faster to load and much more responsive: [LINK](https://bugs.kde.org/show_bug.cgi?id=444001)

## KRunner

* There is a new inline help feature, shown by clicking on the question mark icon on its toolbar or typing “?”. While in Help mode, clicking a particular plugin will show you all the different search syntaxes for it: [LINK](https://bugs.kde.org/show_bug.cgi?id=433636)

## System Settings

* The very minimal contents of System Settings’ own settings window have been moved into its hamburger menu for easier access: [LINK](https://bugs.kde.org/show_bug.cgi?id=437383)
* You can now pick your own custom accent color on the Colors page: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1031)
* System Settings pages that have a single big grid or list now use a more modern frameless style: [LINK](https://invent.kde.org/frameworks/kdeclarative/-/merge_requests/91)
* The Formats page has been rewritten in QtQuick, which also improves the UI and prepares it for a future merge with the Languages page: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1064)
* The Keyboard page now respects the “Show Changed Settings” feature and supports more than 8 spare layouts. Also, the “add a keyboard layout” dialog has been redesigned for simplicity and ease of use: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/519)/[LINK](https://bugs.kde.org/show_bug.cgi?id=446456)/[LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/596)
* Night Color also now supports “Highlight Changed Settings”: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1042)
* The battery charge limit feature gained support for more than one battery: [LINK](https://invent.kde.org/plasma/powerdevil/-/merge_requests/70)
* The speaker test sheet in System Settings’ Audio page has a better look: [LINK](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/101)
* Display & Monitor shows the scale factor used for each screen as well as the proper physical resolution: [LINK](https://invent.kde.org/plasma/kscreen/-/merge_requests/71)/[LINK](https://bugs.kde.org/show_bug.cgi?id=441417)
* Even when your language is not set to English, the System Settings' search field will still find results for English words: [LINK](https://bugs.kde.org/show_bug.cgi?id=446285)
* When you enable auto-login, you are now warned about some changes you might want to make to your KWallet setup: [LINK](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/20)
* A new button on the “About this System” page lets you quickly access the Info Center: [LINK](https://bugs.kde.org/show_bug.cgi?id=438213)

## Window Management

* The new Overview effect, toggled with <kbd>Meta</kbd>+<kbd>W</kbd>, has a blurred background by default, lets you control your Virtual Desktops and find search results from KRunner all in one place: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1548)/[LINK](https://bugs.kde.org/show_bug.cgi?id=445207)
* KWin lets you optionally set a keyboard shortcut to move a window to the center of the screen, which is also where they now appear by default: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1409)/[LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1631)
* Windows now remember the screens they were on when those screens are turned off or unplugged, fixing many multi-monitor annoyances: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1588#note_331696)
* The “scale” effect is now used by default when opening and closing windows, instead of “fade”: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1845)
* The “Cover Switch” and “Flip Switch” effects are back, rewritten in QtQuick for easier extensibility in the future: [LINK](https://bugs.kde.org/show_bug.cgi?id=443757)
* Fixed a major performance issue in QtQuick-based effects for NVIDIA GPU users: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1689)

## Discover

* Added the option to automatically reboot after an update is complete: [LINK](https://bugs.kde.org/show_bug.cgi?id=419504)
* Discover now switches its home page to a two-column card view when the window is very wide and a bottom tab bar in narrow/mobile mode: [LINK](https://invent.kde.org/plasma/discover/-/merge_requests/180)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/173)
* The Update page is now less overwhelming by only showing progress bars for in-progress items and having a nicer design to select updates. Additionally, it tells you the source of the updates: [LINK](https://invent.kde.org/plasma/discover/-/merge_requests/171)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/207)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/206)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/217)
* A new “Report this issue” button takes you straight to your distribution’s bug tracker: [LINK](https://bugs.kde.org/show_bug.cgi?id=442785)
* The search tries to help users if they know an app exists but nothing is found and stops searching automatically if nothing is found after some time: [LINK](https://invent.kde.org/plasma/discover/-/merge_requests/185)/[LINK](https://bugs.kde.org/show_bug.cgi?id=427244)
* You can now easily manage repositories for Flatpak and your distribution: [LINK](https://bugs.kde.org/show_bug.cgi?id=443455)/[LINK](https://bugs.kde.org/show_bug.cgi?id=444239)
* Locally downloaded Flatpak apps can now be opened and installed, adding their repository automatically to your system: [LINK](https://bugs.kde.org/show_bug.cgi?id=445596)
* Application list items now have a more attractive and logical layout: [LINK](https://invent.kde.org/plasma/discover/-/merge_requests/199)
* A new safety feature prevents you from accidentally uninstalling Plasma: [LINK](https://bugs.kde.org/show_bug.cgi?id=445226)
* Checking for updates is much faster and installation error messages are more user-friendly: [LINK](https://bugs.kde.org/show_bug.cgi?id=432965)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/213)/[LINK](https://invent.kde.org/plasma/discover/-/merge_requests/203)

## Login/Lock Screen

* Fingerprint support has been added, letting you enroll and de-enroll fingers. Any of those fingers can be used to unlock the screen, provide authentication when an app asks for your password, and also authenticate `sudo` on the command line:
[LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/149)
* The Lock screen now lets you Sleep and Hibernate (if supported): [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1122)

{{< /container >}}

{{< diagonal-box color="purple" logo="/images/wayland.png" alt="Wayland logo" >}}

## Wayland

* Colors greater than 8-bit are now supported: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1641)
* There is a new drawing tablet page in System Settings: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/677)
* “DRM leasing” support has been added, which allows us to re-add support for VR headsets with optimal performance: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/662)
* Spectacle finally has access to the “Active Window” mode on Wayland: [LINK](https://bugs.kde.org/show_bug.cgi?id=386271)
* We added the concept of the “primary monitor”, which works like in the X11 session: [LINK](https://bugs.kde.org/show_bug.cgi?id=442158)
* The “Minimize All Windows” widget now has Wayland support: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/732)
* The virtual keyboard now only appears when you explicitly focus a text-based UI control with a touch or stylus poke: [LINK](https://invent.kde.org/plasma/kwin/-/merge_requests/1517)
* When using a stylus, it is now possible to activate other windows from their titlebars and also just interact with more with titlebars in general: [LINK](https://bugs.kde.org/show_bug.cgi?id=432104)
* Restoring a minimized or maximized window now switches to the virtual desktop that the window was on before restoring it, instead of restoring it to the current virtual desktop: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/549)
* You can now use the default <kbd>Meta</kbd>+<kbd>Tab</kbd> shortcut to cycle through more than two activities at a time: [LINK](https://bugs.kde.org/show_bug.cgi?id=397662)
* The System Tray item for showing and hiding the virtual keyboard becomes active only in tablet mode: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1335)

{{< /diagonal-box >}}

{{< container >}}

## General Behavior

* It is now possible for Global Themes to specify and change Latte Dock layouts: [LINK](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1243)
* Changing the color scheme now toggles the standardized FreeDesktop light/dark color scheme preference, so third-party apps that respect this preference will be able to automatically switch to light or dark mode based on your chosen color scheme: [LINK](https://invent.kde.org/plasma/xdg-desktop-portal-kde/-/merge_requests/49)
* Kate has been replaced with KWrite in the default set of favorite apps, since it is a bit more user-friendly and less programmer-centric: [LINK](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/704)
* The odd behavior of middle-clicking on a panel to create a sticky note has been disabled: [LINK](https://bugs.kde.org/show_bug.cgi?id=425852)
* The screen layout OSD now indicates screens’ scale factors in it: [LINK](https://invent.kde.org/plasma/kscreen/-/merge_requests/75)
* Scrollable controls in Plasma and other QtQuick-based apps now only change their contents when you start scrolling on them, preventing you from accidentally changing things while scrolling the view: [LINK](https://bugs.kde.org/show_bug.cgi?id=385270)
* Plasma now shuts down faster by no longer accepting new connections after it has begun the shutdown process, which particularly helps when using KDE Connect: [LINK](https://bugs.kde.org/show_bug.cgi?id=432643)

If you would like to learn more, [check out the full changelog for Plasma 5.24 Beta](/announcements/changelogs/plasma/5/5.23.5-5.23.90).

{{< /container >}}
