---
aliases:
- ../../kde-frameworks-5.4.0
date: '2014-11-06'
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Fix build with Qt 5.4

### KArchive

- Add support for rcc files

### KAuth

- Fix install dir when using KDE_INSTALL_USE_QT_SYS_PATHS

### KCMUtils

- Fix KPluginSelector not adding non .desktop file plugins

### KConfigWidgets

- Fix URL in KHelpClient::invokeHelp

### KCoreAddons

- Various build fixes (QNX, Ubuntu 14.04)

### KDeclarative

- Optimize IconItem in QIconItem in memory and speed

### KIO

- New job KIO::mkpath()
- New job KIO::PasteJob, handles pasting+dropping URLs and data; KIO::paste replaces KIO::pasteClipboard
- New function KIO::pasteActionText, to handle the paste action
- When clicking on a script or desktop file in a file manager, let the user choose between executing and vieweing as text
- KUrlRequester: fixing handling of start directory
- Offer also overwrite option when copying multiple files and there is a conflict (regression compared to kdelibs4)
- KDirLister: do not kill+restart running update jobs.
- KDirLister: don't emit refreshItems for items that haven't changed.
- Prevent incorrect disk full error messages on mounts where we cannot determine the amount of free space.
- Fix running the file type editor

### KNewStuff

- Many small fixes and cleanups

### KNotifications

- Add support for custom icons (convenience method KNotification::Event with icon name)

### KTextEditor

- Implement "go to last editing position" action
- Guard against a possibly broken code folding state on disk

### KWallet

- Emit 'walletListDirty' signal when the 'kwalletd' directory is deleted

### KWidgetsAddons

- New function KMimeTypeEditor::editMimeType(), to launch keditfiletype5

### KXmlGui

- Now supports finding ui files in resources (default location: :/kxmlgui5/&lt;componentname&gt;)

### Plasma frameworks

- Fixes in the Dialog QML component size and position
- fixes in the Slider and ProgressBar QML components
- new icons

### Solid

- [not communicated]

### Buildsystem changes

- New modules FindWaylandScanner and FindQtWaylandScanner, including macros ecm_add_qtwayland_client_protocol and ecm_add_qtwayland_server_protocol

### Frameworkintegration

- implement support for setting custom labels in file dialogs

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.
