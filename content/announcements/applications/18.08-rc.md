---
aliases:
- ../announce-applications-18.08-rc
date: 2018-08-03
description: KDE Ships Applications 18.08 Release Candidate.
layout: application
release: applications-18.07.90
title: KDE Ships Release Candidate of KDE Applications 18.08
---

August 3, 2018. Today KDE released the release candidate of the new versions of KDE Applications. With dependency and feature freezes in place, the KDE team's focus is now on fixing bugs and further polishing.

Check the <a href='https://community.kde.org/Applications/18.08_Release_Notes'>community release notes</a> for information on tarballs and known issues. A more complete announcement will be available for the final release

The KDE Applications 18.08 releases need a thorough testing in order to maintain and improve the quality and user experience. Actual users are critical to maintaining high KDE quality, because developers simply cannot test every possible configuration. We're counting on you to help find bugs early so they can be squashed before the final release. Please consider joining the team by installing the release candidate <a href='https://bugs.kde.org/'>and reporting any bugs</a>.