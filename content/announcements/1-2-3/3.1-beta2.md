---
aliases:
- ../announce-3.1beta2
custom_about: true
custom_contact: true
date: '2002-10-02'
description: The second beta of the upcoming KDE 3.1 release. A lot more stable.
title: KDE 3.1 Second Beta Testing Release
---

<h3 align="center">
   KDE Project Ships Second Beta Release of Leading Open Source Desktop
</h3>

The KDE Project Ships Second Beta Release for the Third-Generation of the Leading Desktop for Linux/UNIX, Seeks Testing and Feedback

The <a href="/">KDE
Project</a> today announced the immediate availability of KDE 3.1 beta2,
the third development release of a significant feature upgrade for KDE 3
(<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/">screenshots</a>).
KDE 3 is the third generation of KDE's free, powerful and easy-to-use
Internet-enabled desktop for Linux and other UNIXes.
KDE 3.1, scheduled for final release in early November 2002, will provide
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">substantial
improvements</a> to the KDE desktop experience.
As the KDE 3 API is frozen for binary compatibility, KDE 3.1 will be
binary compatible with KDE 3.0.

KDE 3.1 beta2 ships with the core KDE libraries, the base desktop
environment, and hundreds of applications and other desktop enhancements
from the other KDE base packages (PIM, administration, network,
edutainment, development, utilities, multimedia, games, artwork and
others).

The primary goal of the 3.1beta2 release is to provide experienced KDE
users who prefer to enjoy some of the many great new KDE 3.1 features now
a chance to do so as well as to report any problems with their installation.
Instructions for maintaining a KDE 3.1 beta installation side-by-side
with a stable KDE release are
<a href="http://developer.kde.org/build/build2ver.html">available</a>.

KDE, including all its libraries and its applications, is
available <em>for free</em> under Open Source licenses.
KDE can be obtained in source and, for stable releases, in numerous binary
formats from the KDE
<a href="http://download.kde.org/unstable/kde-3.1-beta2/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors,
and can also be obtained on
<a href="http://www.kde.org/cdrom.html">CD-ROM</a> or with any
of the major Linux/UNIX systems shipping today.

For more information about the KDE 3 series, please view the early KDE 3.1
<a href="http://static.kdenews.org/mirrors/qwertz/kde31alpha/">screenshots</a>
(!), or read the
<a href="/announcements/announce-3.0">KDE 3.0
announcement</a>, the list of KDE 3.1
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">planned
features</a> and the KDE 3.1
<a href="http://developer.kde.org/development-versions/kde-3.1-release-plan.html">release
plan</a>.

<a id="changes"></a>

#### New Features / Enhancements

In addition to the large number of new features previously announced in the
<a href="../3.1-alpha1/#changes">KDE
3.1alpha1 press release</a>, the principal new features and enhancements in
KDE 3.1beta2 include:

<ul>
  <li>
    <div align="justify">
      a new Exchange 2000 plugin for the calendar / scheduling application
      (<a href="http://korganizer.kde.org/">KOrganizer</a>);
    </div>
  </li>
  <li>
    <div align="justify">
      a desktop-sharing client server (<em>KRfb</em>) and client (<em>KRdc</em>)
      compatible with <a href="http://www.uk.research.att.com/vnc/">VNC</a>;
    </div>
  </li>
  <li>
    <div align="justify">
      support for common Microsoft character symbols (TM, bullet, smart quotes,
      etc.) in the HTML rendering engine
      (<a href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/khtml/index.html">KHTML</a>);
    </div>
  </li>
  <li>
    <div align="justify">
      a file browser (<a href="http://konqueror.kde.org/">Konqueror</a>) plugin
      for displaying and/or setting meta-information about JPEG images
      (comments) and digital camera (extended information);
    </div>
  </li>
  <li>
    <div align="justify">
      numerous enhancements to the translation tool
      (<a href="http://i18n.kde.org/tools/kbabel/">KBabel</a>), such as
      mailing .PO files, a tag structure tool, rough translations in the
      catalog manager and automatic updates of the .PO header comment;
      and
    </div>
  </li>
  <li>
    <div align="justify">
      public key authentication support for kio_sftp<sup>*</sup>.
    </div>
  </li>
</ul>
<br />
<small><em>Items marked with a <sup>*</sup> are works in progress</em>.</small>

For a more complete list of features added in the KDE 3.1 branch, please
see the
<a href="http://developer.kde.org/development-versions/kde-3.1-features.html">KDE
3.1 feature plan</a>.

<a id="binary"></a>

#### Installing Binary Packages

<u>Binary Packages</u>.
Some Linux/UNIX OS vendors have kindly provided binary packages of
KDE 3.1beta2 for some versions of their distribution, and in other cases
community volunteers have done so.
Some of these binary packages are available for free download from KDE's
<a href="http://download.kde.org/unstable/kde-3.1-beta2/">http</a> or
<a href="http://www.kde.org/ftpmirrors.html">ftp</a> mirrors.
Additional binary packages, as well as updates to the packages now
available, may become available via the KDE servers over the coming
weeks.

Please note that the KDE Project makes these packages available from the
KDE web site as a convenience to KDE users. The KDE Project is not
responsible for these packages as they are provided by third
parties - typically, but not always, the distributor of the relevant
distribution - using tools, compilers, library versions and quality
assurance procedures over which KDE exercises no control. In addition,
please note that some vendors provide binary packages solely through
their servers. If you cannot find a binary package for your OS, or you
are displeased with the quality of binary packages available for your OS,
please read the <a href="http://www.kde.org/packagepolicy.html">KDE
Binary Package Policy</a> and/or contact your OS vendor.

<u>Library Requirements / Options</u>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and other libraries
than was shipped with the system (e.g., LinuxDistro X.Y
may have shipped with Qt-3.0.0 but the packages below may require
Qt-3.0.3). For general library requirements for KDE, please see the text at
<a href="#source_code-library_requirements">Source Code - Library
Requirements</a> below.

<a id="package_locations"><u>Package Locations</u></a>.
At the time of this release, pre-compiled packages are available through the
KDE servers (or elsewhere, where indicated) for the following distributions
and versions (please be sure to
read any applicable <code>README</code> first):

Binary packages contributed by users rather than the relevant OS vendor
are located in the
<a href="http://download.kde.org/unstable/kde-3.1-beta2/contrib/">contrib</a>
directory.

Additional binary packages will likely become available over the coming
days and weeks; please check the
<a href="/info/1-2-3/3.1">KDE 3.1 Info Page</a>
for updates.

#### Compiling Source Code

<a id="source_code-library_requirements"></a><u>Library
Requirements / Options</u>.
KDE 3.1 beta2 requires
<a href="http://www.trolltech.com/developer/download/qt-x11.html">Qt
3.0.3 for X11</a>, though version 3.0.5 is recommended (Qt 3.1 beta1
<em>will not</em> work, while either Qt 3.1 beta2 or qt-copy should).
In addition, KDE can take advantage of a number of other
<a href="../3.0/#source_code-library_requirements">optional
libraries and applications</a> to improve your desktop experience and
capabilities.

<u>Compiler Requirements</u>.
KDE is designed to be cross-platform and hence to compile with a large
variety of Linux/UNIX compilers. However, KDE is advancing very rapidly
and the ability of native compilers on various UNIX systems to compile
KDE depends on users of those systems
<a href="http://bugs.kde.org/">reporting</a> compilation and other
build problems to the responsible maintainers.

With respect to the most popular KDE compiler,
<a href="http://gcc.gnu.org/">gcc/egcs</a>, please note that some
components of KDE will not compile properly with gcc versions earlier
than gcc 2.95, such as egcs 1.1.2 or gcc 2.7.2, or with unpatched
versions of <a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc</a> 3.0.x.
GCC 3.2 is recommended, though KDE should compile properly with gcc 3.1.

<a id="source_code"></a><u>Source Code</u>.
The complete source code for KDE 3.1 beta2 is available for free
<a href="http://download.kde.org/unstable/kde-3.1-beta2/src/">download</a>.
These source packages have been digitally signed with
<a href="http://www.gnupg.org/">GnuPG</a> using the KDE Archives PGP Key
(available from the
<a href="http://www.kde.org/signature.html">KDE Signature page</a>
or public key servers), and their respective MD5 sums are listed on the
<a href="/info/1-2-3/3.1">KDE 3.1 Info Page</a>.

<u>Further Information</u>. For further
instructions on compiling and installing KDE 3.1 beta2, please consult
the <a href="http://developer.kde.org/build/index.html">installation
instructions</a> and, if you should encounter compilation difficulties, the
<a href="http://developer.kde.org/build/compilationfaq.html">KDE Compilation FAQ</a>.

#### KDE Sponsorship

Besides the superb and invaluable efforts by the
<a href="http://www.kde.org/gallery/index.html">KDE developers</a>
themselves, significant support for KDE development has been provided by
<a href="http://www.mandrakesoft.com/">MandrakeSoft</a> and
<a href="http://www.suse.com/">SuSE</a>. In addition,
the members of the <a href="http://www.kdeleague.org/">KDE
League</a> provide significant support for KDE promotion, and the
<a href="http://www.uni-tuebingen.de/">University of T&uuml;bingen</a>
and the <a href="http://www.uni-kl.de/">University of Kaiserslautern</a>
provide most of the Internet bandwidth for the KDE project.
Thanks!

#### About KDE

KDE is an independent project of hundreds of developers, translators,
artists and other professionals worldwide collaborating over the Internet
to create and freely distribute a sophisticated, customizable and stable
desktop and office environment employing a flexible, component-based,
network-transparent architecture and offering an outstanding development
platform. KDE provides a stable, mature desktop, a full, component-based
office suite (<a href="http://www.koffice.org/">KOffice</a>), a large
set of networking and administration tools and utilities, and an
efficient, intuitive development environment featuring the excellent IDE
<a href="http://www.kdevelop.org/">KDevelop</a>. KDE is working proof
that the Open Source "Bazaar-style" software development model can yield
first-rate technologies on par with and superior to even the most complex
commercial software.

<hr />

  <font size="2">
  <em>Trademarks Notices.</em>
  KDE, K Desktop Environment, KDevelop and KOffice are trademarks of KDE e.V.

Exchange 2000 and Microsoft are registered trademarks of Microsoft Corp.
in the United States and/or other countries.

Linux is a registered trademark of Linus Torvalds.

Trolltech and Qt are trademarks of Trolltech AS.

UNIX is a registered trademark of The Open Group.

All other trademarks and copyrights referred to in this announcement are
the property of their respective owners.
</font>

<hr />

<table id ="press" border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
Andreas Pour<br />
KDE League, Inc.<br />

[pour@kde.org](mailto:pour@kde.org)<br>
(1) 917 312 3122

  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(33) 4 3250 1445

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Ralf Nolden<br>
    
  [nolden@kde.org](mailto:nolden@kde.org) <br>
      (49) 2421 502758
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English):
  </td>
  <td>
   Jono Bacon <br>
    
  [jono@kde.org](mailto:jono@kde.org) <br>
  </td>
</tr>
<tr Valign="top">
  <td >
      Southeast Asia (English and Indonesian):
  </td>
  <td>
   Ariya Hidayat <br>
    
  [ariya@kde.org](mailto:ariya@kde.org) <br>
  (62) 815 8703177

  </td>
</tr>
</table>
