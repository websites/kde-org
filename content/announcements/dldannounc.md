---
custom_about: true
custom_contact: true
title: KDE Included in Delix
date: 1998-09-04
---


## DELIX ANNOUNCES THE INCLUSION OF THE KDE DESKTOP ENVIRONMENT IN DLD 5.41

STUTTGART, SEPTEMBER 4TH, 1998
delix is pleased to announce the German Linux Distribution - DLD 5.4 1
with K Desktop Environment KDE 1.0 as standard desktop. KDE is fully
preconfigured and has lots of multimedia-extensions. KDE as a very
user-friendly Desktop Environment gives our customers the convenience,
they are accustomed from other operating systems. KDE makes it easier
to change completely from other operating systems to Linux. A
Server-mirror of the KDE homepage and a selected HDE FTP-mirror
including a snapshot of KOffice is also included in DLD 5.41

In addition to KDE 1.0, DLD 5.41 offers a lot of additional features:
Support for Plug-and-Play soundcards like Soundbaster/compatible
cards, PCI-autodetection, automatic harddisk partitioning, automatic
ISDN- and PPP-Configuration and a comfortable printer configuration.
DLD 5.41 comes with Kernel 2.0.35. Also included is support for the
very new Adaptec AIC7890 SCSI host adapter 2940U2W. Every DLD comes
with our delix Solution CD: this CD-ROM contains more than 600 MByte
commercial Linux-software. Also included is Staroffice 4.0 (60 days
working demo), WordPerfect 7.0 demo, Databases, development tools,
compilers and more. DLD comes as a 4 CD-ROM set including a 400 page
manual and bootdisks. DLD is completely german, there is no english
release available. For more information on DLD 5.41 please fell free
and contact our homepage http://www.delix.de or send email to
delix@delix.de.
