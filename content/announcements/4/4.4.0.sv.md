---
aliases:
- ../4.4
title: KDE SC 4.4.0 Caikaku Release Announcement
date: "2010-02-09"
---

<h3 align="center">
  KDE 4.4.0 introducerar ett nytt gränssnitt för netbooks, flikhantering för fönster samt ett nytt autentiseringsramverk
</h3>

<p align="justify">
  <strong>
    KDE Software Compilation 4.4.0 (Kodnamn: <i>"Caikaku"</i>) släppt
  </strong>
</p>

<p align="justify">
Idag lanserar <a href=http://www.kde.org/>KDE</a> av version 4.4 av sin mjukvarusamling, <i>"Caikaku"</i>. Flera nya teknologier introduceras, inklusive sociala nätverk och kollaboreringstekniker, ett nytt gränssnitt för netbooks samt ett nytt ramverk för autentisering, KAuth. Enligt KDE's bugghanteringssystem har 7293 buggar stängts och 1433 begäran om ny funktionalitet har implementerats. KDE-communityn tackar alla som har hjälpt till att göra det här släppet möjligt.
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/general-desktop.png">
	<img src="/announcements/4/4.4.0/thumbs/general-desktop_thumb.png" class="img-fluid" alt="KDE:s skrivbordsskal Plasma">
	</a> <br/>
	<em>KDE:s skrivbordsskal Plasma</em>
</div>
<br/>

<p align=justify>
 Läs den <a href="/announcements/4/4.4.0/guide">visuella guiden</a> till KDE Software Compilation 4.4 för mer information om de förbättringar som gjorts eller fortsätt
 läsa för en överblick.
</p>

<h3>
  Plasma introducerar social- och webbfunktionalitet
</h3>
<br />
<p align=justify>
 KDE:s skrivbordsskal Plasma tillhandahåller den grundläggande funktionalitet en användare behöver för att starta och hantera applikationer, filer och globala inställningar. Ett nytt gränssnitt för netbooks, förädlad grafik och ett bättre arbetsflöde ingår bland de delar som utvecklarna har levererat till den här versionen. Funktionalitet för sociala nätverk och andra nätverksservicar introduceras som ett resultat av samarbete inom KDE:s community.
</p>

<!-- Plasma Screencast -->
<div class="text-center">
	<em>Förbättrad interaktion med Plasma  </em><br/>
<a href="http://blip.tv/file/get/Sebasje-KDEPlasmaIn44540.ogg">ladda ner</a></div>
<br/>

<p align="justify">
<ul>
  <li>
    <strong>Plasma Netbook</strong> debuterar i 4.4.0. Plasma Netbook är ett alternativt gränssnitt till Plasma Desktop, speciellt anpassat för att erbjuda ergonomi till netbooks och mindre bärbara datorer. Ramverket kring Plasma är från början designat för att inte låsa sig mot någon specifik skrivbordsmiljö. Plasma Netbook delar många komponenter med Plasma Desktop, men är specifikt designat för att ta till vara på den begränsade ytan och att vara mer lämpad för touch screen-inmatning.
    Plasma Netbook innehåller ett gränssnitt för att söka efter och starta applikationer i fullskärm och en dagstidningsmetafor som grupperar ett antal mindre komponenter och verktyg från skrivbordsversionen till en sida.
  </li>
    <strong>Social Desktop</strong> är ett intiativ för att ge möjlighet för användare att hitta och kommunicera med med sina vänner direkt från widgeten. Social News visar en ström av vad som händer i användarens sociala nätverk. Den nya Knowledge Base widgeten tillåter användaren att söka efter svar och frågor från ett antal olika källor inklusive openDesktop.org.
  </li>
  <li>
    Den nya flikhanteringen i KWin låter användaren <strong>gruppera ihop fönster</strong> i ett enda flikat gränssnitt, vilket gör det mycket lättare och effektivare att hantera stora mängder program. Ytterligare förbättringar inkluderar fönster som går att fästa mot sidan av en skärmyta och maximering av fönster genom att dra dom. Teamet bakom KWin har även samarbetat med Plasma-utvecklarna för att göra interaktionen mellan applikationerna i arbetsytan bättre för mjukare animationer och ökad prestanda. Slutligen har tillvaron för grafiker gjorts enklare genom att en mer konfigurerbar fönsterdekoration som kan använda sig av skalbar grafik.
  </li>
</ul>
</p>

<!-- Window grouping Screencast -->
<div class="text-center">
	<em>Förenklar fönsterhantering  </em><br/>
<a href="http://blip.tv/file/get/Sebasje-WindowManagingFeaturesInKDEPlasma44222.ogg">ladda ner</a></div>
<br/>

<h3>
  Innovativa KDE-applikationer
</h3>
<p align=justify>
KDE tillhandahåller ett stort antal kraftfulla och användbara applikationer. Den här versionen introducerar en variete av inkrementella förbättringar och några innovativa nya teknologier.
</p>
<p align=justify>
<ul>
  <li>
    Med den här versionen av KDE Software Compilation medföljer en kraftigt förbättrad version av <strong>ramverket GetHotNewStuff</strong> vilket är ett resultat av en lång design- och planeringsprocess. Ramverket är en mekanism för att låta alla de tusentals utvecklare och artister som skapar tillägg och innhåll till KDE:s applikationer enkelt kunna distribuera sitt arbete till alla miljontals KDE-användare. Användare kan t.ex. ladda ner <a href="http://dimsuz.wordpress.com/2009/11/11/katomic-new-feature-level-sets-support/">nya banor till KAtomic</a>, nya stjärnor till KStart eller skript som tillför funktionalitet direkt ifrån applikationen. Nytt är att man kan betygssätta och kommentera innehållet samt bli en anhängare till en viss produkt vilket låter en följa produktens utveckling i Social News-widgeten. Användare kan <strong>ladda upp</strong> sina egna resultat direkt från flera applikationer vilket eliminerar den tråkiga paketerings- och distribueringsprocessen.
  </li>
  <li>
    Två andra långprojekt inom KDE har levererat resultat till den här versionen. Nepomuk, ett internationellt forskningsprojekt finansierat av EU, har nu nått tillräcklig stabilitet och prestanda. <strong><a href="http://ppenz.blogspot.com/2009/11/searching.html">Dolphins skrivbordsök</a> </strong> använder Nepomuks semantiska ramver för att hjälpa användaren att hitta och organisera sin data. Den nya tidslinjevyn visar de filer som använts senast och i vilken ordning de ändrats. KDE PIM har ägnat sig åt att portera sina första applikationer till det nya lagrings- och hämtningssystemet <strong>Akonadi</strong>. Adressboksapplikationen har skrivits om och fått ett nytt 3-panelsgränssnitt. Fler applikationer kommer att portas till Akonadi i kommande versioner.
  <li>
    Förutom integration av de här teknologierna har ett flertal andra applikationer förbättrats. KGet har lagt till stöd för digitala signaturer och möjlighet att ladda ner filer från flera källor samtidigt. Gwenview har fått ett lättanvänt fotoimporteringsverktyg. Några nya applikationer ser också dagens ljus iom dagens släpp. Palapeli är ett pusselspel som låter användaren inte bara lösa pussel på datorn men även skapa egna och dela med sig av arbetet till andra. Cantor är ett nytt gränssnitt till några kraftfulla statistik- och vetenskapsmjukvaror (<a href="http://r-project.org/">R</a>, <a href="http://sagemath.org/">SAGE</a>, och <a href="http://maxima.sourceforge.net/">Maxima</a>). Blogilo är en ny bloggapplikation i KDE PIM.
  </li>
</ul>
</p>
<div class="text-center">
	<a href="/announcements/4/4.4.0/dolphin-search.png">
	<img src="/announcements/4/4.4.0/thumbs/dolphin-search_thumb.png" class="img-fluid" alt="Skrivbordssök inbyggt i Dolphin">
	</a> <br/>
	<em>Skrivbordssök inbyggt i Dolphin</em>
</div>
<br/>

<h3>
  Plattformen som hjälper utvecklaren
</h3>
<p align=justify>
Det stora fokus som legat på att förfina teknologin bakom KDE SC har levererat en ännu bättre, mer komplett och effektiv utvecklingsplattform. Med version 4.4 har det tillkommit några nya kollaborerings och sociala teknologier till plattformen. Vi tillhandahåller kraftfulla, flexibla och öppna teknologier och strävar efter att stödja innovation både online och på skrivbordsmiljön.
<p align=justify>
All kod är licensierad under LPGL vilket öppnar för både proprietär- och öppen källkodsutveckling för de flesta större plattformar (Linux, UNIX, Mac och MS Windows).
</p>

<div class="text-center">
	<a href="/announcements/4/4.4.0/social-web-widgets.png">
	<img src="/announcements/4/4.4.0/thumbs/social-web-widgets_thumb.png" class="img-fluid" alt="Sociala nätverk direkt på ditt Plasma-skrivbord">
	</a> <br/>
	<em>Sociala nätverk direkt på ditt Plasma-skrivbord</em>
</div>
<br/>

<h4>
Sprid ordet
</h4>
<p align="justify">
KDE-communityn uppmanar alla att sprida ordet på de sociala nätverken. Skriv om nyheten
på deliciou, digg, reddit, twitter och identi.ca. Ladda upp bilder på Facebook, FlickR,
ipernity och Picasa och posta dom till relevanta grupper. Glöm inte att tagga dom med
<strong>kde</strong>-taggen så att det blir lättare att hitta.</p>

<p align="justify">
Om du vill följa vad som händer runt KDE SC 4.4 gå till den helt nya
<a href="http://buzz.kde.org"><strong>KDE Community livefeed</strong></a>. Sajten aggregerar
allt vad som händer på de sociala nätverken om kde.
</p>

<div align="center">
<table border="0" cellspacing="2" cellpadding="2">
<tr>
    <td>
        <a href="http://digg.com/linux_unix/KDE_Software_Compilation_4_4_0_Introduces_new_innovations"><img src="/announcements/buttons/digg.gif" alt="Digg" title="Digg" /></a>
    </td>
    <td>
        <a href="http://www.reddit.com/r/technology/comments/azx29/kde_software_compilation_440_introduces_netbook/"><img src="/announcements/buttons/reddit.gif" alt="Reddit" title="Reddit" /></a>
    </td>
    <td>
        <a href="http://twitter.com/#search?q=kde44"><img src="/announcements/buttons/twitter.gif" alt="Twitter" title="Twitter" /></a>
    </td>
    <td>
        <a href="http://identi.ca/search/notice?q=kde44"><img src="/announcements/buttons/identica.gif" alt="Identi.ca" title="Identi.ca" /></a>
    </td>
</tr>
<tr>
    <td>
        <a href="http://www.flickr.com/photos/tags/kde44"><img src="/announcements/buttons/flickr.gif" alt="Flickr" title="Flickr" /></a>
    </td>
    <td>
        <a href="http://www.youtube.com/results?search_query=kde44"><img src="/announcements/buttons/youtube.gif" alt="Youtube" title="Youtube" /></a>
    </td>
    <td>
        <a href="http://www.facebook.com/#!/pages/K-Desktop-Environment/6344818917?ref=ts"><img src="/announcements/buttons/facebook.gif" alt="Facebook" title="Facebook" /></a>
    </td>
    <td>
        <a href="http://delicious.com/tag/kde44"><img src="/announcements/buttons/delicious.gif" alt="del.icio.us" title="del.icio.us" /></a>
    </td>
</tr>
</table>
<style="font-size: 5pt"><a href="http://microbuttons.wordpress.com">microbuttons</a></style>
</div>

<h4>
  Installera KDE SC 4.4.0
</h4>
<p align="justify">
KDE, inklusive alla sina bibliote och applikationer är fritt tillgängligt under öppen källkodslicenser och på flera
olika plattformar inklusive Linux och andra UNIX-baserade operativsystem. De flesta KDE-applikationer går även att köra
på Microsoft Windows från <a href="http://windows.kde.org">KDE på Windows</a> samt Apple Mac OS X från
<a href="http://mac.kde.org/">KDE på Mac</a>. Det finns även experimentella versioner byggda för några olika
mobila plattformar som MS Windows Mobile och Symbian.
<br />
Hämta binära paket här <a
href="http://download.kde.org/stable/4.4.0/">download.kde.org</a> eller på en
<a href="/download">CD-ROM</a>
eller med någon av <a href="/distributions">de större Linux och UNIX distributionerna
</a> som finns idag.
</p>



