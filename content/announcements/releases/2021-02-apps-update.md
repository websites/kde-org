---
title: KDE's February 2021 Apps Update
publishDate: "2021-02-04T11:00:00+00:00" # todo change me
layout: page
type: announcement
---

This month, a new app to guide you through FOSDEM's busy timetable and a new version for our time planner app.

## Kongress 1.0 release

{{< img class="text-center img-fluid" src="kongress.png" caption="Kongress" style="width: 600px">}}

The launch of [Kongress 1.0](https://dimitris.cc/kde/2021/01/28/Kongress_1_0.html) took place this month.  It's a conference timetable guide app and is launched just in time for [FOSDEM](https://fosdem.org/2021/), one of the world's largest free software conferences happening online this weekend.  You can use Kongress to browse the timetable and favourite all the important talks.  You can adjust times to your local timezone to avoid confusion. 

KDE will have a stand at FOSDEM with demos and welcoming faces, and as an online event people come visit from all over the world so do say hi on the stall Matrix channel [#kde-stand:fosdem.org](https://chat.fosdem.org/#/room/#kde-stand:fosdem.org).

Kongress is available in your Linux distro. You can also install the flatpak version of Kongress from the software repository of your distribution or directly from [flathub](https://flathub.org/apps/details/org.kde.kongress). Android users can also try the nightly build from the [F-Droid](https://community.kde.org/Android/FDroid) repository of KDE.

## Calligra Plan 3.3

{{< img class="text-center img-fluid" src="plan-summary.png" caption="Plan Summary" style="width: 600px">}}

[Calligra Plan](https://calligra.org/plan/) is our project schedule and tracking app.  [Version 3.3](https://mail.kde.org/pipermail/kde-announce-apps/2021-January/005631.html) was released this week and mainly improves printing. You can now select a time range for printing. You can scale your print to a single page or to the page height or over multiple pages.  And it uses a colour palette that works better on white paper.

Plan is available in your Linux distro.

## Bugfixes

Hex editor [Okteta 0.26.5](https://mail.kde.org/pipermail/kde-announce-apps/2021-February/005635.html) updates translations, makes it easier to select colour scheme and lets your turn off cursor blinking.
Matrix chat app [Neochat 1.0.1](https://carlschwan.eu/2021/01/13/neochat-1.0.1-first-bugfix-release/) fixes editing messages, showing user avatars, saves images and fixes pesky graphics glitches.

## Releases 20.12.2

Some of our projects release on their own timescale and some get released en-masse. The 20.12.2 bundle of projects was released today with dozens of bugfixes and will be available through app stores and distros soon. See the [20.12.2 releases page](https://www.kde.org/info/releases-20.12.2) for details.

Some of the fixes in today's bugfix releases include:

 * Ark no longer crashes when closing the window while loading a TAR archive
 * Dolphin calculates the folder size on FUSE and network file systems correctly now
 * Konsole no longer crashes when exiting all tabs at the same time
 * Dictionaries can be added in Kiten's config dialog
 * Umbrello doesn't crash anymore if exiting when a widget in a diagram is selected

[20.12 release notes](https://community.kde.org/Releases/20.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.12.2 source info page](https://kde.org/info/releases-20.12.2) &bull; [20.12.2 full changelog](https://kde.org/announcements/fulllog_releases-20.12.2/)

## Contributing

If you would like to help, [get involved](https://community.kde.org/Get_Involved)! We are a friendly and accepting community and we need developers, writers, translators, artists, documenters, testers and evangelists. You can also visit our [welcome chat channel](https://go.kde.org/matrix/#/#kde-welcome:kde.org) and talk live to active KDE contributors.

Another way to help KDE is by [donating to KDE](https://kde.org/community/donations/) and helping the Community keep things running.
