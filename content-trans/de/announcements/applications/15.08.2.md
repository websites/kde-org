---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE veröffentlicht die KDE-Anwendungen 15.08.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 15.08.2
version: 15.08.2
---
13. Oktober 2015. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../15.08.0'>KDE-Anwendungen 15.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 30 aufgezeichnete Fehlerkorrekturen enthalten Verbesserungen für Ark, Gwenview, Kate, KBruch, kdelibs, kdepim, Lokalize und Umbrello.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
