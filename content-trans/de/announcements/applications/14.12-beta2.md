---
aliases:
- ../announce-applications-14.12-beta2
custom_spread_install: true
date: '2014-11-13'
description: KDE veröffentlicht die Anwendungen 14.12 Beta 2.
layout: application
title: KDE veröffentlicht die zweite Beta-Version der Anwendungen 14.12
---
13. November 2014. Heute veröffentlicht KDE die Beta-Version der neuen Ausgaben der KDE-Anwendungen. Mit dem Einfrieren von Abhängigkeiten und Funktionen konzentriert sich das KDE-Team auf die Behebung von Fehlern und Verbesserungen.

Bei den verschiedenen Anwendungen auf der Grundlage der KDE Frameworks 5 sind gründliche Tests für die Veröffentlichung der KDE Anwendungen 14.12 nötig, um die Qualität und Benutzererfahrung beizubehalten und zu verbessern. Anwender, die KDE täglich benutzen, sind sehr wichtig, um die hohe Qualität der KDE-Software zu erhalten, weil Entwickler nicht jede mögliche Kombination von Anwendungsfällen testen können. Diese Benutzer können Fehler finden, so dass sie vor der endgültigen Veröffentlichung korrigiert werden können. Beteiligen Sie sich beim Team und installieren Sie die Beta-Version und berichten Sie alle <a href='https://bugs.kde.org/'>Fehler</a>.

#### Binärpakete für KDE-Anwendungen 14.12 Beta2 installieren

<em>Pakete</em>. Einige Anbieter von Linux-/UNIX-Betriebssystemen haben dankenswerterweise Binärpakete der KDE-Anwendungen 14.12 Beta 2 (intern 14.11.90) für einige Versionen Ihrer Distributionen bereitgestellt, ebenso wie freiwillige Mitglieder der Gemeinschaft. Zusätzliche binäre Pakete und Aktualisierungen der jetzt verfügbaren Pakete werden in den nächsten Wochen bereitgestellt.

<em>Paketquellen</em>. Eine aktuelle Liste aller Binärpakete, von denen das KDE-Projekt in Kenntnis gesetzt wurde, finden Sie im <a href='http://community.kde.org/KDE_SC/Binary_Packages'>Community Wiki</a>.

#### Kompilieren der KDE-Anwendungen 14.12 Beta 2

Der vollständige Quelltext für 4.12 Beta2 kann <a href='http://download.kde.org/unstable/applications/14.11.90/src/'>hier</a> heruntergeladen werden. Anweisungen zum Kompilieren und Installieren von 4.11.90 finden Sie auf der <a href='/info/applications/applications-14.11.90'>Beta 2 Infoseite</a>.
