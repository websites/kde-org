---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE veröffentlicht die KDE-Anwendungen 17.12.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.12.2
version: 17.12.2
---
08. Februar 2018. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../17.12.0'>KDE-Anwendungen 17.12</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Dolphin, Gwenview, KGet und Okular.
