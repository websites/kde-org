---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE veröffentlicht die KDE-Anwendungen 17.08.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 17.08.1
version: 17.08.1
---
07. September 2017. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../17.08.0'>KDE-Anwendungen 17.08</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 20 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello und die KDE-Spiele.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
