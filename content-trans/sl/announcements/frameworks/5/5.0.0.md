---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE objavlja prvo izdajo Frameworks 5.
layout: framework
qtversion: 5.2
title: Prva izdaja KDE Frameworks 5
---
7. julij 2014. Skupnost KDE s ponosom objavlja izid KDE Frameworks 5.0. Frameworks 5 je naslednja generacija knjižnic KDE, modulariziranih in optimiziranih za enostavno integracijo v aplikacijah Qt. Nudijo široko paleto običajno potrebne funkcionalnost v zrelih, vrstniško pregledanih in dobro preizkušenih knjižnicah s prijaznimi licencami. Obstaja več kot 50 različnih okvirjev, ki kot del te izdaje zagotavljajo rešitve, vključno z integracijo strojne opreme, podporo za različne zapise datotek, dodatne pripomočke, funkcije za izris, preverjanje črkovanja in še več. Mnogi okviri delujejo na več platformah in imajo minimalne ali so brez dodatnih odvisnosti, kar omogoča enostavno gradnjo in dodajanje k poljubni aplikaciji Qt.

Okviri KDE predstavljajo prizadevanje za preučitev zmogljivih knjižnic platforme KDE 4 v sklop neodvisnih modulov za različne platforme, ki bodo lahko na voljo vsem razvijalcem Qt za poenostavitev, pospeševanje in zmanjšanje stroškov razvoja Qt. Posamezni okvirji delujejo na več platformah in so dobro dokumentirani in preizkušeni, njihova uporaba pa bo dobro znana razvijalcem Qt, po slogu in standardih, ki jih določa projekt Qt. Okviri so razviti v okviru dokazanega modela upravljanja KDE s predvidljivim razporedom izdaj, jasnim in od dobaviteljev nevtralnim procesom prispevanja, odprtim upravljanjem in prilagodljivim licenciranjem (LGPL).

The Frameworks have a clear dependency structure, divided into Categories and Tiers. The Categories refer to runtime dependencies:

- <strong>Functional</strong> elements have no runtime dependencies.
- <strong>Integration</strong> designates code that may require runtime dependencies for integration depending on what the OS or platform offers.
- <strong>Solutions</strong> have mandatory runtime dependencies.

The <strong>Tiers</strong> refer to compile-time dependencies on other Frameworks. Tier 1 Frameworks have no dependencies within Frameworks and only need Qt and other relevant libraries. Tier 2 Frameworks can depend only on Tier 1. Tier 3 Frameworks can depend on other Tier 3 Frameworks as well as Tier 2 and Tier 1.

The transition from Platform to Frameworks has been in progress for over 3 years, guided by top KDE technical contributors. Learn more about Frameworks 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>in this article from last year</a>.

## Poudarki

There are over 50 Frameworks currently available. Browse the complete set <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>in the online API documentation</a>. Below an impression of some of the functionality Frameworks offers to Qt application developers.

<strong>KArchive</strong> offers support for many popular compression codecs in a self-contained, featureful and easy-to-use file archiving and extracting library. Just feed it files; there's no need to reinvent an archiving function in your Qt-based application!

<strong>ThreadWeaver</strong> offers a high-level API to manage threads using job- and queue-based interfaces. It allows easy scheduling of thread execution by specifying dependencies between the threads and executing them satisfying these dependencies, greatly simplifying the use of multiple threads.

<strong>KConfig</strong> is a Framework to deal with storing and retrieving configuration settings. It features a group-oriented API. It works with INI files and XDG-compliant cascading directories. It generates code based on XML files.

<strong>Solid</strong> offers hardware detection and can inform an application about storage devices and volumes, CPU, battery status, power management, network status and interfaces, and Bluetooth. For encrypted partitions, power and networking, running daemons are required.

<strong>KI18n</strong> adds Gettext support to applications, making it easier to integrate the translation workflow of Qt applications in the general translation infrastructure of many projects.
