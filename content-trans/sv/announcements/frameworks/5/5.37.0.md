---
aliases:
- ../../kde-frameworks-5.37.0
date: 2017-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nytt ramverk: kirigami, en uppsättning QtQuick-insticksprogram för att skapa användargränssnitt baserade på KDE UX-riktlinjerna

### Breeze-ikoner

- update .h och .h++ färger (fel 376680)
- Ta bort ktorrent liten svartvit ikon (fel 381370)
- Bokmärken är en åtgärdsikon inte en katalogikon (fel 381383)
- Uppdatera utilities-system-monitor (fel 381420)

### Extra CMake-moduler

- Lägg till --gradle i androiddeployqt
- Rätta installation av apk mål
- Rätta användning av query_qmake: åtskilj anrop som förväntar sig qmake eller inte
- Lägg till dokumentation av programmeringsgränssnitt för KDEInstallDirs' KDE_INSTALL_USE_QT_SYS_PATHS
- Lägg till metainfo.yaml för att göra ECM till ett riktigt ramverk
- Android: Sök efter QML-filer i källkodskatalogen, inte i installationskatalogen

### KActivities

- Skicka runningActivityListChanged när en aktivitet skapas

### KDE Doxygen-verktyg

- Undanta HTML från sökförfrågan

### KArchive

- Lägg till Conan-filer, som ett första experiment för att stödja Conan

### KConfig

- Tillåt att bygga KConfig utan Qt5Gui
- Standardgenvägar: Använd Ctrl+PageUp/PageDown för föregående/nästa flik

### KCoreAddons

- Ta bort deklaration av unused init() från K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY_SKEL
- Nytt spdx programmeringsgränssnitt i KAboutLicense för att hämta SPDX-licensuttryck
- kdirwatch: Undvik möjlig krasch om d-ptr förstörs innan KDirWatch (fel 381583)
- Rätta visning av formatDuration med avrundning (fel 382069)

### KDeclarative

- Rätta att plasmashell tar bort QSG_RENDER_LOOP

### Stöd för KDELibs 4

- Rätta 'Tips om föråldrat anrop KUrl::path() är felaktigt på Windows' (fel 382242)
- Uppdatera kdelibs4support att använda målbaserat stöd som tillhandahålls av kdewin
- Markera att också användning av konstruktorer avråds från
- Synkronisera KDE4Defaults.cmake från kdelibs

### KDesignerPlugin

- Lägg till stöd för den nya grafiska komponenten kpasswordlineedit

### KHTML

- Stöd också SVG (fel 355872)

### KI18n

- Tillåt att ladda i18n-kataloger från godtyckliga platser
- Säkerställ att målet tsfiles skapas

### KIdleTime

- Kräv bara Qt5X11Extras när det faktiskt behövs

### KInit

- Använd riktig funktionalitetsflagga för att inkludera kill(2)

### KIO

- Lägg till ny metod urlSelectionRequested i KUrlNavigator
- KUrlNavigator: Exponera KUrlNavigatorButton som tar emot drop händelsen
- Göm utan att fråga användaren med en kopiera/avbryt meddelanderuta
- Säkerställ att KDirLister uppdaterar objekt vars målwebbadress har ändrats (fel 382341)
- Gör avancerade alternativ i dialogrutan "öppna med" hopfällbara  och normalt dolda (fel 359233)

### KNewStuff

- Ge KMoreToolsMenuFactory menyer ett överliggande objekt
- När poster begärs från cachen, rapportera alla på en gång

### Ramverket KPackage

- Nu kan kpackagetool mata ut appstream-data till en fil
- Använd ny KAboutLicense::spdx

### KParts

- Nollställ webbadress i loseUrl()
- Lägg till mall för ett enkelt kpart-baserat program
- Sluta använd KDE_DEFAULT_WINDOWFLAGS

### KTextEditor

- Hantera finkorniga hjulhändelser vid zoomning
- Lägg till mall för ett ktexteditor insticksprogram
- Kopiera rättigheter från originalfilen vid spara kopia (fel 377373)
- Undvik möjligtvis krasch i stringbuild (fel 339627)
- Rätta problem med att * läggs till för rader utanför kommentarer (fel 360456)
- Rätta spara som kopia, som saknade tillåtelse att skriva över målfilen (fel 368145)
- Kommandot 'set-highlight': Sammanfoga argument med mellanslag
- Rätta krasch när vy förstörs på grund av icke-deterministisk städning av objekt
- Skicka signaler från ikonkant när inget märke klickades
- Rätta krasch i vi-inmatningsläge (följd: "o" "Esc" "O" "Esc" ".") (fel 377852)
- Använd ömsesidigt uteslutande grupp i Default Mark Type

### KUnitConversion

- Markera MPa och PSI som vanliga enheter

### Ramverket KWallet

- Använd CMAKE_INSTALL_BINDIR för D-Bus tjänstgenerering

### Kwayland

- Förstör alla kwayland-objekt skapade av registry när det förstörs
- Skicka connectionDied om QPA förstörs
- [klient] Spåra alla skapade ConnectionThreads och lägg till programmeringsgränssnitt för att komma åt dem
- [server] Skicka lämna textinmatning om fokuserad yta blir obunden
- [server] Skicka lämna pekare om fokuserad yta blir obunden
- [klient] Följ enteredSurface på ett riktigt sätt i Keyboard
- [server] Skicka lämna tangentbord när klienten förstör den fokuserade ytan (fel 382280)
- Kontrollera buffertens giltighet (fel 381953)

### KWidgetsAddons

- Extrahera grafisk radeditorkomponent för lösenord ⇒ ny klass KPasswordLineEdit
- Rättade en krasch vid sökning med handikappstöd aktiverat (fel 374933)
- [KPageListViewDelegate] Skicka grafisk komponent till drawPrimitive i drawFocus

### KWindowSystem

- Ta bort deklarationens beroende av QWidget

### KXMLGUI

- Sluta använd KDE_DEFAULT_WINDOWFLAGS

### NetworkManagerQt

- Lägg till stöd för ipv*.route-metric
- Rätta odefinierade uppräkningsvärden NM_SETTING_WIRELESS_POWERSAVE_FOO (fel 382051)

### Plasma ramverk

- [Containment Interface] Skicka alltid contextualActionsAboutToShow för omgivning
- Hantera beteckningar för knapp och verktygsknapp som vanlig text
- Utför inte wayland-specifika rättningar när X används (fel 381130)
- Lägg till KF5WindowSystem i länkgränssnitt
- Deklarera AppManager.js som pragma library
- [PlasmaComponents] Ta bort Config.js
- Använd vanlig text som förval för beteckningar
- Ladda översättningar från KPackage-filer om hopslagna (fel 374825)
- [PlasmaComponents Menu] Krascha inte för nollåtgärd
- [Plasma Dialog] Rätta flaggvillkor
- Uppdatera akregator ikon för systembricka (fel 379861)
- [Containment Interface] Behåll omgivning i RequiresAttentionStatus medan sammanhangsberoende meny visas (fel 351823)
- Rätta hantering av layoutnyckel för flikrad med höger-till-vänster (fel 379894)

### Sonnet

- Tillåt att bygga Sonnet utan Qt5Widgets
- cmake: Skriv om FindHUNSPELL.cmake att använda pkg-config

### Syntaxfärgläggning

- Tillåt att bygga KSyntaxHighlighter utan Qt5Gui
- Lägg till stöd för korskompilering för färgläggningsindexering
- Teman: Ta bort all oanvänd metadata (licens, upphovsman, skrivskyddad)
- Tema: Ta bort fälten licens och upphovsman
- Tema: Härled skrivskyddsflagga från filen på disk
- Lägg till syntaxfärgläggning för datamodelleringsspråket YANG
- PHP: Lägg till PHP 7 nyckelord (fel 356383)
- PHP: Städa PHP 5 information
- Rätta gnuplot, gör inledande eller efterföljande mellanslag till allvarliga fel
- Rätta detektering av 'else if', vi behöver byta sammanhang, lägg till extra regel
- Indexering kontrollerar inledande och efterföljande blanktecken i XML-färgläggning
- Doxygen: Lägg till färgläggning av Doxyfile
- Lägg till saknade standardtyper i C-färgläggning och uppdatera till C11 (fel 367798)
- Q_PI D ⇒ Q_PID
- PHP: Förbättra färgläggning av variabler i klammerparenteser inom dubbla citationstecken (fel 382527)
- Lägg till färgläggning av PowerShell
- Haskell: Lägg till filändelsen .hs-boot (startmodul) (fel 354629)
- Rätta replaceCaptures() så att det fungerar med fler än 9 lagrade värden
- Ruby: Använd WordDetect istället för StringDetect för fullständig ordmatchning
- Rätta felaktig färgläggning av BEGIN och END i ord som "EXTENDED" (fel 350709)
- PHP: Ta bort mime_content_type() från listan över funktioner som avråds från (fel 371973)
- XML: Lägg till XBEL-filändelse och Mime-typ för XML-färgläggning (fel 374573)
- Bash: Rätta felaktig färgläggning av kommandoväljare (fel 375245)
- Perl: Rätta färgläggning av heredoc med inledande mellanslag i avskiljaren (fel 379298)
- Uppdatera SQL (Oracle)-syntaxfil (fel 368755)
- C++: Rätta '-' så att det inte är en del av UDL-sträng (fel 380408)
- C++: Formatspecifikation för printf: lägg till 'n' och 'p', ta bort 'P' (fel 380409)
- C++: Rätta char-värde så att det har strängarnas färg (fel 380489)
- VHDL: Rätta färgläggningsfel när parenteser och attribut används (fel 368897)
- zsh-färgläggning: Rätta matematiskt uttryck i ett delsträngsuttryck (fel 380229)
- Javascript-färgläggning: Lägg till stöd för E4X xml-filändelse (fel 373713)
- Ta bort filändelseregel "*.conf"
- Pug- och Jade-syntax

### ThreadWeaver

- Lägg till saknad export till QueueSignals

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
