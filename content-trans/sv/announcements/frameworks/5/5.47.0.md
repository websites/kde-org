---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Avsluta frågekörning tidigt om subterm returnerar tom resultatmängd
- Undvik krasch vid läsning av felaktig data från databasen över dokumenttermer (fel 392877)
- Hantera stränglistor som indata
- Ignorera flera typer av källfiler (fel 382117)

### Breeze-ikoner

- Uppdatera grepp och överflödesmeny

### Extra CMake-moduler

- Android-verktygskedja: Tillåt att ange extra bibliotek manuellt
- Android: Definiera inte qml-import-paths om den är tom

### KArchive

- Hantera zip-filer inbäddade i zip-filer (fel 73821)

### KCMUtils

- [KCModuleQml] Ignorera inaktiverade kontroller vid användning av tabulator

### KConfig

- kcfg.xsd - Kräv inte ett kcfgfile-element

### KConfigWidgets

- Rätta färgschemat "Förval" så att det motsvarar Breeze igen

### KDeclarative

- Ställ in IM-sammanhangsegenskap för rätt sammanhang
- [Plotter] Återge inte om m_node är null (fel 394283)

### KDocTools

- Uppdatera listan över ukrainska poster
- Lägg till entiteten OSD i general.entites
- Lägg till entiteterna CIFS, NFS, Samba, SMB i general.entities
- Lägg till Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM i general.entities

### KFileMetaData

- Kontrollera att ffmpeg har minst version 3.1, som introducerade programmeringsgränssnittet som vi kräver
- Sök efter taggarna album artist och albumartist i taglibextractor
- popplerextractor: Försök inte gissa titel om ingen finns

### KGlobalAccel

- Säkerställ att tangentbordsbegäran ungrab behandlas innan genväg sänds (fel 394689)

### KHolidays

- holiday_es_es - Rätta dag för "Comunidad de Madrid"

### KIconThemes

- Kontrollera om group &lt; LastGroup, eftersom KIconEffect ändå inte hanterar UserGroup

### KImageFormats

- Ta bort duplicerade Mime-typer från json-filer

### KIO

- Kontrollera också om målet finns när binärdata klistras in (fel 394318)
- Auth-stöd: Returnera uttagsbuffertens verkliga längd
- Auth-stöd: Förena programmeringsgränssnitt för delning av fildeskriptorer
- Auth-stöd: Skapa uttagsfil i användarens körtidskatalog
- Auth-stöd: Ta bort uttagsfil efter användning
- Auth-stöd: Flytta uppgiften att rensa uttagsfil till FdReceiver
- Auth-stöd: Använd abstrakt uttag för att dela fildeskriptor på Linux
- [kcoredirlister] Ta bort så många url.toString() som möjligt
- KFileItemActions: Återgå till förvald Mime-typ när bara filer markeras (fel 393710)
- Introducera KFileItemListProperties::isFile()
- KPropertiesDialogPlugin kan nu ange flera protokoll som stöds genom att använda X-KDE-Protocols
- Bevara fragment vid omdirigering från http till https
- [KUrlNavigator] Skicka tabRequested när sökväg i platsväljarmenyn mittenklickas
- Prestanda: Använd den nya uds-implementeringen
- Omdirigera inte smb:/ till smb:// och därefter till smb:////
- Tillåt att acceptera med dubbelklick i dialogrutan för att spara (fel 267749)
- Aktivera normalt förhandsgranskning i dialogrutan filväljare
- Dölj förhandsgranskning av fil när ikonen är för liten
- i18n: Använd åter pluralform för insticksprogrammeddelande
- Använd en vanlig dialogruta istället för en listdialogruta när en enda fil flyttas till papperskorgen eller tas bort
- Låt varningstexten vid borttagningsåtgärder betona att den är permanent och irreversibel
- Återställ "Visa knappar för visningsläge i verktygsraden för dialogrutorna öppna och spara"

### Kirigami

- Visa action.main mer framträdande i ToolBarApplicationHeader
- Tillåt att bygga Kirigami utan beroende på KWin ritplatteläge
- Korrigera swipefilter för höger-till-vänster
- Korrigera storleksändring av contentItem
- Rätta beteende hos --reverse
- Dela contextobject för att alltid komma åt i18n
- Säkerställ att verktygstips är dolt
- Säkerställ att inte ogiltiga varianter tilldelas till de följda egenskaperna
- Hantera inte ett musområde, signalen dropped()
- Ingen effekt när pekaren hålls över på mobiltelefon
- Riktiga ikoner för overflow-menu-left och right
- Dra grepp för att ordna om objekt i ListView
- Använd minnesregler på verktygsradsknapparna
- Tillägg av saknade filer i QMake .pri
- [Dokumentation av programmeringsgränssnitt] Rätta Kirigami.InlineMessageType -&gt; Kirigami.MessageType
- Rätta applicationheaders i applicationitem
- Tillåt inte att lådan visas eller döljs när det inte finns något grepp (fel 393776)

### KItemModels

- KConcatenateRowsProxyModel: Sanera indata riktigt

### KNotification

- Rätta krascher i NotifyByAudio när program stängs

### Ramverket KPackage

- kpackage*install**package: Rätta saknat beroende mellan .desktop and .json
- Säkerställ att sökvägar i rcc aldrig härleds från absoluta sökvägar

### Kör program

- Behandla D-Bus svar i tråden ::match (fel 394272)

### KTextEditor

- Använd inte stora inledande bokstäver för kryssrutan "visa ordantal"
- Gör antal ord och tecken till en global inställning

### Kwayland

- Öka version för org_kde_plasma_shell-gränssnitt
- Lägg till "SkipSwitcher" i programmeringsgränssnitt
- Lägg till utdataprotokollet XDG

### KWidgetsAddons

- [KCharSelect] Rätta tabellens cellstorlek med Qt 5.11
- [Dokumentation av programmeringsgränssnitt] Ta bort användning av overload, som orsakade felaktiga dokument
- [Dokumentation av programmeringsgränssnitt] Tala om för doxygen att "e.g." inte avslutar meningen, använd ". "
- [Dokumentation av programmeringsgränssnitt] Ta bort onödiga HTML undantagstecken
- Ställ inte automatiskt in standardikonerna för varje stil
- Låt KMessageWidget motsvara stilen hos Kirigami inlineMessage (fel 381255)

### NetworkManagerQt

- Gör information om ohanterad egenskap till bara felsökningsmeddelanden
- WirelessSetting: Implementera egenskapen assignedMacAddress

### Plasma ramverk

- Mallar: Konsekvent namngivning, rätta namn på översättningskataloger, med mera
- [Breeze Plasma Theme] Rätta ikon för kleopatra så att den använder färgstilmall (fel 394400)
- [Dialog] Hantera att dialogrutan minimeras snyggt (fel 381242)

### Syfte

- Förbättra integrering av Telegram
- Behandla inre fält som ELLER-begränsningar istället för OCH
- Gör det möjligt att begränsa insticksprogram med närvaro av en skrivbordsfil
- Gör det möjligt att filtrera insticksprogram enligt körbart program
- Markera vald enhet i insticksprogrammet KDE-anslut
- Rätta i18n-problem i frameworks/purpose/plugins
- Lägg till insticksprogram för Telegram
- kdeconnect: Underrätta om att processen misslyckas starta (fel 389765)

### QQC2StyleBridge

- Använd bara palettegenskap när qtquickcontrols 2.4 används
- Fungera med Qt&lt;5.10
- Rätta flikradens höjd
- Använd Control.palette
- [RadioButton] Byt namn på "control" till "controlRoot"
- Ange inte explicita mellanrum för RadioButton/CheckBox
- [FocusRect] Använd manuell placering istället för grepp
- Det visar sig att flickable i en scrollview är contentItem
- Visa fokusrektangel när CheckBox eller RadioButton har fokus
- Hackig rättning av scrollview-detektering
- Låt inte mousearea bli överliggande objekt till flickable
- [TabBar] Byt flikar med mushjul
- Control får inte har underliggande objekt (fel 394134)
- Begränsa rullning (fel 393992)

### Syntaxfärgläggning

- Perl6: Lägg till stöd för filändelserna .pl6, .p6, or .pm6 (fel 392468)
- DoxygenLua: Rätta avslutande kommentarblock (fel 394184)
- Lägg till pgf till Latex-liknande filformat (samma format som tikz)
- Lägg till postgresql-nyckelord
- Färgläggning av OpenSCAD
- debchangelog: Lägg till Cosmic Cuttlefish
- cmake: Rätta varningen DetectChar om undantaget bakstreck
- Pony: Rätta identifierare och nyckelord
- Lua: Uppdaterad för Lua5.3

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
