---
aliases:
- ../../kde-frameworks-5.41.0
date: 2017-12-10
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Plocka isär och skriv om Baloo I/O-slav för etiketter (fel 340099)

### BluezQt

- Läck inte rfkill fildeskriptorer (fel 386886)

### Breeze-ikoner

- Lägg till saknade ikonstorlekar (fel 384473)
- Lägg till installations- och avinstallationsikoner för Discover

### Extra CMake-moduler

- Lägg till beskrivningsetiketten i de genererade pkgconfig-filerna
- ecm_add_test: Använd rätt sökvägsseparator på Windows
- Lägg till FindSasl2.cmake i ECM
- Skicka bara ARGS-saken när Makefiles görs
- Lägg till FindGLIB2.cmake och FindPulseAudio.cmake
- ECMAddTests: Ställ in QT_PLUGIN_PATH så att lokalt byggda insticksprogram kan hittas
- KDECMakeSettings: Mer dokumentation om byggkatalogens layout

### Integrering med ramverk

- Stöd nerladdning av den andra och tredje nerladdningslänken från en KNS-produkt (fel 385429)

### KActivitiesStats

- Börja rätta libKActivitiesStats.pc: (fel 386933)

### KActivities

- Rätta kapplöpning som startar kactivitymanagerd flera gånger

### KAuth

- Gör det möjligt att bara bygga kauth-policy-gen kodgenerator
- Lägg till en not om att anropa hjälpprogrammet från flertrådiga program

### KBookmarks

- Visa inte alternativet för att redigera bokmärken om keditbookmarks inte är installerat
- Konvertera från KAuthorized::authorizeKAction som avråds från till authorizeAction

### KCMUtils

- Tangentbordsnavigering till och från QML-inställningsmoduler

### KCompletion

- Krascha inte när ny radeditor anges för en redigerbar kombinationsruta
- KComboBox: Returnera tidigt när redigerbar ställs in till tidigare värde
- KComboBox: Återanvänd befintligt kompletteringsobjekt för ny radeditor

### KConfig

- Leta inte efter /etc/kderc varenda gång

### KConfigWidgets

- Uppdatera standardfärger för att stämma med nya färger i D7424

### KCoreAddons

- Inmatningsvalidering för deljobb
- Varna om fel när json-filer tolkas
- Installera Mimetyp-definitioner för kcfg/kcfgc/ui.rc/knotify och qrc-filer
- Lägg till en ny funktion för att mäta längden enligt text
- Rätta fel i KAutoSave för fil med blanktecken

### KDeclarative

- Gör så att kompilering på Windows fungerar
- Gör så att kompilering med QT_NO_CAST_FROM_ASCII och QT_NO_CAST_FROM_BYTEARRAY fungerar
- [MouseEventListener] Tillåt att mushändelser accepteras
- Använd ett enda QML-gränssnitt

### KDED

- kded: Ta bort D-Bus anrop till ksplash

### KDocTools

- Uppdatera översättning till brasiliansk portugisiska
- Uppdatera översättning till ryska
- Uppdatera översättning till ryska
- Uppdatera customization/xsl/ru.xml (nav-home saknades)

### KEmoticons

- KEmoticons: Konvertera insticksprogram till JSON och lägg till stöd för att ladda med KPluginMetaData
- Läck inte symboler för pimpl-klasser, skydda med Q_DECL_HIDDEN

### KFileMetaData

- Testen usermetadatawritertest kräver Taglib
- Om egenskapsvärdet är null, ta bort attributet user.xdg.tag (fel 376117)
- Öppna filer skrivskyddade i TagLib-extrahering

### KGlobalAccel

- Gruppera några blockerande anrop till D-Bus
- kglobalacceld: Undvik att ladda en ikonladdare utan orsak
- Generera korrekta genvägssträngar

### KIO

- KUriFilter: Filtrera bort duplicerade insticksprogram
- KUriFilter: Förenkla datastrukturer, rätta minnesläcka
- [CopyJob] Starta inte om från början efter att ha tagit bort en fil
- Rätta att skapa en katalog via KNewFileMenu+KIO::mkpath på Qt 5.9.3+ (fel 387073)
- Skapade en hjälpfunktion 'KFilePlacesModel::movePlace'
- Exponera rollen KFilePlacesModel 'iconName'
- KFilePlacesModel: Undvik onödig signal 'dataChanged'
- Returnera ett giltigt bokmärkesobjekt för alla poster i KFilePlacesModel
- Skapa funktionen 'KFilePlacesModel::refresh'
- Skapa statiska funktionen 'KFilePlacesModel::convertedUrl'
- KFilePlaces: Skapade sektionen 'remote'
- KFilePlaces: Lägg till en sektion för flyttbara enheter
- Tillägg av Baloo webbadresser i platsmodellen
- Rätta KIO::mkpath med qtbase 5.10 beta 4
- [KDirModel] Skicka ut ändring för HasJobRole när jobb ändras
- Ändra beteckning "Avancerade alternativ" &gt; "Terminalalternativ"

### Kirigami

- Positionera rullningslisten enligt rubrikstorleken (fel 387098)
- Undre marginal baserad på närvaro av åtgärdsknapp
- Anta inte att applicationWindow() är tillgängligt
- Underrätta inte om värdeändring om vi fortfarande är i konstruktorn
- Ersätt biblioteksnamnet i källkoden
- Stöd färger på fler ställen
- Färgikoner i verktygsrader vid behov
- Ta hänsyn till ikonfärger i huvudåtgärdsknapparna
- Starta för en "icon" grupperad egenskap

### KNewStuff

- Återställ "Koppla loss innan d-pekaren tilldelas" (fel 386156)
- Installera inte utvecklingsverktyg för att sammanlägga skrivbordsfiler
- [knewstuff] Läck inte ImageLoader vid fel

### Ramverket KPackage

- Hantera strängar riktigt i ramverket kpackage
- Försök inte generera metadata.json om metadata.desktop inte finns
- Rätta lagring av kpluginindex i cache
- Förbättra felutmatning

### KTextEditor

- Rätta buffertkommandon i VI-läge
- Förhindra oavsiktlig zoomning

### KUnitConversion

- Konvertera från QDom till QXmlStreamReader
- Använd https för nerladdning av valutakurser

### Kwayland

- Exponera wl_display_set_global_filter som en virtuell metod
- Rätta kwayland-testXdgShellV6
- Lägg till stöd för zwp_idle_inhibit_manager_v1 (fel 385956)
- [server] Stöd inhibering av IdleInterface

### KWidgetsAddons

- Undvik inkonsekvent lösenordsdialogruta
- Ställ in tipset enable_blur_behind vid behov
- KPageListView: Uppdatera bredd vid teckensnittsändring

### KWindowSystem

- [KWindowEffectsPrivateX11] Lägg till anropet reserve()

### KXMLGUI

- Rätta översättning av namn på verktygsrad när det har i18n-sammanhang

### Plasma ramverk

- Direktivet #warning är inte universellt, och stöds i synnerhet INTE av MSVC
- [IconItem] Använd ItemSceneHasChanged istället för att ansluta vid windowChanged
- [Icon Item] Skicka explicit ut overlaysChanged i tilldelningsmetoden istället för att ansluta till det
- [Dialog] Använd KWindowSystem::isPlatformX11()
- Reducera antalet falska egenskapsändringar för ColorScope
- [Icon Item] Skicka bara ut validChanged om det faktiskt ändrats
- Undertryck onödiga rullningsindikeringar om det bläddringsbara objektet är en listvy med känd orientering
- [AppletInterface] Skicka ut ändringssignaler för configurationRequired och -Reason
- Använd setSize() istället för setProperty bredd och höjd
- Rättade ett fel där PlasmaComponents meny dök upp med felaktiga hörn(fel 381799)
- Rättade ett fel där sammanhangsberoende menyer dök upp med felaktiga hörn (fel 381799)
- Dokumentation av programmeringsgränssnitt: Lägg till anmärkning om avråd från användning funnen i git-loggen
- Synkronisera komponenten med den i Kirigami
- Sök i alla KF5-komponenter som sådana istället för som separata ramverk
- Reducera falsk signalutsändning (fel 382233)
- Lägg till signaler som anger om en skärm lagts till eller tagits bort
- Installera Switch saker
- Lita inte på inkludering av inkludering
- Optimera SortFilterModel rollnamn
- Ta bort DataModel::roleNameToId

### Prison

- Lägg till Aztec kodgenerator

### QQC2StyleBridge

- Bestäm version av QQC2 vid byggtid (fel 386289)
- Låt bakgrunden normal vara osynlig
- Lägg till en bakgrund i ScrollView

### Solid

- Snabbare UDevManager::devicesFromQuery

### Sonnet

- Gör det möjligt att korskompilera sonnet

### Syntaxfärgläggning

- Lägg till PKGUILD i bash-syntax
- JavaScript: Inkludera Mime-standardtyper
- debchangelog: Lägg till Bionic Beaver
- Uppdatera SQL (Oracle)-syntaxfil (fel 386221)
- SQL: Flytta detektering av kommentarer innan operatorer
- crk.xml: Lägg till &lt;?xml&gt; huvudrad

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
