---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE levererar KDE-program 17.08.3
layout: application
title: KDE levererar KDE-program 17.08.3
version: 17.08.3
---
9:e november, 2017. Idag ger KDE ut den tredje stabilitetsuppdateringen av <a href='../17.08.0'>KDE-program 17.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Omkring ett dussin registrerade felrättningar omfattar förbättringar av bland annat Kontact, Ark, Gwenview, KGpg, KWave, Okular och Spectacle.

Utgåvan inkluderar också den sista versionen av KDE:s utvecklingsplattform 4.14.38.

Förbättringar omfattar:

- Provisorisk lösning för en regression i Samba 4.7 med lösenordsskyddade delade SMB-resurser
- Okular kraschar inte längre efter vissa rotationsjobb
- Ark bevarar ändringsdatum för filer när ZIP-arkiv packas upp
