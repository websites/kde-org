---
aliases:
- ../announce-applications-19.08.2
changelog: true
date: 2019-10-10
description: KDE levererar Program 19.08.2.
layout: application
major_version: '19.08'
release: applications-19.08.2
title: KDE levererar Program 19.08.2
version: 19.08.2
---
{{% i18n_date %}}

Idag ger KDE ut den andra stabilitetsuppdateringen av <a href='../19.08.0'>KDE-program 19.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Mer än tjugo registrerade felrättningar omfattar förbättringar av bland annat Kontact, Dolphin, Gwenview, Kate, Kdenlive, Konsole, Lokalize och Spectacle.

Förbättringar omfattar:

- Stöd för hög upplösning har förbättrats i Terminal och andra program
- Byte mellan olika sökningar i Dolphin uppdaterar nu sökparametrar riktigt
- Kmail kan åter spara brev direkt i fjärrkorgar
