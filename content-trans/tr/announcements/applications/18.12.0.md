---
aliases:
- ../announce-applications-18.12.0
changelog: true
date: 2018-12-13
description: KDE Uygulamalar 18.12.'yi Gönderdi
layout: application
release: applications-18.12.0
title: KDE, KDE Uygulamalar 18.12.0'ı Gönderdi
version: 18.12.0
---
{{% i18n_date %}}

KDE Uygulamaları %[1] şimdi yayınlandı.

{{%youtube id="ALNRQiQnjpo"%}}

We continuously work on improving the software included in our KDE Application series, and we hope you will find all the new enhancements and bug fixes useful!

## What's new in KDE Applications 18.12

More than 140 bugs have been resolved in applications including the Kontact Suite, Ark, Cantor, Dolphin, Gwenview, Kate, KmPlot, Konsole, Lokalize, Okular, Spectacle, Umbrello and more!

### Dosya Yönetimi

{{<figure src="/announcements/applications/18.12.0/app1812_dolphin01.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager:

- New MTP implementation that makes it fully usable for production
- Dosyaları SFTP protokolü üzerinden okumak için büyük performans iyileştirmesi
- For thumbnail previews, frames and shadows are now only drawn for image files with no transparency, improving the display for icons
- New thumbnail previews for LibreOffice documents and AppImage apps
- 5 MB'den büyük video dosyaları artık dizin küçük resim oluşturma etkinleştirildiğinde dizin küçük resimlerinde görüntüleniyor
- Ses CD'lerini okurken, Dolphin artık MP3 kodlayıcı için CBR bit hızını değiştirebilir ve FLAC için zaman damgalarını düzeltir
- Dolphin'in 'Kontrol' menüsü artık 'Yeni Oluştur …' öğelerini gösteriyor ve yeni bir 'Gizli Yerleri Göster' menü öğesi içeriyor
- Dolphin artık yalnızca bir sekme açık olduğunda ve standart 'sekmeyi kapat' klavye kısayoluna (Ctrl+w) basıldığında kapanıyor
- Yerler panelinden bir birimin bağlantısını kestikten sonra, artık yeniden monte etmek mümkündür
- The Recent Documents view (available by browsing to recentdocuments:/ in Dolphin) now only shows actual documents, and automatically filters out web URLs
- Dolphin, dosyayı veya dizini anında gizlenmesini sağlayacak şekilde yeniden adlandırmanıza izin vermeden önce bir uyarı gösteriyor.
- Etkin işletim sisteminizin veya ana dizininizin disklerini Yerler Panelinden kaldırmaya çalışmak artık mümkün değil

<a href='https://www.kde.org/applications/utilities/kfind'>KFind</a>, KDE's traditional file search, now has a metadata search method based on KFileMetaData.

### Ofis

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client:

- KMail can now display a unified inbox
- Yeni eklenti: Markdown Dilinden HTML postası oluşturun
- Using Purpose for Sharing Text (as email)
- HTML emails are now readable no matter what color scheme is in use

{{<figure src="/announcements/applications/18.12.0/app1812_okular01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/okular/'>Okular</a>, KDE's versatile document viewer:

- New 'Typewriter' annotation tool that can be used to write text anywhere
- The hierarchical table of contents view now has the ability to expand and collapse everything, or just a specific section
- Improved word-wrap behavior in inline annotations
- When hovering the mouse over a link, the URL is now shown anytime it could be clicked on, instead of only when in Browse mode
- ePub files containing resources with spaces in their URLs now display correctly

### Geliştirme

{{<figure src="/announcements/applications/18.12.0/app1812_kate01.png" width="600px" >}}

<a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, KDE's advanced text editor:

- Gömülü terminali kullanırken, artık mevcut dizini aktif belgenin disk üzerindeki konumu ile otomatik olarak senkronize eder
- Kate'in yerleşik terminali artık F4 klavye kısayolu kullanılarak odaklanabilir ve odak dışı bırakılabilir
- Kate’s built-in tab switcher now shows full paths for similarly-named files
- Satır numaraları artık varsayılan olarak etkindir
- İnanılmaz derecede kullanışlı ve güçlü Metin Filtresi eklentisi artık varsayılan olarak etkindir ve daha keşfedilebilir
- Hızlı Aç özelliğini kullanarak zaten açık olan bir belgeyi açmak artık o belgeye geri dönüyor
- Hızlı Aç özelliği artık yinelenen girişleri göstermiyor
- Birden fazla Aktivite kullanılırken, dosyalar artık doğru Aktivitede açılıyor
- Kate artık GNOME simge temasını kullanarak GNOME'da çalıştırıldığında tüm doğru simgeleri görüntülüyor

{{<figure src="/announcements/applications/18.12.0/app1812_konsole.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator:

- Konsole artık emoji karakterlerini tamamen destekliyor
- Inactive tabs' icons are now highlighted when they receive a bell signal
- Trailing colons are no longer considered parts of a word for the purposes of double-click selection, making it easier to select paths and 'grep' output
- Geri ve ileri düğmelerine sahip bir fare takıldığında, Konsole artık sekmeler arasında geçiş yapmak için bu düğmeleri kullanabilir.
- Konsole, artık büyütülmüş veya küçültülmüş yazıtipi boyutunu profil öntanımlısına sıfırlamak için bir menü ögesine sahiptir.
- Tabs are now harder to accidentally detach, and faster to accurately re-order
- Improved shift-click selection behavior
- Fixed double-clicking on a text line that exceeds the window width
- Escape tuşuna bastığınızda arama çubuğu bir kez daha kapanır

<a href='https://www.kde.org/applications/development/lokalize/'>Lokalize</a>, KDE's translation tool:

- Proje sekmesinde çevrilmiş dosyaları gizle
- Added basic support of pology, the syntax and glossary checking system
- Simplified navigation with tab ordering and multi-tab opening
- Fixed segfaults due to concurrent access to database objects
- Fixed inconsistent drag and drop
- Düzenleyiciler arasında farklı olan kısayollarla ilgili bir hata düzeltildi
- Improved search behavior (search will find and display plural forms)
- Restore Windows compatibility thanks to the craft building system

### İzlenceler

<a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer:

- The 'Reduce Red Eye' tool received a variety of nice usability improvements
- Gwenview now displays a warning dialog when you hide the menubar that tells you how to get it back

{{<figure src="/announcements/applications/18.12.0/app1812_spectacle01.png" width="600px" >}}

<a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's screenshot utility:

- Spectacle artık ekran görüntüsü dosyalarını sıralı olarak numaralandırma yeteneğine sahiptir ve dosya adı metin alanını temizlerseniz varsayılan olarak bu adlandırma şemasına geçer
- Fixed saving images in a format other than .png, when using Save As…
- Harici bir uygulamada bir ekran görüntüsü açmak için Spectacle kullanırken, işiniz bittikten sonra görüntüyü değiştirmek ve kaydetmek artık mümkün.
- Spectacle artık Araçlar> Ekran Görüntüleri Klasörünü Aç'ı tıkladığınızda doğru klasörü açıyor
- Ekran görüntüsü zaman damgaları artık görüntünün kaydedildiği zamanı değil, oluşturulduğu zamanı yansıtıyor
- Tüm kaydetme seçenekleri artık "Kaydet" sayfasında bulunmaktadır

<a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, KDE's archive manager:

- Zstandard formatı için destek eklendi (tar.zst arşivleri)
- Fixed Ark previewing certain files (e.g. Open Document) as archives instead of opening them in the appropriate application

### Matematiksel

<a href='https://www.kde.org/applications/utilities/kcalc/'>KCalc</a>, KDE's simple calculator, now has a setting to repeat the last calculation multiple times.

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE'nin matematiksel ön ucu:

- Add Markdown entry type
- Halihazırda hesaplanan komut girişinin animasyonlu vurgulanması
- Bekleyen komut girişlerinin görselleştirilmesi (sıraya alındı, ancak henüz hesaplanmadı)
- Komut girişlerini biçimlendirmeye izin ver (arka plan rengi, ön plan rengi, yazıtipi özellikleri)
- İmleci istenen konuma yerleştirerek ve yazmaya başlayarak çalışma sayfasındaki rastgele yerlere yeni komut girişleri eklemeye izin verin
- Birden çok komuta sahip ifadeler için sonuçları çalışma sayfasında bağımsız sonuç nesneleri olarak gösterin
- Konsoldan göreli yollara göre çalışma sayfalarını açmak için destek ekleyin
- Add support for opening multiple files in one Cantor shell
- Komut girişindeki normal girdiden daha iyi ayırt etmek için ek bilgi isterken rengini ve yazıtipini değiştir
- Çalışma sayfalarında gezinmek için kısayollar eklendi (Ctrl+PageUp, Ctrl+PageDown)
- Add action in 'View' submenu for zoom reset
- Enable downloading of Cantor projects from store.kde.org (at the moment upload works only from the website)
- Arka uç sistemde mevcut değilse çalışma sayfasını salt okunur modda açın

<a href='https://www.kde.org/applications/education/kmplot/'>KmPlot</a>, KDE's function plotter, fixed many issues:

- Fixed wrong names of the plots for derivatives and integral in the conventional notation
- Kmplot'ta SVG dışa aktarımı artık düzgün çalışıyor
- First derivative feature has no prime-notation
- Unticking unfocused function now hides its graph:
- İşlev düzenleyiciden 'Sabitleri Düzenle'yi yinelemeli olarak açarken Kmplot çökmesi çözüldü
- Fare işaretçisinin grafikte takip ettiği bir işlevi sildikten sonra kmplot çökmesi çözüldü
- Artık çizilen verileri herhangi bir resim formatı olarak dışa aktarabilirsiniz
