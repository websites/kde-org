---
aliases:
- ../../kde-frameworks-5.53.0
date: 2018-12-09
layout: framework
libCount: 70
---
### Baloo

- Zoeken naar waardering 10 (5 sterren) repareren (bug 357960)
- Schrijven van ongewijzigde gegevens naar termen-dbs vermijden
- Type::Document/Presentation/Spreadsheet niet twee keer toevoegen voor MS Office documenten
- Kcrash echt juist initialiseren
- Ga na dat er slechts één MTime per document is in de MTimeDB
- [Extractor] QDataStream serialisatie gebruiken in plaats van gemaakte
- [Extractor] Eigen IO-handler vervangen door QDataStream, vangt HUP

### Breeze pictogrammen

- Pictogrammen voor application-vnd.appimage/x-iso9660-appimage toevoegen
- 22px pictogram voor dialoogwaarschuwing toevoegen (bug 397983)
- Hoek en marge van 32px dialoog-ok-toegepast gerepareerd (bug 393608)
- Primaire monochrome pictogramkleuren wijzigen om overeen te komen met nieuwe HIG-kleuren
- Actiepictogrammen van archief-* wijzigen om archieven te representeren (bug 399253)
- Symbolische koppeling van help-browser toevoegen aan 16px en 22px mappen (bug 400852)
- Nieuwe generieke sorteerpictogrammen toevoegen; bestaande sorteerpictogrammen hernoemen
- Root-versie van drive-harddisk toevoegen (bug 399307)

### Extra CMake-modules

- Nieuwe module: FindLibExiv2.cmake

### Frameworkintegratie

- Optie BUILD_KPACKAGE_INSTALL_HANDLERS toevoegen aan bouwen installatie handlers overslaan

### KDE Doxygen hulpmiddelen

- Bezig-indicator toevoegen tijdens research en de research asynchroon maken (bug 379281)
- Alle invoerpaden normaliseren met de functie os.path.normpath (bug 392428)

### KCMUtils

- Uitlijning tussen QML en QWidget KCM-titels perfectioneren
- Context toevoegen aan verbinding van kcmodule aan lambdas (bug 397894)

### KCoreAddons

- Het mogelijk maken om KAboutData/License/Person uit QML te gebruiken
- Crash als XDG_CACHE_HOME map te klein is of geen ruimte repareren (bug 339829)

### KDE DNS-SD

- installeert nu kdnssd_version.h om de versie van de bibliotheek te controleren
- resolver niet lekken in remoteservice
- uit de hand lopen van avahi-signaal voorkomen
- reparatie voor macOS

### KFileMetaData

- Breng nieuw leven in eigenschap 'Beschrijving' voor DublinCore metagegevens
- een beschrijvende eigenschap aan KFileMetaData toevoegen
- [KFileMetaData] Extractie toevoegen voor DSC volgens (ingekapselde) Postscript
- [ExtractorCollection] afhankelijkheid van kcoreaddons vermijden voor CI testen
- Ondersteuning toevoegen voor speex-bestanden aan taglibextractor
- nog twee internetbronnen toevoegen met betrekking tot tag aan informatie toevoegen
- behandeling van id3-tags vereenvoudigen
- [XmlExtractor] QXmlStreamReader gebruiken voor betere prestaties

### KIO

- Toekennen repareren bij opschonen van symbolische koppelingen in PreviewJob
- De mogelijkheid toevoegen om een sneltoets te hebben om een bestand aan te maken
- [KUrlNavigator] Bij klik met middelste muis opnieuw activeren (bug 386453)
- niet meer bestaande leverancier van zoekmachine verwijderen
- Meer leveranciers van zoekmachines overzetten naar HTTPS
- KFilePlaceEditDialog opnieuw exporteren (bug 376619)
- Ondersteuning van sendfile herstellen
- [ioslaves/trash] Behandel gebroken symbolische koppelingen in verwijderde submappen (bug 400990)
- [RenameDialog] Indeling repareren bij gebruik van de vlag NoRename
- Ontbrekende @since voor KFilePlacesModel::TagsType toegevoegd
- [KDirOperator] Het nieuwe pictogram <code>view-sort</code> gebruiken voor de kiezer van de sorteervolgorde
- HTTPS voor alle leveranciers van zoekmachines gebruiken die dat ondersteunen
- Optie afkoppelen uitschakelen voor / of /home (bug 399659)
- [KFilePlaceEditDialog] Include bewaking repareren
- [Places panel] nieuw pictogram <code>folder-root</code> gebruiken voor root-item
- [KSambaShare] "net usershare info"-ontleder te testen maken
- Het bestand dialogs een "Sorteren op" menuknop geven op de werkbalk

### Kirigami

- DelegateRecycler: Geen nieuwe propertiesTracker aanmaken voor elke gedelegeerde
- Pagina Over van Discover verplaatsen naar Kirigami
- Contextlade verbergen wanneer er een globale werkbalk is
- ga na dat alle items er zijn (bug 400671)
- index wijzigen bij indrukken, niet bij klikken (bug 400518)
- nieuwe tekstgroottes voor koppen
- zijbalkladen verplaatsen geen globale kop-/voetteksten

### KNewStuff

- Programmatisch nuttige foutsignalering toevoegen

### KNotification

- NotifyByFlatpak naar NotifyByPortal hernoemen
- Meldingenportaal: pixmaps in meldingen ondersteunen

### KPackage-framework

- Geen appstream-gegevens voor bestanden genereren waarbij een beschrijving ontbreekt (bug 400431)
- Metagegevens van pakket vangen voordat installatie begint

### KRunner

- 1

### KTextEditor

- Negatieve prioriteiten in syntaxisdefinitie toestaan
- Functie "Commentaar omschakelen" tonen via hulpmiddelenmenu en standaard sneltoets (bug 387654)
- Verborgen talen in het menu modus
- SpellCheckBar: DictionaryComboBox gebruiken in plaats van gewone QComboBox
- KTextEditor::ViewPrivate: waarschuwing "Tekst gevraagd voor ongeldige reeks" vermijden
- Android: log2 definiëren is niet langer nodig
- Verbinding van contextmenu verbreken vanuit alle ontvangers van aboutToXXContextMenu (bug 401069)
- AbstractAnnotationItemDelegate introduceren voor meer controle door consument

### KUnitConversion

- Bijgewerkt met eenheden uit de olie-industrie (bug 388074)

### KWayland

- Bestand voor loggen automatisch genereren + categorieënbestand repareren
- VirtualDesktops toevoegen aan PlasmaWindowModel
- Test voor PlasmaWindowModel bijwerken om wijzigingen in VirtualDesktop mee te nemen
- Testen in windowInterface opschonen voordat windowManagement wordt weggegooid
- Het juiste item in removeDesktop verwijderen
- Item in lijst van Virtual Desktop Manager opschonen in PlasmaVirtualDesktop destructor
- Versie van nieuw toegevoegde PlasmaVirtualDesktop-interface corrigeren
- [server] Tip voor inhoud van tekstinvoer en doel van per protocolversie
- [server] tekstinvoer (niet/wel) activeren, in-/uitschakelen van callbacks in onderliggende klassen zetten
- [server] instellen van omgevende tekst callback met uint in v0 klasse zetten
- [server] exclusieve callbacks van v0 bij enige tekstinvoer in v0 klasse zetten

### KWidgetsAddons

- Niveau-api uit Kirigami.Heading toevoegen

### KXMLGUI

- De tekst "Over KDE" bijwerken

### NetworkManagerQt

- Een bug(fout?) in instellingen van ipv4 &amp; ipv6 gerepareerd
- Instellingen voor ovs-bridge en ovs-interface toegevoegd
- IP-tunnelinstellingen bijwerken
- Proxy en gebruikersinstelling toevoegen
- IP-tunnelinstelling toevoegen
- We kunnen nu test voor tun-instelling altijd bouwen
- Ontbrekende opties voor IPv6 toevoegen
- Luisteren naar toegevoegde DBus-interfaces in plaats van geregistreerde services (bug 400359)

### Plasma Framework

- pariteit van menu met de bureaubladstijl mogelijk gemaakt
- Qt 5.9 is nu de minimaal vereiste versie
- (Per ongeluk?) verwijderde regel in CMakeLists.txt terugbracht
- 100% consistentie met grootte van kop in kirigami
- homogener uiterlijk met Kirigami-koppen
- de verwerkte versie van private imports installeren
- Selectiebesturing van text op mobiel
- Kleurschema's breeze-light en breeze-dark bijwerken
- Een aantal geheugenlekken gerepareerd (dankzij ASAN)

### Omschrijving

- phabricator-plug-in: diff.rev. ordening van Arcanist gebruiken (bug 401565)
- Een titel leveren voor JobDialog
- De JobDialog toestaan om een nettere initiële grootte te krijgen (bug 400873)
- Het mogelijk maken om menudemo te delen met verschillende URL's
- QQC2 voor de JobDialog gebruiken (bug 400997)

### QQC2StyleBridge

- consistente grootte van item maken vergeleken met QWidgets
- afmetingen van menu instellen repareren
- ga na dat flickables pixelAligned zijn
- Ondersteuning voor op QGuiApplication gebaseerde apps (bug 396287)
- tekstbesturing voor aanraakscherm
- Grootte volgens gespecificeerde breedte en hoogte van pictogram
- Vlakke eigenschap van knoppen honoreren
- Probleem repareren wanneer er slechts één element in het menu is (bug 400517)

### Solid

- Wijziging van pictogram van root-schijf repareren zodat het niet foutief andere pictogrammen wijzigt

### Sonnet

- DictionaryComboBoxTest: uitrekken toevoegen om uitrekken van Dump-knop te vermijden

### Accentuering van syntaxis

- BrightScript: sub toestaan om geen naam te hebben
- Bestand voor accentuering toevoegen voor Wayland Debug Traces
- Syntaxisaccentuering toevoegen voor TypeScript &amp; TypeScript React
- Rust &amp; Yacc/Bison: commentaar verbeteren
- Prolog &amp; Lua: shebang repareren
- Laden van taal repareren na invoegen van trefwoorden uit deze taal in een ander bestand
- BrightScript-syntaxis toevoegen
- debchangelog: Disco Dingo toevoegen

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
