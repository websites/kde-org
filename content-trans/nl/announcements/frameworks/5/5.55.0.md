---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] toegang uitschakelen met een dubbele slash in de url, bijv. "tags://" (bug 400594)
- Maak QApplication gereed voor KCrash/KCatalog
- Alle signalen non-storage deviceAdded van Solid negeren
- De mooiere K_PLUGIN_CLASS_WITH_JSON gebruiken
- Controle op Qt 5.10 verwijderen nu we het vereisen als de minimale versie

### Breeze pictogrammen

- Cursors van KCM pictogrammen toevoegen
- Enige YaST pictogrammen en symbolische koppelingen toevoegen en hernoemen
- Het pictogram Meldingenbel verbeteren door het ontwerp van KAlarm te gebruiken (bug 400570)
- yast-upgrade toevoegen
- Weather-storm-* pictogrammen verbeteren (bug 403830)
- font-otf symbolische koppelingen toevoegen, net als de font-ttf symbolische koppelingen
- Juiste edit-delete-shred pictogrammen toevoegen (bug 109241)
- Pictogrammen voor trimmarges en trim naar selectie toevoegen (bug 401489)
- Symbolische koppelingen edit-delete-shred verwijderen in voorbereiding op het vervangen worden door echte pictogrammen
- Pictogram van Activiteiten KCM hernoemen
- Pictogram van Activiteiten KCM toevoegen
- KCM-pictogram van virtuele bureaubladen toevoegen
- Pictogrammen voor aanraakscherm en schermranden van KCM toevoegen
- Aan voorkeur van bestanden delen gerelateerde pictogramnamen repareren
- Een pictogram voor voorkeurbureaubladeffecten toevoegen
- Een Plasma-thema Pictogrammen met voorkeur toevoegen
- Contrast van voorkeur-systeemtijd verbeteren (bug 390800)
- Een nieuw preferences-desktop-theme-global pictogram toevoegen
- Pictogram voor hulpmiddelen toevoegen
- pictogram voor document-nieuw volgt ColorScheme-Text
- Een preferences-system-splash pictogram voor het Splash Screen KCM toevoegen
- Applets/22 invoegen
- Kotlin (.kt) mime-type pictogrammen toevoegen
- Het pictogram voor voorkeur-bureaublad-cryptografie verbeteren
- Slotje in voorkeur-bureaublad-gebruiker-wachtwoord invullen
- Het slotje consistent invullen in het pictogram voor versleuteld
- Een Kile-pictogram gebruiken dat gelijksoortig is aan het origineel

### Extra CMake-modules

- FindGperf: in ecm_gperf_generate SKIP_AUTOMOC instellen voor gegenereerd bestand
- -Wsuggest-override -Wlogical-op naar reguliere instellingen van compiler verplaatsen
- Generatie van python binding voor klassen met verwijderde copy constructors repareren
- Qmake module generatie voor Qt 5.12.1 repareren
- Meer https in koppelingen gebruiken
- API dox: ontbrekende items voor sommige find-modules &amp; modules toevoegen
- FindGperf: api dox verbeteren: gebruiksvoorbeeld voor mark-up
- ECMGenerateQmlTypes: api dox repareren: titel heeft nodig meer --- markup
- ECMQMLModules: api dox repareren: titel komt overeen met modulenaam, ontbrekende "Since" toevoegen
- FindInotify: api dox .rst tag repareren, ontbrekende "Since" toevoegen

### KActivities

- reparatie voor macOS

### KArchive

- Twee aanroepen KFilterDev::compressionTypeForMimeType opslaan

### KAuth

- Ondersteuning voor doorgeven gui QVariants aan KAuth-helpers verwijderen

### KBookmarks

- Zonder D-Bus bouwen op Android
- Const'ify

### KCMUtils

- [kcmutils] ellipsis toevoegen aan zoeklabels in KPluginSelector

### KCodecs

- nsSJISProber::HandleData: geen crash als aLen 0 is

### KConfig

- kconfig_compiler: de toekenningsoperator en kopieerconstructor verwijderen

### KConfigWidgets

- Zonder KAuth en D-Bus op Android gebouwd
- KLanguageName toevoegen

### KCrash

- Commentaar waarom wijzigen van de ptracer vereist is
- [KCrash] socket opzetten om wijziging van ptracer toe te staan

### KDeclarative

- [KCM Controls GridView] animatie van verwijderen toevoegen

### Ondersteuning van KDELibs 4

- Enige landenvlaggen gerepareerd om alles van de pixmap te gebruiken

### KDESU

- verkeerd wachtwoord behandelen bij gebruik van sudo die vraagt om een ander wachtwoord (bug 389049)

### KFileMetaData

- exiv2extractor: ondersteuning voor bmp, gif, webp, tga toevoegen
- mislukte test van exiv gps gegevens repareren
- Lege en nul gps-gegevens testen
- ondersteuning toevoegen voor meer mimetypes aan taglibwriter

### KHolidays

- holidays/plan2/holiday_ua_uk - bijgewerkt voor 2019

### KHTML

- JSON metagegevens toevoegen aan khtmlpart plug-in binair programma

### KIconThemes

- Zonder D-Bus bouwen op Android

### KImageFormats

- xcf: reparatie voor repareren van dekking die uit zijn grenzen is
- De includes van qdebug uitcommentariëren
- tga: Use-of-uninitialized-value op gebroken bestanden repareren
- maximale dekking is 255
- xcf: toekenning in bestanden met twee PROP_COLORMAP repareren
- ras: toekennen omdat ColorMapLength te groot is repareren
- pcx: crash op verstoord bestand repareren
- xcf: robuustheid implementeren voor wanneer PROP_APPLY_MASK niet in het bestand staat
- xcf: loadHierarchy: het layer.type gehoorzamen en niet de bpp
- tga: niet meer dan 8 alfabits ondersteunen
- ras: false teruggeven als toekennen van de afbeelding is mislukt
- rgb: overflow op geheel getal in vervaagd bestand repareren
- rgb: Heap-buffer-overflow in verstoord bestand repareren
- psd: crash op vervaagd bestand repareren
- xcf: x/y_offset initialiseren
- rgb: crash in vervaagde afbeelding repareren
- pcx: crash op vervangde afbeelding repareren
- rgb: crash in vervaagd bestand repareren
- xcf: modus laag initialiseren
- xcf: dekking van laag initialiseren
- xcf: buffer op 0 instellen als er minder gegevens zijn gelezen dan verwacht
- bzero -&gt; memset
- Verschillend lezen en schrijven in kimg_tga and kimg_xcf in/uit OOB repareren
- pic: grootte van header-id terugwijzigen als er geen 4 bytes zijn gelezen zoals verwacht
- xcf: bzero buffer als minder gegevens zijn gelezen dan verwacht
- xcf: setDotsPerMeterX/Y alleen aanroepen als PROP_RESOLUTION is gevonden
- xcf: num_colors initialiseren
- xcf: eigenschap zichtbaarheid van laag initialiseren
- xcf: int niet omvormen tot enum als die enum die waarde van int niet kan bevatten
- xcf: geen overflow van int bij aanroep van setDotsPerMeterX/Y

### KInit

- KLauncher: processen behandelen die zonder fout eindigen (bug 389678)

### KIO

- Toetsenbord besturing van het wigdet voor controlesommen verbeteren
- Helperfunctie toevoegen aan omleidingen uitschakelen (nuttig voor kde-open)
- "Refactor SlaveInterface::calcSpeed" terug wijzigen (bug 402665)
- CMake policy CMP0028 niet instellen naar oud. We hebben geen doelen met :: tenzij ze zijn geïmporteerd.
- [kio] ellipsis toevoegen aan zoeklabel in sectie Cookies
- [KNewFileMenu] fileCreated niet uitzenden bij aanmaken van een map (bug 403100)
- Gebruik (en suggereer gebruik) van het mooiere K_PLUGIN_CLASS_WITH_JSON
- blokkering vermijden van kio_http_cache_cleaner en zorg voor beëindigen met sessie (bug 367575)
- Mislukken van test van knewfilemenu en onderliggende reden voor mislukken repareren

### Kirigami

- alleen gekleurde knoppen omhoog brengen (bug 403807)
- hoogte repareren
- hetzelfde beleid voor grootte van marges als voor de andere items in de lijst
- === operators
- ondersteuning voor het concept van uit te breiden items
- niet wissen bij vervangen van alle pagina's
- horizontale opvulling is het dubbele van verticaal
- opvulling meenemen om grootte van tips te berekenen
- [kirigami] geen lichte stijl voor lettertypen voor headings (2/3) (bug 402730)
- Repareer de indeling van de AboutPage op kleinere apparaten
- stopatBounds voor breadcrumb flickable

### KItemViews

- [kitemviews] Het zoeken in Bureaubladgedrag/Activiteiten wijzigen naar meer in overeenstemming met andere zoeklabels

### KJS

- SKIP_AUTOMOC instellen voor enige gegenereerde bestanden, om met CMP0071 rekening te houden

### KNewStuff

- Semantiek voor ghns_exclude repareren (bug 402888)

### KNotification

- geheugenlek repareren bij doorgeven van gegevens va pictogram aan Java
- Afhankelijkheid van ondersteuningsbibliotheek AndroidX verwijderen
- Android ondersteuning voor meldingenkanaal toevoegen
- Meldingen laten werken op Android met API-niveau &lt; 23
- De controle op Android API-niveau verplaatsen naar runtime
- Ongebruikte voorwaarts-declaratie verwijderen
- AAR opnieuw bouwen wanneer Java broncode wijzigt
- De Java kant bouwen met Gradle, als AAR in plaats van JAR
- Vertrouw niet op de Plasma werkruimteintegratie op Android
- Naar configuratie van meldinggebeurtenis zoeken in qrc-hulpbronnen

### KPackage-framework

- Laat vertalingen werken

### KPty

- Niet overeenkomende struct/class repareren

### KRunner

- Expliciet gebruik van ECM_KDE_MODULE_DIR verwijderen, is onderdeel van ECM_MODULE_PATH

### KService

- Zonder D-Bus bouwen op Android
- Aan mensen suggereren om K_PLUGIN_CLASS_WITH_JSON te gebruiken

### KTextEditor

- Qt 5.12.0 heeft problemen in de implementatie van reguliere expressies in QJSEngine, inspringen kan zich niet juist gedragen, Qt 5.12.1 zal een reparatie bevatten
- KateSpellCheckDialog: actie "Selectie van spellingcontrole" verwijderen
- JavaScript bibliotheek underscore.js bijwerken naar versie 1.9.1
- Bug 403422 repareren: wijziging van de markeringsgrootte opnieuw toestaan (bug 403422)
- SearchBar: Knop Annuleren toevoegen om lang actieve taken te stoppen (bug 244424)
- Expliciet gebruik van ECM_KDE_MODULE_DIR verwijderen, is onderdeel van ECM_MODULE_PATH
- KateGotoBar nakijken
- ViewInternal: 'Ga naar overeenkomend haakje' in modus overschrijven repareren (bug 402594)
- HTTPS gebruiken, indien beschikbaar, in koppelingen zichtbaar voor gebruikers
- KateStatusBar nakijken
- ViewConfig: optie toevoegen om op de cursorpositie met de muis te plakken (bug 363492)
- De mooiere K_PLUGIN_CLASS_WITH_JSON gebruiken

### KWayland

- [server] juiste aanraak-id's genereren
- XdgTest-spec compliant maken
- Optie toevoegen om wl_display_add_socket_auto te gebruiken
- [server] Initiële org_kde_plasma_virtual_desktop_management.rows verzenden
- Informatie over rijen aan het Plasma virtuele bureaubladprotocol toevoegen
- [client] Wrap wl_shell_surface_set_{class,title}
- Verwijderen van hulpbron bewaken in OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Geen lichte lettertypenstijlen voor koppen gebruiken (3/3) (bug 402730)

### KXMLGUI

- Zonder D-Bus bouwen op Android
- KCheckAccelerators minder invloed geven voor toepassingen die niet direct koppelen met KXmlGui

### ModemManagerQt

- Implementatie van QVariantMapList-operator &gt;&gt; repareren

### Plasma Framework

- [Wallpaper templates] Ontbrekend Comment= item toevoegen aan desktop-bestand
- Plasma::Theme exemplaren delen tussen meerdere ColorScope
- De schaduwen van klok-svg's logischer correct maken en visueel passend (bug 396612)
- [frameworks] Geen lichte lettertypenstijlen voor koppen gebruiken (1/3) (bug 402730)
- [Dialog] Zichtbaarheid van mainItem niet veranderen
- parentItem resetten wanneer mainItem wijzigt

### Omschrijving

- De mooiere K_PLUGIN_CLASS_WITH_JSON gebruiken

### QQC2StyleBridge

- Initiële grootte van combinatieveld repareren (bug 403736)
- Keuzelijstpopups instellen op modaal (bug 403403)
- 4f00b0cabc1230fdf gedeeltelijk terugdraaien
- Regelafbreking voor lange tekstballonnen (bug 396385)
- ComboBox: standaard gedelegeerde repareren
- Zweven met muis valselijk laten zien terwijl keuzelijst open is
- CombooBox QStyleOptionState == On instellen inplaats van verzonken om met qwidgets overeen te laten komen (bug 403153)
- ComboBox repareren
- De tekstballon altijd bovenop al het andere laten zien
- Een tekstballon op een MouseArea ondersteunen
- geen tekst tonen afdwingen voor ToolButton

### Solid

- Zonder D-Bus bouwen op Android

### Sonnet

- Deze code niet aanroepen als we alleen ruimte hebben

### Accentuering van syntaxis

- Einde van invouwgebied repareren in regels met lookAhead=true
- AsciiDoc: accentueren van directive include repareren
- Ondersteuning voor AsciiDoc toevoegen
- Bug die oneindige lus veroorzaakt bij accentuering van Kconfig-bestanden repareren
- op eindeloze omschakelingen van context controleren
- Ruby: RegExp repareren na ": " en repareer/verbeter detectie van HEREDOC (bug 358273)
- Haskell: = een speciaal symbool maken

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
