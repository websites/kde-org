---
aliases:
- ../../kde-frameworks-5.54.0
date: 2019-01-12
layout: framework
libCount: 70
---
### Attica

- Notificar si ha fallado la descarga de un proveedor predeterminado.

### Baloo

- Mover el auxiliar typesForMimeType de BasicIndexingJob a un espacio de nombres anónimo.
- Añadir «image/svg» como Type::Image a BasicIndexingJob.
- Usar el formato JSON compacto para guardar metadatos de documentos.

### Iconos Brisa

- Cambiar «preferences-system-network» a un enlace simbólico.
- Usar un icono para Kolf con un bonito fondo y con sombras.
- Copiar algunos iconos modificados de Brisa a Brisa oscuro.
- Añadir YaST y nuevos iconos para las preferencias.
- Añadir un icono correcto para «python-bytecode», usar un color consistente en los iconos para Python (error 381051).
- Borrar los enlaces simbólicos para bytecode de Python en preparación para sus futuros iconos propios.
- Hacer que el icono para el tipo MIME de Python sea la base, y que el icono para bytecode de Python sea un enlace a él.
- Añadir iconos de dispositivo para los puertos RJ11 y RJ45.
- Añadir el separador de directorio que faltaba (error 401836).
- Usar el icono correcto para los scripts de Python 3 (error 402367).
- Cambiar los iconos de color para la red/web a un estilo consistente.
- Añadir un nuevo nombre para los archivos de sqlite, de modo que los iconos se muestren realmente (error 402095).
- Añadir los iconos «drive-*» para el particionador de YaST.
- Añadir el icono «view-private» (error 401646).
- Añadir los iconos de acción «flashlight».
- Mejorar el simbolismo para los iconos de estado «desactivado» y «silenciado» (error 398975).

### Módulos CMake adicionales

- Añadir módulo de búsqueda para «libphonenumber» de Google.

### KActivities

- Corregir la versión del archivo «pkgconfig».

### Herramientas KDE Doxygen

- Corregir la visualización de markdown de doxygen.

### KConfig

- Codificar los bytes que son mayores o iguales a 127 en los archivos de configuración.

### KCoreAddons

- Macros de cmake: Adaptar las variables ECM desaconsejadas de kcoreaddons_add_plugin (error 401888).
- Hacer que se puedan traducir las unidades y los prefijos formatValue.

### KDeclarative

- No mostrar separadores en móvil.
- root.contentItem en lugar de solo contentItem.
- Añadir la API que faltaba para KCM multinivel para controlar las columnas.

### KFileMetaData

- Hacer que falle la prueba de escritura si el tipo MIME no está permitido por el extractor.
- Corregir la extracción del número de disco en APE.
- Implementar la extracción de portadas para los archivos ASF.
- Extender la lista de tipos MIME permitidos para el extractor de imágenes empotradas.
- Refactorizar el extractor de imágenes empotradas para una mayor extensibilidad.
- Añadir el tipo MIME WAV que faltaba.
- Extraer más etiquetas de los metadatos exif (error 341162).
- Corregir la extracción de la altura GPS de los datos exif.

### KGlobalAccel

- Corregir la construcción de KGlobalAccel con el prelanzamiento de Qt 5.13.

### KHolidays

- README.md: añadir instrucciones básicas para probar los archivos de días festivos.
- Diversos calendarios: corregir errores de sintaxis.

### KIconThemes

- ksvg2icns: usar la API QTemporaryDir de Qt 5.9 o posterior.

### KIdleTime

- [xscreensaverpoller] Descargar datos tras reiniciar el salvapantallas.

### KInit

- Usar un rlimit ligero para el número de manejadores abiertos. Esto corrige el inicio muy lento de plasma con el último systemd.

### KIO

- Revertir «Ocultar la vista previa del archivo cuando el icono es demasiado pequeño».
- Mostrar un error en lugar de fallar de forma silenciosa cuando se solicita crear una carpeta que ya existe (error 400423).
- Cambiar la ruta para cada elemento de los subdirectorios cuando se cambia el nombre de un directorio (error 401552).
- Extender el filtro de expresión regular getExtensionFromPatternList.
- [KRun] Cuando se solicita abrir un enlace en un navegador externo, recurrir a la lista de las aplicaciones MIME si no se ha definido nada en kdeglobals (error 100016).
- Hacer que la función de abrir URL en pestaña sea algo más visible (error 402073).
- [kfilewidget] Devolver el URL editable del navegador al modo rastro de migas si tiene el focus y todo está seleccionado cuando se pulsa Ctrl+L.
- [KFileItem] Corregir la comprobación isLocal en checkDesktopFile (error 401947).
- SlaveInterface: Detener speed_timer tras matar un trabajo.
- Advertir al usuario antes de un trabajo de copiar o mover si el tamaño del archivo excede el máximo posible en el sistema de archivos FAT32 (4 GB) (error 198772).
- Evitar el incremento constante de la cola de eventos de Qt en los esclavos KIO.
- Permitir el uso de TLS 1.3 (parte de Qt 5.12).
- [KUrlNavigator] Corregir firstChildUrl cuando se vuelve de un archivo comprimido.

### Kirigami

- Asegurar que no se redefine QIcon::themeName cuando no se debe.
- Introducir un objeto DelegateRecycler adjunto.
- Corregir los márgenes de la vista en rejilla teniendo en cuenta las barras de desplazamiento.
- Hacer que AbstractCard.background reaccione a AbstractCard.highlighted.
- Simplificar el código de MnemonicAttached.
- SwipeListItem: Mostrar siempre los iconos si !supportsMouseEvents.
- Considerar si se debe usar needToUpdateWidth según widthFromItem, y no según la altura.
- Tener en cuenta la barra de desplazamiento para el margen de ScrollablePage (error 401972).
- No tratar de reposicionar la ScrollView cuando tenemos una altura errónea (error 401960).

### KNewStuff

- Cambiar el orden por omisión en el diálogo de descargas a «Más descargados» (error 399163).
- Mostrar una notificación cuando el proveedor no se carga.

### KNotification

- [Android] Fallar con más elegancia cuando se compila con una API &lt; 23.
- Añadir el motor de notificaciones de Android.
- Compilar sin Phonon ni D-Bus en Android.

### KService

- applications.menu: Eliminar la categoría no usada X-KDE-Edu-Teaching.
- applications.menu: Eliminar &lt;KDELegacyDirs/&gt;.

### KTextEditor

- Corregir el uso de scripts para Qt 5.12.
- Corregir el script «emmet» usando números hexadecimales en lugar de octales en las cadenas (error 386151).
- Corregir el script «Emmet» (error 386151).
- ViewConfig: Añadir la opción «Ajuste dinámico en el marcador estático».
- Corregir el final de la región de plegado; añadir el elemento final al intervalo.
- Evitar el feo sobrepintado con alfa.
- No volver a marcar las palabras añadidas o ignoradas al diccionario como mal escritas (error 387729).
- KTextEditor: Añadir la acción para el ajuste de palabras estático (error 141946).
- No ocultar la acción «Borrar intervalos del diccionario».
- No solicitar confirmación cuando se vuelve a cargar (error 401376).
- Clase Message: Usar la incialización miembro «inclass».
- Exponer KTextEditor::ViewPrivate:setInputMode(InputMode) a KTextEditor::View.
- Mejorar el rendimiento de las acciones de edición pequeñas, como corregir las acciones grandes de sustituir todo (error 333517).
- Llamar solo updateView() en visibleRange() cuando endPos() no es válido.

### KWayland

- Añadir una aclaración sobre el uso conjunto de las decoraciones del servidor de KDE y las decoraciones Xdg.
- Permitir el uso de las decoraciones de Xdg.
- Corregir la instalación de cabeceras cliente XDGForeign.
- [servidor] Permitir el uso de tocar y arrastrar.
- [servidor] Permitir múltiples interfaces táctiles por cliente.

### KWidgetsAddons

- [KMessageBox] Corregir el tamaño mínimo del diálogo cuando se solicitan detalles (error 401466).

### NetworkManagerQt

- Se han añadido preferencias para DCB, macsrc, match, tc, ovs-patch y ovs-port.

### Framework de Plasma

- [Calendario] Exponer firstDayOfWeek en la vista mensual (error 390330).
- Añadir «preferences-system-bluetooth-battery» a preferences.svgz.

### Purpose

- Añadir el tipo de complemento para compartir URL.

### QQC2StyleBridge

- Corregir la anchura de los elementos de menú cuando se redefine el delegado (error 401792).
- Rotar el indicador de ocupado en el sentido horario.
- Forzar que las casillas de verificación y los botones de selección sean cuadrados.

### Solid

- [UDisks2] Usar MediaRemovable para determinar si se puede expulsar un medio.
- Permitir el uso de baterías Bluetooth.

### Sonnet

- Añadir un método a BackgroundChecker para añadir una palabra a la sesión.
- DictionaryComboBox: Mantener en la parte superior los diccionarios favoritos del usuario (error 302689).

### Resaltado de sintaxis

- Actualizar el uso de la sintaxis PHP.
- WML: Corregir el código Lua integrado y usar los nuevos estilos por omisión.
- Resaltado de sintaxis para los archivos .cu y .cuh de CUDA como C++.
- Reaccionar a TypeScript y TS/JS: Mejorar la detección de tipos, corregir números en coma flotante y otras mejoras y correcciones.
- Haskell: Resaltado de comentarios vacíos tras «import».
- WML: Corregir un bucle infinito en contextos de «switch» y resaltar solo las etiquetas con nombres válidos (error 402720).
- BrightScript: Añadir un remedio para el resaltado de «endsub» de QtCreator, añadir el plegado de funciones y subrutinas.
- Permitir el uso de más variantes de literales numéricas de C (error 402002).

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
