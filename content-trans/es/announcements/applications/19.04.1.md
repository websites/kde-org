---
aliases:
- ../announce-applications-19.04.1
changelog: true
date: 2019-05-09
description: KDE lanza las Aplicaciones 19.04.1.
layout: application
major_version: '19.04'
release: applications-19.04.1
title: KDE lanza las Aplicaciones de KDE 19.04.1
version: 19.04.1
---
{{% i18n_date %}}

KDE ha lanzado hoy la primera actualización de estabilización para las <a href='../19.04.0'>Aplicaciones 19.04</a>. Esta versión solo contiene correcciones de errores y actualizaciones de traducciones, por lo que será una actualización agradable y segura para todo el mundo.

Se han registrado más de veinte correcciones de errores, que incluyen mejoras en Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle y Umbrello, entre otras aplicaciones.

Las mejoras incluyen:

- El etiquetado de archivos en el escritorio ya no trunca el nombre de la etiqueta.
- Se ha corregido un bloqueo en el complemento de compartir texto de KMail.
- Se han corregido varias regresiones en el editor de vídeo Kdenlive.
