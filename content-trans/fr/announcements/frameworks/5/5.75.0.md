---
aliases:
- ../../kde-frameworks-5.75.0
date: 2020-10-10
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

+ [AdvancedQueryParser] Relax parsing of string ending with parentheses
+ [AdvancedQueryParser] Relax parsing of string ending with comparator
+ [AdvancedQueryParser] Fix out-of-bound access if last character is a comparator
+ [Term] Replace Term::Private constructor with default values
+ [BasicIndexingJob] Shortcut XAttr retrieval for files without attributes
+ [extractor] Fix document type in extraction result
+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure

### BluezQt

+ Ajout d'une propriété « rfkill » au gestionnaire
+ Ajout d'une propriété d'état au « rfkill »
+ Déclarer « Rfkill » pour « QML »
+ Exporter « Rfkill »
+ Support providing service data values for LE advertisements

### Icônes « Breeze »

+ Ajouter une nouvelles icône générique de « comportement »
+ Make icon validation depend on icon generation only if enabled
+ Replace 24px icon bash script with python script
+ Use flag style iconography for view-calendar-holiday
+ Ajout du logo « Nano » de Plasma
+ Ajout de « application-x-kmymoney »
+ Ajout d'une icône pour KMyMoney

### Modules additionnels « CMake »

+ Correction des recherches de traduction pour les « URL » de « invent »
+ Include FeatureSummary and find modules
+ Introduce plausibility check for outbound license
+ Ajout de « CheckAtomic.cmake »
+ Correction de la configuration avec « pthread » sous Android « 32 bits »
+ Ajout d'un paramètre « RENAME » pour « ecm_generate_dbus_service_file »
+ Fix find_library on Android with NDK &lt; 22
+ Tri explicite des listes de version d'Android
+ Store Android {min,target,compile}Sdk in variables

### Outils KDE avec « DOxygen »

+ Améliorations de licences
+ Correction de « api.kde.org » pour plate-forme mobile
+ Passage de « api.kde.org » en « PWA »

### KArchive

+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure

### KAuth

+ Utilisation de la nouvelle variable d'installation (bogue 415938)
+ Mark David Edmundson as maintainer for KAuth

### KCalendarCore

+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure

### KCMUtils

+ Remove handling for inside events from tab hack (bug 423080)

### KCompletion

+ Ajustement des fichiers vers la licence « GPL » 2.0 ou supérieure

### KConfig

+ CMake: Also set SKIP_AUTOUIC on generated files
+ Use reverse order in KDesktopFile::locateLocal to iterate over generic config paths

### KConfigWidgets

+ Fix isDefault that cause the KCModule to not properly update its default state
+ [kcolorscheme]: Add isColorSetSupported to check if a colour scheme has a given color set
+ [kcolorscheme] Properly read custom Inactive colors for the backgrounds

### KContacts

+ Remove obsolete license file for LGPL-2.0-only
+ Ajustement des fichiers vers la licence « GPL » 2.0 ou supérieure

### KCoreAddons

+ KJob: emit result() and finished() at most once
+ Add protected KJob::isFinished() getter
+ Déconseillé « KRandomSequence » en faveur de « QRandomGenerator »
+ Initialisation de la variable dans la classe d'en-tête + transformation en constante de variable / pointeur
+ Renforcement des tests reposant sur des messages à l'encontre de l'environnement (bogue 387006)
+ Simplification des tests surveillant « qrc » (bogue 387006)
+ Comptage des références et suppressions des instances « KDirWatchPrivate » (bogue 423928)
+ Déconseillé « KBackup::backupFile() » à cause de fonctionnalité supprimée
+ Déconseiller « KBackup::rcsBackupFile(...)  » à cause de l'absence d'utilisateurs connus.

### KDBusAddons

+ Ajustement des fichiers vers la licence « GPL » 2.0 ou supérieure

### KDeclarative

+ QML pour I18n sont ajoutés dans KF 5.17
+ Ajustement des fichiers vers la licence « GPL » 2.0 ou supérieure
+ Blocage des raccourcis lors des enregistrements des séquences de touches (bogue 425979)
+ Ajout de « SettingHighlighter » comme une version manuelle de la coloration syntaxique, réalisée par « SettingStateBinding ».

### Prise en charge de « KDELibs 4 »

+ KStandardDirs : toujours résoudre les liens symboliques dans les fichiers de configuration.

### KHolidays

+ Description dé-commentée des champs pour les fichiers de jours fériés « mt_* ».
+ Ajout des jours fériés nationaux pour Malte, à la fois en Anglais (« en-gb » et en maltais (« mt »).

### KI18n

+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure

### KIO

+ KUrlNavigator : toujours utiliser « desktop : / » et non « desktop : »
+ Prise en charge de la syntaxe avancée de « DuckDuckGo » dans « Webshortcuts » (bogue 374637)
+ KNewFileMenu : « KIO::mostLocalUrl » n'est utile qu'avec des protocoles « :local ».
+ Déconseiller « KIO::pixmapForUrl »
+ kio_trash : suppression de vérification stricte de permission non nécessaire
+ OpenUrlJob : gestion cohérente de tous les scripts de texte (bogue 425177)
+ KProcessRunner : plus de métadonnées pour « systemd »
+ KDirOperator : suppression de l'appel à « setCurrentItem » avec une « URL » vide (bogue 425163)
+ [KNewFileMenu] : correction de la création d'un nouveau dossier avec un nom commençant par « : » (bogue 425396)
+ StatJob : rendre plus évident que « mostLocalUrl » ne fonctionne qu'avec des protocoles « :local ».
+ Documentation sur comment ajouter de nouveaux rôles « aléatoires » dans « kfileplacesmodel.h ».
+ Suppression de vieux patch « kio_fonts » dans « KCoreDirLister ». Le nom de l'hôte n'a pas été analysé correctement. 
+ KUrlCompletion : prise en compte des protocoles « : local » utilisant le nom d'hôte dans l'« URL ».
+ Découpage du code de la méthode « addServiceActionsTo » en de plus petites méthodes.
+ Bogue [kio] : possibilité d'utilisation des doubles quotes dans la boîte de dialogue « Ouvrir » et « Enregistrer » (bogue 185433).
+ StatJob : annulation d'une tâche si son « URL » est non valable (bogue 426367).
+ Connexion explicite des connecteurs plutôt que l'utilisation d'une connexion automatique.
+ Vérification du bon fonctionnement de l'API « filesharingpage »

### Kirigami

+ AbstractApplicationHeader : utilisation de « anchors.fill » plutôt qu'un ancrage dépendant de la position.
+ Améliorations de l'apparence et la cohérence pour « GlobalDrawerActionItem ».
+ Suppression de l'indentation de formes pour les formats étroits.
+ Retour à « Permettre la personnalisation des couleurs d'en-tête ».
+ Capacité de personnaliser les couleurs d'en-tête
+ Ajout d'une propriété « @since » manquante pour les propriétés de la zone dessinée.
+ Introduction de propriétés de styles « QtQuick » de style d'images « paintedWidth » / « paintedHeight ».
+ Ajout d'une propriété d'espace réservé pour une image à une icône (dans le style d'un solution de repli).
+ Suppression du comportement personnalisé des icônes avec « implicitWidth » / « implicitHeight ».
+ Se prémunir d'un pointeur avec valeur « NULL » (devenant de plus en plus banal)
+ « setStatus » protégé
+ Introduction d'une propriété d'état
+ Prise en charge des fournisseurs d'images pour des « ImageResponse » et des types de texture dans « Kirigami::Icon »
+ Affichage d'une alarme à tout le monde pour ne pas utiliser « ScrollView » dans « ScrollablePage».
+ Retour à « Toujours afficher le séparateur »
+ make mobilemode support custom title delegates
+ Masquage de la ligne grisé de séparateur si le format des boutons est visible mais de largeur 0 (bogue 426738)
+ [icon] Condition prise de la non validité de l'icône quand la source est une « URL » vide.
+ Modification de « Units.fontMetrics » pour utiliser maintenant « FontMetrics »
+ Ajout de la propriété « Kirigami.FormData.labelAlignment ».
+ Toujours afficher un séparateur
+ Utilisation des couleurs d'en-têtes pour le style de bureau « AbstractApplicationHeader ».
+ Utilisation du contexte du composant lorsque sa création est déléguée à « ToolBarLayout »
+ Annulation et suppression des incubateurs lors de la suppression de « ToolBarLayoutDelegate »
+ Suppression des actions et report vers « ToolBarLayout », quand elles sont détruites (bogue 425670)
+ Remplacement de l'utilisation de la conversion de pointeur dans le style du langage « C » dans « sizegroup »
+ Les constantes binaires sont une extension « C++14 ».
+ sizegroup : correction des alarmes non gérées concernant les énumérations.
+ sizegroup : correction des connecteurs à 3 arguments.
+ sizegroup : ajout de « CONSTANT » au signal
+ Correction de quelques cas d'utilisation de boucles sur intervalle dans les conteneurs « Qt » non constants.
+ Limitation de la hauteur du bouton dans la barre générale d'outils.
+ Affichage d'un séparateur entre les zones grisées et les icônes sur la gauche.
+ Utilisation de « KDE_INSTALL_TARGETS_DEFAULT_ARGS »
+ Dimensionnement de « ApplicationHeaders » en utilisant le paramètre « SizeGroup »
+ Introduction de « SizeGroup »
+ Correction : faire apparaître l'indicateur de rechargement au dessus des en-têtes de listes.
+ Mise en place de feuillets superposés au dessus des tiroirs

### KItemModels

+ Non prise en compte de « sourceDataChanged » pour des index non valables.
+ Prise en charge des nœuds « s'effondrant » pour « KDescendantsProxyModel »

### KNewStuff

+ Suivi manuel du cycle de vie pour les utilisateurs internes de « kpackage ».
+ Mise à jour des versions pour les mises à jour « factices » de « kpackage » (bogue 427201)
+ Ne pas utiliser le paramètre par défaut si non nécessaire.
+ Correction d'un plantage lors de l'installation de « kpackage » (bogue 426732)
+ Détection de la modification du cache et action correspondante.
+ Correction de la mise à jour de l'entrée si le numéro de version est vide (bogue 417510)
+ Définition du pointeur en constante + initialisation de la variable dans l'en-tête.
+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
+ Accord pour une suggestion de passage en maintenance.

### KNotification

+ Priorité moindre pour le module externe « Android », jusqu'à la disponibilité de « Gradle »
+ Utilisation des versions « Android » de l'environnement de développement logiciels (« SDK »), à partir de « ECM ».
+ Abandon du constructeur « KNotification » prenant un paramètre de composant graphique

### Environnement de développement « KPackage »

+ Correction des notifications « D-Bus » lors des installations / mises à jour
+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure

### KParts

+ Installation des fichiers de définition pour les types de services pour « krop » &amp; « krwp » grâce à un type de correspondance par nom de fichier.

### KQuickCharts

+ Absence de boucle de connexion à l'intérieur de « Legend »
+ Suppression de la vérification de « GLES3 » dans « SDFShader » (bogue 426458)

### KRunner

+ Ajout d'une propriété « matchRegex » pour se prémunir des apparitions de processus non nécessaires
+ Autorisation pour la définition d'actions dans « QueryMatch »
+ Autorisation pour la spécification d'actions individuelles pour les correspondances des logiciels interfaces « D-Bus »
+ Ajustement des fichiers vers la licence « GPL » 2.0 ou supérieure
+ Ajout d'une propriété de nombre minimum de lettres.
+ Prise en compte de la variable d'environnement « XDG_DATA_HOME » pour les dossiers standards d'installation.
+ Amélioration des messages d'erreurs pour les programmes « D-Bus »
+ Démarrage de l'émission d'alarmes de portage des métadonnées durant l'exécution.

### KService

+ Retour à « disableAutoRebuild » à partir du seuil (bogue 423931)

### KTextEditor

+ [kateprinter] Portage des méthodes déconseillées dans « QPrinter »
+ Aucune création d'un tampon temporaire pour détecter le type « MIME » d'un fichier localement enregistré.
+ Évitement de gels lors du chargement de dictionnaires ou de trigrammes à la première saisie.
+ [Vimode] Affichage systématique des buffers « a-z » en minuscule
+ [KateFadeEffect] Émission d'un « hideAnimationFinished() » quand un éclaircissement est interrompu par un assombrissent.
+ Vérification de mise en place d'une bordure parfaite au niveau des pixels, même dans le cas d'un rendu redimensionné.
+ Vérification de la bon remplissage du séparateur de bordure dans toutes les configurations comme la mise en valeur du retour à la ligne automatique.
+ Déplacement du séparateur de la position entre la bordure de l'icône et les numéros de lignes vers la position entre la barre et le texte.
+ [Vimode] Correction du comportement avec des fichiers numérotés
+ [Vimode] Enregistrement du texte supprimé dans le fichier adéquat.
+ Retour au comportement précédent pour la recherche d'une sélection quand aucune sélection n'a été réalisée.
+ Plus de fichiers de « LGPL » en version 2.1 ou 3.0.
+ Ré-édition des fichiers de licences vers « LGPL » en version 2.0 ou supérieure.
+ Utilisation non nécessaire de la méthode de définition pour certains thèmes de couleurs.
+ La version 5.75 sera, pour une fois, incompatible avec le paramètre par défaut « Sélection automatique de thème de couleurs » pour les thèmes.
+ Changer « Schéma » vers « Thème » dans le code pour éviter toute confusion.
+ Réduction de l'en-tête de la licence proposée à son état actuel
+ Vérification que cela se termine toujours par un thème valable
+ Amélioration de la boîte de dialogue « Copier... »
+ Correction de plus de renommages de « Nouveau » vers « Copie  ».
+ Ajout de quelques « KMessageWidget » rappelant les thèmes en lecture seule lors de leurs copies. Renommage de « Nouveau » vers « Copie  ».
+ Désactivation des modifications pour les thèmes en lecture seule
+ L'enregistrement des surcharges spécifiques pour la coloration syntaxique fonctionne. Mais seules, les différences sont enregistrées.
+ Simplification de la création d'attributs. Les couleurs transparentes sont maintenant correctement gérées dans « Format ».
+ Tentative de limiter l'exportation aux attributs modifiés, cela marche à moitié. Mais, il y a toujours des noms erronés dans les définitions intégrées.
+ Démarrage des travaux pour calculer les « véritables » paramètres par défaut, concernant le thème et les formats courants, sans surcharge de styles pour la coloration syntaxique.
+ Correction de l'action de ré-initialisation
+ Enregistrement des surcharges spécifiques pour la coloration syntaxique, à ce jour, ne consistant qu'en l'enregistrement de tous les éléments à charger pour l'affichage en arborescence.
+ Démarrage des travaux sur les surcharges spécifiques pour la coloration syntaxique, à ce jour, ne consistant qu'en l'affichage des styles d'une coloration tels qu'ils sont.
+ Autorisation pour que les modifications de style par défaut soient enregistrées.
+ Autorisation pour que les modifications de couleurs soient enregistrées.
+ Implémentation de l'exportation de thème : une simple copie de fichier.
+ Ne pas mettre en valeur les importations / exportations spécifiques. Cela n'a plus aucun sens avec le nouveau format « .theme ».
+ Implémentation de l'importation de fichiers « .theme »
+ Thème « New » &amp; suppression du travail. « New » copiera le thème courant comme point de départ.
+ Utilisation des couleurs de thèmes partout
+ Enlever plus de code des anciens schémas au profit de « KSyntaxHighlighting::Theme ».
+ Commencez à utiliser les couleurs comme définies dans le thème, sans aucune autre logique autour de ceci.
+ Initialisation de « m_pasteSelection » et incrémentation de la version du fichier d'interface utilisateur.
+ Ajout d'un raccourci pour coller la sélection fait avec la souris
+ Éviter « setTheme ». Ne passer que le thème aux fonctions d'assistance.
+ Correction des infobulles. Ceci ne fera juste que ré-initialiser au thème par défaut.
+ Exportation de la configuration par défaut des styles dans un thème « json »
+ Démarrage du travail sur l'exportation « json » de thèmes, activé avec l'utilisation de l'extension « .theme » dans la boîte de dialogue d'exportation.
+ Renommage de « Utiliser le thème de couleurs de KDE » par « Utiliser les couleurs par défaut ». Cela est l'effet actuel.
+ Ne pas livrer de thème KDE vide par défaut.
+ Prise en charge de la sélection automatique du thème correct à partir du thème actuel de couleurs de Qt / KDE.
+ Conversion des noms des anciens thèmes vers de nouveaux noms. Utilisation d'un nouveau fichier de configuration et transfert des données la première fois.
+ Déplacement de la configuration de police de caractères vers l'apparence. Renommage de schéma en thème de couleurs.
+ Suppression du nom de thème par défaut codé en dur. Utilisation des mécanismes d'accès « KSyntaxHighlighting ».
+ Chargement de couleurs de secours à partir d'un thème
+ Ne pas grouper les couleurs intégrées du tout
+ Utiliser la bonne fonction pour rechercher un thème
+ Les couleurs de l'éditeur sont maintenant utilisées à partir de « Thème »
+ Utilisation de « KSyntaxHighlighting::Theme::EditorColorRole enum »
+ Gestion de « Normal » =&gt; « Transition par défaut »
+ Première étape : chargement de la liste des thèmes à partir de « KSyntaxHighlighting ». Ils sont maintenant déjà connus comme thèmes dans KTextEditor.

### KUnitConversion

+ Utilisation des majuscules pour la consommation de carburant.

### KWayland

+ Ne pas mettre en cache l'itérateur « QList::end() » si « erase() » est utilisé.

### KWidgetsAddons

+ kviewstateserializer.cpp - protection contre les plantages dans « restoreScrollBarState() »

### KWindowSystem

+ Modification des fichiers de licences pour être compatible avec la LGPL 2.1.

### KXMLGUI

+ [kmainwindow] Ne pas supprimer des entrées à partir d'un « kconfiggroup » non valable (bogue 427236).
+ Ne pas recouvrir les fenêtres principales lors de l'ouverture d'instances supplémentaires (bogue 426725).
+ [kmainwindow] ne pas créer de fenêtres natives pour les fenêtres qui ne sont au premier niveau (bogue 424024).
+ KAboutApplicationDialog : vérification que l'onglet « Bibliothèques » est non vide si « HideKdeVersion » est défini.
+ Affichage du code de langue en addition du nom de langue (traduite) dans la boîte de dialogue de basculement de langue pour les applications.
+ Déconseillé « KShortcutsEditor::undoChanges() » et remplacement par le nouveau « undo() ».
+ Gestion d'une double fermeture dans la fenêtre principale (bogue 416728)

### Environnement de développement de Plasma

+ Correction de l'installation à un emplacement incorrect de « plasmoidheading.svgz » (bogue 426537)
+ Fourniture d'une valeur « lastModified » dans « ThemeTest »
+ Détection d'une recherche d'un élément vide et fermeture plus tôt.
+ Utilisation du style standard d'infobulles pour les info-bulles « PlasmaComponents3 » (bogue 424506)
+ Utilisation des couleurs d'en-tête dans les en-têtes de « PlasmoidHeading ».
+ Modification de l'animation du mouvement de mise en valeur de la barre d'onglets « PC2 » facilitant le type à « OutCubic »
+ Ajout d'une prise en charge d'un thème de couleurs pour les astuces
+ Correction des icônes « PC3 Button » / « ToolButton », qui n'ont pas toujours le bon ensemble de couleurs (bogue 426556)
+ Vérification que « FrameSvg » utilise un horodatage « lastModified » lors de l'utilisation du cache (bogue 426674)
+ Vérification qu'il y a toujours un horodatage valable « lastModified » quand « setImagePath » est appelé
+ Déconseillé l'horodatage « lastModified » à 0 dans « Theme::findInCache » (bogue 426674)
+ Adaptation de l'importation « QQC2 » vers un nouveau schéma de nommage des versions.
+ [windowthumbnail] Vérification qu'aucun paramètre « GLContext » associé n'existe.
+ Ajoute de l'importation manquante de « PlasmaCore » vers « ButtonRow.qml »
+ Correction de quelques erreurs de référence de plus dans « PlasmaExtras.ListItem ».
+ Correction de l'erreur « implicitBackgroundWidth » dans « PlasmaExtras.ListItem »
+ Appel au mode de modification par « Mode de modification »
+ Correction d'un « TypeError » dans « QueryDialog.qml »
+ Correction de « ReferenceError » de « PlasmaCore » dans « Button.qml »

### QQC2StyleBridge

+ Utilisation aussi des couleurs de coloration syntaxique de texte quand « checkDelegate » est indiqué (bogue 427022).
+ Respect du clic dans la barre de défilement pour se déplacer à la valeur de la position (bogue 412685)
+ Non-transmission des couleurs par défaut vers le style des barres d'outils du bureau.
+ Ajustement de la licence du fichier vers la licence « GPL » 2.0 ou supérieure
+ Utilisation des couleurs d'en-tête pour les barres d'outils d'en-tête.
+ Déplacement de la déclaration de l'ensemble des couleurs, à un emplacement où elle peut être ignorée.
+ Utilisation des couleurs d'en-tête pour les barres d'outils pour le style du bureau.
+ Ajout de la propriété manquante « isItem », nécessaire pour les arborescences

### Sonnet

+ Abaissement de la sortie des trigrammes

### Coloration syntaxique

+ AppArmor : correction de l'expression rationnelle pour la détection des emplacements
+ AppArmor : ajout d'une coloration syntaxique pour « AppArmor » 3.0
+ Cache de couleurs pour les conversions « rgb » vers « ansi256colors » (accélère le chargement « Markdown »)
+ SELinux : utilisation des mots clés « include »
+ Sous-titres « SubRip » &amp; petites améliorations de « Logcat »
+ Générateur pour « doxygenlua.xml »
+ Correction des formules « LaTex » pour « doxygen » (bogue 426466)
+ Utilisation d'un « Q_ASSERT » similaire dans l'environnement de développement existant + correction des assertions.
+ Renommage de « --format-trace » en « --syntax-trace »
+ Appliquer un style à un zone
+ Tracer les contextes et les zones
+ Utilisation de la couleur d'arrière-plan de l'éditeur par défaut
+ Surlignage « ANSI »
+ Ajout d'une licence et d'une couleur mise à jour du séparateur dans le thème « Radical »
+ Mise à jour de la couleur du séparateur dans les thèmes solarisés
+ Amélioration de la couleur du séparateur et de la bordure d'icônes pour les thèmes « Ayu », « Nord » et « Vim Sombre »
+ Réalisation d'une couleur pour le séparateur moins envahissante
+ Importation du schéma de Kate pour le convertisseur de thèmes par Juraj Oravec
+ Section plus importante concernant les licences, lien avec la copie de notre fichier « MIT.txt »
+ Premier modèle pour le générateur « base16 » (Voir https://github.com/chriskempson/base16)
+ Ajout des informations correctes de licences pour tous les thèmes
+ Ajout d'un thème de couleurs « Radical »
+ Ajout d'un thème de couleurs « Nord »
+ Amélioration de la présentation des thèmes pour afficher plus de styles.
+ Ajout des thèmes « gruvbox » clair et sombre, sous licence « MIT »
+ Ajout d'un thème de couleurs « ayu » (avec des variantes clair, sombre et mirage)
+ Ajout d'une alternative « POSIX » pour l'affection simple d'une variable
+ Outils pour générer un graphique à partir d'un fichier de syntaxe
+ Correction de la conversion automatique de la couleur non définie « QRgb == 0 » vers « Noir » à la place de « Transparent »
+ Ajout de la note de version « Debian » et des fichiers d'exemples de contrôles
+ Ajout d'un thème de couleurs « Dracula »

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
