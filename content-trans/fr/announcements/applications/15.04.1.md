---
aliases:
- ../announce-applications-15.04.1
changelog: true
date: '2015-05-12'
description: KDE publie la version 15.04.1 des applications de KDE.
layout: application
title: KDE publie KDE Applications 15.04.1
version: 15.04.1
---
12 mai 2015. Aujourd'hui, KDE a publié la première mise à jour de consolidation pour les <a href='../15.04.0'>applications 15.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 50 corrections de bogues apportent des améliorations à kdelibs, kdepim, kdenlive, okular, marble and umbrello.

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.19, de la plate-forme de développement 4.14.8 et de la suite Kontact4.14.8.
