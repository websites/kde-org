---
aliases:
- ../announce-applications-15.04.3
changelog: true
date: 2015-07-01
description: KDE publie les applications de KDE en version 15.04.3
layout: application
title: KDE publie les applications de KDE en version 15.04.3
version: 15.04.3
---
01 Juillet 2015. Aujourd'hui, KDE a publié la troisième mise à jour de consolidation pour les <a href='../15.04.0'>applications 15.04 de KDE</a>. Cette version ne contient que des corrections de bogues et des mises à jour de traduction, permettant une mise à jour sûre et appréciable pour tout le monde.

Plus de 20 corrections de bogues apportent des améliorations à kdenlive, kdepim, kopete, ktp-contact-list, marble, okteta and umbrello.

Cette version inclut également les versions maintenues à long terme des espaces de travail de Plasma 4.11.21, de la plate-forme de développement 4.14.10 et de la suite Kontact4.14.10.
