---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE rilascia KDE Applications 17.08.1
layout: application
title: KDE rilascia KDE Applications 17.08.1
version: 17.08.1
---
7 settembre 2017. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../17.08.0'>KDE Applications 17.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 20 errori corretti includono, tra gli altri, miglioramenti a Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello e KDE games.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.36.
