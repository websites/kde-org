---
aliases:
- ../announce-4.11.1
date: 2013-09-03
description: KDE rilascia la versione 4.11.1 degli spazi di lavoro Plasma (Workspaces),
  delle applicazioni (Applications) e della piattaforma (Platform)
title: KDE rilascia gli aggiornamenti di settembre per gli spazi di lavoro Plasma
  (Workspaces), le applicazioni (Applications) e la piattaforma (Platform)
---
3 settembre 2013. Oggi KDE ha rilasciato gli aggiornamenti per gli spazi di lavoro Plasma (Workspaces), le applicazioni (Applications) e la piattaforma di sviluppo (Development Platform). Questi aggiornamenti costituiscono il primo di una serie di aggiornamenti mensili di stabilizzazione per la serie 4.11. Come annunciato precedentemente, gli spazi di lavoro continueranno a ricevere aggiornamenti per i prossimi due anni. Questo rilascio contiene solo correzioni di bug e aggiornamenti delle traduzioni e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 70 bug corretti includono miglioramenti al gestore delle finestre KWin, al gestore dei file Dolphin e ad altri programmi. Gli utenti potranno aspettarsi un avvio più rapido del desktop Plasma, uno scorrimento più fluido di Dolphin e un uso minore di memoria da parte di vari programmi e strumenti. I miglioramenti includono il ritorno del trascinamento dalla barra delle applicazioni al cambia-desktop, correzioni all'evidenziazione ed ai colori in Kate e MOLTI piccoli bug risolti nel gioco Kmahjongg. Ci sono anche molte correzioni che migliorano la stabilità e i consueti aggiornamenti alle traduzioni.

Un <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2011-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.1&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>elenco</a> più completo delle modifiche è disponibile nel sistema di tracciamento dei bug di KDE. Per un elenco dettagliato delle modifiche che sono state introdotte in 4.11.1 puoi anche controllare i log di Git.

Per scaricare il codice sorgente o i pacchetti da installare va nella <a href='/info/4/4.11.1'>pagina delle informazioni di 4.11.1</a>. Se vuoi scoprire di più sulla versione 4.11 degli spazi di lavoro Plasma, delle applicazioni e della piattaforma di sviluppo, fai riferimento alle <a href='/announcements/4.11/'>note di rilascio di 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`Il nuovo processo di invio posticipato in Kontact` width="600px">}}

Il software KDE, incluse tutte le librerie e le applicazioni, è disponibile gratuitamente sotto licenze Open Source. Il software KDE può essere ottenuto come sorgente e in vari formati binari da <a href='http://download.kde.org/stable/4.11.1/'>download.kde.org</a> o con qualsiasi <a href='/distributions'>principale distribuzione GNU/Linux o sistema UNIX</a> attualmente disponibile.
