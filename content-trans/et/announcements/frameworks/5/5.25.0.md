---
aliases:
- ../../kde-frameworks-5.25.0
date: 2016-08-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Üldine

- Nüüd on nõuav Qt &gt;= 5.5

### Attica

- HTTP ümbersuunamiste järgimine

### Breeze'i ikoonid

- mail- 16px ikoonide uuendamine nende parema äratundmise huvides
- mikrofoni ja ehli olekuikoonide uuendamine, et need oleksid sama paigutuse ja suurusega
- Uus Süsteemi seadistuste rakenduseikoon
- gnome olekuikoonide lisamine
- gnome 3 ikoonide toetuse lisamine
- Added icons for Diaspora and Vector, see phabricator.kde.org/M59
- Dolphini ja Gwenview uued ikoonid
- ilmaikoonid on oleku-, mitte rakenduseikoonid
- mõne lingi lisamine xliff'ile tänu gnastyle'ile
- kig'i ikooni lisamine
- MIME tüüpide ikoonide, krdc ikooni, teiste rakenduste ikoonide lisamine gnastyle'ist
- sertifikaadi MIME tüübi ikooni lisamine (veateade 365094)
- gimp'i ikoonide uuendamine tänu gnastyle'ile
- gloobuse toiminguikoon ei ole enam lingitud fail, palun kasutada seda digikam'is
- labplot'i ikoonide uuendamine vastavalt Alexander Semke 13.07 kirjale
- Rakenduseikoonide lisamine gnastyle'ist
- kruler'i ikooni lisamine tänu Yuri Fabirovskyle
- katkiste svg-failide parandus tänu fuchsile (veateade 365035)

### CMake'i lisamoodulid

- Kaasamise parandus, kui Qt5 puudub
- query_qmake() tagavarameetodi lisamine, kui Qt5 paigaldus puudub
- Tagamine, et ECMGeneratePriFile.cmake käitub samamoodi nagu ülejäänud ECM
- Appstreami andmete eelistatud asukohta muudeti

### KActivities

- [KActivities-CLI] käsud tegevuse käivitamiseks ja peatamiseks
- [KActivities-CLI] tegevuse nime, ikooni ja kirjelduse määramine ja hankimine
- CLI rakenduse lisamine tegevuste juhtimiseks
- Skriptide lisamine lülitumiseks eelmisele ja järgmisele tegevusele
- QFlatSet'i lisamise meetod tagastab nüüd indeksi koos iteraatoriga (veateade 365610)
- ZSH funktsioonide lisamine mitteaktiivsete tegevuste peatamiseks ja kustutamiseks
- Omaduse isCurrent lisamine KActivities::Info'le
- Tegevuse otsimisel konstandi iteraatorite kasutamine

### KDE Doxygeni tööriistad

- Palju väljundi vormindamise täiustusi
- Mainpage.dox on nüüd kõrgema prioriteediga kui README.md

### KArchive

- Mitme gzip-voo käitlemine (veateade 232843)
- Eeldatakse, et kataloog on kataloog, isegi õigused on valesti määratud (veateade 364071)

### KBookmarks

- KBookmarkGroup::moveBookmark: tagastusväärtuse parandus, kui element on juba õiges asukohas

### KConfig

- DeleteFile'i ja RenameFile'i standardsete kiirklahvide lisamine

### KConfigWidgets

- DeleteFile'i ja RenameFile'i standardsete toimingute lisamine
- Seadistuslehekülg kasutab nüüd vajaduse korral kerimisribasid (veateade 362234)

### KCoreAddons

- Teadaolevate litsentside paigaldamine ja nende leidmine käitusajal (tagasilanguse parandus) (veateade 353939)

### KDeclarative

- valueChanged'i tegelik väljastamine

### KFileMetaData

- xattr'i kontroll seadistamise ajal, vastasel juhul võib ehitamine nurjuda (xattr.h puudub)

### KGlobalAccel

- klauncher'i dbus'i kasutamine KRun'i asemel (veateade 366415)
- Hüpikloendi toimingute käivitamine KGlobalAccel'i vahendusel
- KGlobalAccel: ummiku parandus väljumise korral Windowsi keskkonnas

### KHTML

- Protsentühiku toetus piirde raadiusel
- tausta ja piirderaadiuse omaduste prefiksiga versioonide eemaldamine
- 4 väärtusega kiirklahvikonstruktori funktsiooni parandused
- stringiobjektide loomine ainult siis, kui neid hakatakse kasutama

### KIconThemes

- makeCacheKey jõudluse tuntav parandamine, kuna see on ikoonide otsimisel otsustava tähtsusega kood
- KIconLoader: otsingute arvu vähendamine  varukoopiate kasutamisel
- KIconLoader: kiiruse võimas parandamine kättesaamatute ikoonide laadimisel
- Kategooria vahetamisel otsingurida ei puhastata
- KIconEngine: parandus - QIcon::hasThemeIcon tagastas alati true (veateade 365130)

### KInit

- Kinit'i kohandamine Mac OS X peale

### KIO

- KIO::linkAs() parandus, et see töötaks nagu kord ja kohus, s.t. nurjuks, kui sihtkoht on juba olemas
- KIO::put("file:///path") parandus, et see austaks umask'i (veateade 359581)
- KIO::pasteActionText parandus puuduva asukohaga elemendi ja tühja URL-i korral
- Nimeviida loomise tagasivõtmise toetuse lisamine
- Graafilise kasutajaliidese valik seadistada KIO-moodulite globaalset MarkPartial'i
- Parandus: MaxCacheSize oli piiratud 99 KiB-ga
- Lõikepuhvri nuppude lisamine kontrollsumma kaardile
- KNewFileMenu: mallifaili põimitud ressursist kopeerimise parandus (veateade 359581)
- KNewFileMenu: rakendusele lingi loomise parandus (veateade 363673)
- KNewFileMenu: uue failinime pakkumise parandus, kui fail on töölaual juba olemas
- KNewFileMenu: tagamine, et fileCreated() väljastatakse ka töölauafailide korral
- KNewFileMenu: suhtelise sihtkohaga nimeviitade loomise parandus
- KPropertiesDialog: nupukasti kasutamise lihtsustamine, klahvi Esc käitumise parandus
- KProtocolInfo: puhvri taastäitmine äsja paigaldatud protokollide leidmiseks
- KIO::CopyJob: portimine qCDebug'i peale (omaette alale, sest see võib olla üpris jutukas)
- KPropertiesDialog: kontrollsummakaardi lisamine
- URL-i asukoha puhastamine enne KUrlNavigator'i initsialiseerimist

### KItemModels

- KRearrangeColumnsProxyModel: eeldamise parandus index(0, 0)-s tühja mudeli korral
- KDescendantsProxyModel::setSourceModel() parandus: see jättis sisemised puhvrid puhastamata
- KRecursiveFilterProxyModel: parandati QSFPM riknemine, mille põhjustas signaali rowsRemoved väljafiltreerimine (veateade 349789)
- KExtraColumnsProxyModel: hasChildren() teostus

### KNotification

- Alampaigutuse eellast ei määrata käsitsi, hoiatuse vaigistamine

### Paketiraamistik

- ParentApp'i eeldamine PackageStructure'i plugina järgi
- kpackagetool5 genereerib kpackage'i komponentide appstream-teabe
- Faili metadata.json laadimise võimaldamine kpackagetools5-st

### Kross

- Kasutamata KF5 sõltuvuste eemaldamine

### KService

- applications.menu: kasutamata kategooriate viidete eemaldamine
- Trader'i parserit uuendatakse alati yacc/lex allikate pealt

### KTextEditor

- Omadialoogis ei küsita kaks korda luba faili üle kirjutada
- FASTO süntaksi lisamine

### KWayland

- [klient] QPointer'i kasutamine the enteredSurface'i jaoks Pointer'is
- Geomeetria avaldamine PlasmaWindowModel'is
- Geomeetriasündmuse lisamine PlasmaWindow'le
- [src/server] Enne viidasisendi saatmist kontrollitakse, kas pinnal on ressurss või mitte
- xdg shelli toetuse lisamine
- [server] valiku puhastamise kohane saatmine, enne kui klaviatuur võtab fookusse Enteri
- [server] XDG_RUNTIME_DIR'i puudumise käitlemine viisakamalt

### KWidgetsAddons

- [KCharSelect] Krahhi vältimine otsimisel puuduva andmefailiga (veateade 300521)
- [KCharSelect] märkide käitlemine väljaspool BMP-d (veateade 142625)
- [KCharSelect] kcharselect'i andmete uuendamine Unicode 9.0.0 peale (veateade 336360)
- KCollapsibleGroupBox: animatsiooni peatamine destruktoris, kui see veel käib
- Breeze'i paleti uuendamine (sünkroonimine KColorScheme'iga)

### KWindowSystem

- [xcb] Signaali compositingChanged väljastamise kindlustamine NETEventFilter'i taasloomisel (veateade 362531)
- Mugavuse nimel API lisamine Qt kasutatava aknasüsteemi/platvormi pärimiseks

### KXMLGUI

- Miinimumsuuruse vihje parandus (veateade 312667)
- [KToggleToolBarAction] action/options_show_toolbar piirangu arvestamine

### NetworkManagerQt

- Default to WPA2-PSK and WPA2-EAP when getting security type from connection settings

### Oxygeni ikoonid

- rakendusemenüü lisamine oxygenile (veateade 365629)

### Plasma raamistik

- Pesa createApplet hoitakse ühilduvana Frameworks 5.24-ga
- gl-tekstuuri ei kustutata pisipildis kaks korda (veateade 365946)
- Tõlkedomeeni lisamine taustapildi QML-objektile
- Aplette ei kustutata käsitsi
- Plasma taustapildi kapptemplate'i lisamine
- Mallid: mallide registreerimine omaette tipptasandi kategoorias "Plasma/"
- Templates: techbase'i wiki-linkide uuendamine README-des
- Määramine, milline Plasma packagestructure laiendab plasmashell'i
- suuruse toetus aplettide lisamisel
- Plasma PackageStructure'i defineerimine KPackage'i regulaarse PackageStructure'i pluginana
- Parandus: taustapildinäidise Autumn seadistuse uuendamine QtQuick.Controls'i peale
- KPackage'i kasutamine Plasma pakettide paigaldamiseks
- Kui me edastame QIcon'i argumendina IconItem::Source'ile, siis tuleb seda ka kasutada
- Ülekatte toetuse lisamine Plasma IconItem'ile
- Qt::Dialog'i lisamine vaikimisi lippudele, et QXcbWindow::isTransient() oleks rahul (veateade 366278)
- [Breeze'i Plasma teema] võrgu lennurežiimi sisse-väljalülitamise ikoonide lisamine
- contextualActionsAboutToShow' väljastamine enne apleti contextualActions menüü näitamist (veateade 366294)
- Seondamine teksti asemel TextField'i pikkusega
- [Nupustiilid] Rõhtne tsentreerimine "ainult ikoonid" režiimis (veateade 365947)
- [Containment] HiddenStatus'e kohtlemine alama olekuna
- kruler'i süsteemisalve ikooni lisamine tänu Yuri Fabirovskyle
- Kurikuulsa dialoogide tegumihalduri peale ilmumise vea veelkordne parandus
- Juhtmeta võrgu saadavuse ikooni sümboli ? parandus (veateade 355490)
- IconItem: parem võimalus keelata animatsiooni üleminekul nähtamatust olekust nähtavasse
- Kohtspikri aknatüübi määramine ToolTipDialog'is KWindowSystem'i API abil

### Solid

- Predicate'i parserit uuendatakse alati yacc/lex allikate pealt

### Sonnet

- hunspell: koodi puhastamine sõnastike otsimiseks, XDG kataloogide lisamine (veateade 361409)
- Keelefitri parandamise katse keele tuvastamise kindlustamiseks

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
