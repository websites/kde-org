---
date: 2013-08-14
hidden: true
title: Plasma-työtilat 4.11 jatkaa käyttäjäkokemuksen hiomista
---
{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/empty-desktop.png" caption=`KDE Plasma -työtilat 4.11` width="600px" >}}

In the 4.11 release of Plasma Workspaces, the taskbar – one of the most used Plasma widgets – <a href='http://blogs.kde.org/2013/07/29/kde-plasma-desktop-411s-new-task-manager'>has been ported to QtQuick</a>. The new taskbar, while retaining the look and functionality of its old counterpart, shows more consistent and fluent behavior. The port also resolved a number of long standing bugs. The battery widget (which previously could adjust the brightness of the screen) now also supports keyboard brightness, and can deal with multiple batteries in peripheral devices, such as your wireless mouse and keyboard. It shows the battery charge for each device and warns when one is running low. The Kickoff menu now shows recently installed applications for a few days. Last but not least, notification popups now sport a configure button where one can easily change the settings for that particular type of notification.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/notifications.png" caption=`Parannettu ilmoitusten käsittely` width="600px" >}}

KMix, KDE's sound mixer, received significant performance and stability work as well as <a href='http://kmix5.wordpress.com/2013/07/26/kmix-mission-statement-2013/'>full media player control support</a> based on the MPRIS2 standard.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/battery-applet.png" caption=`Uudelleen suunniteltu akkusovelma toiminnassa` width="600px" >}}

## KWin-ikkunointiohjelma ja -koostin

Ikkunointiohjelmassa KWinissä on tapahtunut jälleen suuria muutoksia. Vanhaa teknologiaa on poistettu käytöstä ja XCB-viestintäprotokolla on otettu käyttöön. Näiden seurauksena ikkunoiden hallinta on nyt sujuvampaa ja nopeampaa. Myös tuki OpenGL 3.1:lle ja OpenGL ES 3.0:lle on lisätty. Tässä julkaisussa on myös ensimmäinen kokeellinen X11:n seuraajan Waylandin tuki, joka mahdollistaa käyttää KWiniä X11-ympäristössä Waylandin päällä. Lisätietoja tämän kokeellisen tuen käytöstä saat <a href='http://blog.martin-graesslin.com/blog/2013/06/starting-a-full-kde-plasma-session-in-wayland/'>tästä kirjoituksesta</a> (englanniksi). KWinin skriptausrajapinta on parantunut valtavasti, ja siinä on nyt tuki asetuskäyttöliittymälle, uusia animaatioita, graafisia tehosteita sekä monia pienempiä parannuksia. Tässä julkaisussa on myös parempi tuki useille näytöille (esimerkiksi näytön reunan hehkuasetus aktiivisille kulmille), parempi ikkunoiden nopea vierekkäinasettelu (englanniksi quick tiling) mahdollisuudella muuttaa vierekkäin asettelun alueita sekä tavanomainen liuta virhekorjauksia ja optimointeja. Lue lisää englanniksi <a href='http://blog.martin-graesslin.com/blog/2013/06/what-we-did-in-kwin-4-11/'>täältä</a> ja <a href='http://blog.martin-graesslin.com/blog/2013/06/new-kwin-scripting-feature-in-4-11/'>täältä</a>.

## Näyttöjen käsittely ja WWW-pikavalinnat

Järjestelmäasetusten näyttöasetukset on korvattu uudella <a href='http://www.afiestas.org/kscreen-1-0-released/'>KScreen-työkalulla</a>. KScreen tuo Plasma-työtiloihin älykkäämmän useiden näyttöjen tuen: se valitsee uusien näyttöjen asetukset automaattisesti ja muistaa näytöille käsin annetut asetukset. KScreenissä on intuitiivinen, visuaalinen käyttöliittymä, jolla näyttöjen järjestely hoituu vetämällä ja pudottamalla.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/kscreen.png" caption=`Uusi KScreen näyttöjen hallintaan` width="600px" >}}

WWW-pikavalinnat, nopein tapa etsiä haluamaansa verkosta, on siistitty ja pikavalintoja on paranneltu. Monet pikavalinnoista on päivitetty käyttämään turvallisesti salattuja (TLS/SSL) yhteyksiä, uusia pikavalintoja on lisätty ja muutamia vanhentuneita on poistettu. Omien pikavalintojen lisäämistapaa on myös parannettu. Lisätietoja saat <a href='https://plus.google.com/108470973614497915471/posts/9DUX8C9HXwD'>täältä</a> (englanniksi).

Tämä on viimeinen KDE SC 4 -sarjan Plasma-työtilat 1 -julkaisu, joka sisältää uusia ominaisuuksia. Tätä julkaisua tuetaan ainakin kaksi vuotta, jotta siirtymä seuraavaan sukupolveen on helpompi. Uusien ominaisuuksien kehitys keskittyy tästä lähtien Plasma-työtilat 2:een, mutta suorituskykyparannukset ja virheiden korjaus keskittyvät edelleen 4.11-sarjaan.

#### Plasman asentaminen

KDE-ohjelmat, mukaan lukien kaikki KDE:n kirjastot ja sovellukset, ovat ilmaisesti saatavilla avoimen lähdekoodin lisenssien mukaisesti. KDE-ohjelmia voi käyttää useilla laitekokoonpanoilla ja suoritinarkkitehtuureilla kuten ARMilla ja x86:lla, useissa käyttöjärjestelmissä, ja ne toimivat kaikenlaisten ikkunointiohjelmien ja työpöytäympäristojen kanssa. Linux- ja UNIX-pohjaisten käyttöjärjestelmien lisäksi useimmista KDE-sovelluksista on saatavilla Microsoft Windows -versiot <a href='http://windows.kde.org'>KDE-ohjelmat Windowsissa</a> -sivustolta ja Apple Mac OS X -versiot <a href='http://mac.kde.org/'>KDE-ohjelmat Macillä</a> -sivustolta. Joistain KDE-sovelluksista on verkossa kokeellisia versioita, jotka toimivat useilla mobiilialustoilla kuten Meegolla, Microsoftin Windows Mobilella ja Symbianilla, mutta niitä ei tällä hetkellä tueta. <a href='http://plasma-active.org'>Plasma Active</a> on käyttökokemus laajemmalle laitekirjolle kuten esimerkiksi tablettitietokoneille ja muille mobiililaitteille.

KDE software can be obtained in source and various binary formats from <a href='http://download.kde.org/stable/4.11.0'>download.kde.org</a> and can also be obtained on <a href='/download'>CD-ROM</a> or with any of the <a href='/distributions'>major GNU/Linux and UNIX systems</a> shipping today.

##### Paketit

Some Linux/UNIX OS vendors have kindly provided binary packages of 4.11.0 for some versions of their distribution, and in other cases community volunteers have done so. <br />

##### Pakettien sijainnit

For a current list of available binary packages of which the KDE's Release Team has been informed, please visit the <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_4.11.0'>Community Wiki</a>.

The complete source code for 4.11.0 may be <a href='/info/4/4.11.0'>freely downloaded</a>. Instructions on compiling and installing KDE software 4.11.0 are available from the <a href='/info/4/4.11.0#binary'>4.11.0 Info Page</a>.

#### Järjestelmävaatimukset

In order to get the most out of these releases, we recommend to use a recent version of Qt, such as 4.8.4. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br /> In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.

## Tänään tiedotettiin myös seuraavista:

## <a href="../applications"><img src="/announcements/4/4.11.0/images/applications.png" class="app-icon" alt="The KDE Applications 4.11"/> KDE Applications 4.11 Bring Huge Step Forward in Personal Information Management and Improvements All Over</a>

Tätä julkaisua leimaa runsaat parannukset KDE:n PIM-pinossa (henkilökohtaisten tietojen hallinnassa). Niiden ansiosta sen suorituskyky on nyt paljon parempi ja siinä on paljon uusia ominaisuuksia. Kate parantaa Python- ja Javascript-kehittäjien tuottavuutta uusilla liitännäisillä. Dolphin nopeutui, ja opetusohjelmissa on monia uusia ominaisuuksia.

## <a href="../platform"><img src="/announcements/4/4.11.0/images/platform.png" class="app-icon" alt="The KDE Development Platform 4.11"/> KDE Platform 4.11 Delivers Better Performance</a>

Tämä KDE-ohjelmistoalustan 4.11-julkaisu keskittyy edelleen vakauteen. Uusia ominaisuuksia toteutetaan parhaillaan myöhemmin tulossa olevaan KDE Frameworks 5.0 -julkaisuun, mutta onnistuimme mahduttamaan vakaaseen julkaisuumme optimointeja Nepomuk-sovelluskehykseemme.
