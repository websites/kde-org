---
aliases:
- ../announce-applications-17.12-beta
custom_spread_install: true
date: 2017-11-17
description: KDE Ships Applications 17.12 Beta.
layout: application
release: applications-17.11.80
title: KDE publica a beta da versión 17.12 das aplicacións de KDE
---
17 de novembro de 2017. Hoxe KDE publicou a beta da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Check the <a href='https://community.kde.org/Applications/17.12_Release_Notes'>community release notes</a> for information on tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Hai que probar ben a versión 17.12 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a beta <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.

#### Instalar os paquetes binarios da beta da versión 17.12 das aplicacións de KDE

<em>Packages</em>. Some Linux/UNIX OS vendors have kindly provided binary packages of KDE Applications 17.12 Beta (internally 17.11.80) for some versions of their distribution, and in other cases community volunteers have done so. Additional binary packages, as well as updates to the packages now available, may become available over the coming weeks.

<em>Package Locations</em>. For a current list of available binary packages of which the KDE Project has been informed, please visit the <a href='http://community.kde.org/Binary_Packages'>Community Wiki</a>.

#### Compilar a beta da versión 17.12 das aplicacións de KDE

The complete source code for KDE Applications 17.12 Beta may be <a href='http://download.kde.org/unstable/applications/17.11.80/src/'>freely downloaded</a>. Instructions on compiling and installing are available from the <a href='/info/applications/applications-17.11.80'>KDE Applications 17.12 Beta Info Page</a>.
