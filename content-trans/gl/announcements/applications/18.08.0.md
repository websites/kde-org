---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE publica a versión 18.08.0 das aplicacións de KDE
layout: application
title: KDE publica a versión 18.08.0 das aplicacións de KDE
version: 18.08.0
---
16 de agosto de 2018. Publicouse a versión 18.08.0 das aplicacións de KDE.

Traballamos continuamente en mellorar o software que se inclúe nas aplicacións de KDE, e esperamos que as novas melloras e correccións de fallos lle resulten de utilidade!

### What's new in KDE Applications 18.08

#### Sistema

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE's powerful file manager, has received various quality-of-life improvements:

- O diálogo de configuración modernizouse para axeitarse mellor ás nosas directrices de deseño e ser máis intuitivo.
- Elimináronse varias fugas de memoria que podían facer que o computador funcionase con maior lentitude.
- Ao ver o lixo xa non están dispoñíbeis os elementos de menú de «Crear novo».
- Agora a aplicación adáptase mellor as pantallas de alta resolución.
- Agora o menú de contexto inclúe máis opcións útiles, permitíndolle ordenar e cambiar a vista de maneira máis directa.
- Sorting by modification time is now 12 times faster. Also, you can now launch Dolphin again when logged in using the root user account. Support for modifying root-owned files when running Dolphin as a normal user is still work in progress.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Multiple enhancements for <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, KDE's terminal emulator application, are available:

- Agora o trebello de «Atopar» aparecerá na parte superior da xanela sen romper o fluxo de traballo.
- Engadiuse compatibilidade con máis secuencias de escape (DECSCUSR e o modo de desprazamento alternativo de XTerm).
- Agora tamén pode asignar calquera carácter como tecla para un atallo.

#### Gráficos

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 is a major release for <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE's image viewer and organizer. Over the last months contributors worked on a plethora of improvements. Highlights include:

- Agora a barra de estado de Gwenview ten un contados de imaxes e mostra o número total de imaxes.
- Agora pódese ordenar por puntuación e en orde descendente. Agora ordenar por data separa os directorios e os arquivos e corrixiuse nalgunhas situacións.
- Mellorouse a funcionalidade de arrastrar e soltar para permitir arrastrar ficheiros e cartafoles ao modo de vista para mostralos, así como arrastrar elementos vistos a aplicacións externas.
- Pegar imaxes copiadas de Gwenview agora tamén funciona con aplicacións que só aceptan datos de imaxe en bruto, pero sen ruta de ficheiro. Agora tamén se permite copiar imaxes modificadas.
- Cambiouse completamente o diálogo de cambio de tamaño de imaxes para que sexa máis fácil de usar e para engadir unha opción para cambiar as imaxes de tamaño segundo unha porcentaxe.
- Corrixíronse o control desprazábel de tamaño e o cursor de mira da ferramenta de redución de ollos vermellos.
- Agora a selección de fondo transparente ten unha opción «Ningunha» e pode configurarse tamén con SVG.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

A ampliación de imaxes é agora máis cómoda:

- Permitir ampliar mediante desprazamento ou premendo, así como afastando os dedos, tamén cando as ferramentas de recorte e de redución de ollos vermellos están activadas.
- O clic central volve conmutar entre ampliación para axustar e ampliación ao 100%.
- Engadíronse os atallos de teclado Maiús+clic central e Maiús+F para conmutar a ampliación de «Encher».
- Agora Ctrl+clic amplía máis rapidamente e de maneira máis fiábel.
- Agora Gwenview achega á posición actual do rato para as operacións de achegar e afastar, encher e ampliar ao 100% ao usar os atallos de rato e teclado.

Fixéronse varias melloras no modo de comparación de imaxes:

- Corrixíronse o tamaño e o aliñamento do salientado de selección.
- Corrixiuse a sobreposición de SVG do salientado de selección.
- Para imaxes SVG pequenas, o salientado da selección coincide co tamaño da imaxe.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Introducíronse varias pequenas melloras para mellorar aínda máis o fluxo de traballo:

- Melloráronse as transicións de esvaecemento entre imaxes de distintos tamaños e transparencias.
- Corrixiuse a visibilidade de iconas nalgúns botóns flotantes cando se unha un esquema de cores claro.
- Ao gardar unha imaxe con outro nome, o visor non salta a unha imaxe non relacionada despois.
- Ao premer o botón de compartir sen estar instalados os complementos KIPI, Gwenview suxerirá ao usuario instalalos. Tras a instalación móstranse inmediatamente.
- Agora a barra lateral evita agocharse accidentalmente ao cambiar de tamaño, e lembra a súa anchura.

#### Oficina

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, KDE's powerful email client, features some improvements in the travel data extraction engine. It now supports UIC 918.3 and SNCF train ticket barcodes and Wikidata-powered train station location lookup. Support for multi-traveler itineraries was added, and KMail now has integration with the KDE Itinerary app.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, the personal information management framework, is now faster thanks to notification payloads and features XOAUTH support for SMTP, allowing for native authentication with Gmail.

#### Educación

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE's frontend to mathematical software, now saves the status of panels (\"Variables\", \"Help\", etc.) for each session separately. Julia sessions have become much faster to create.

User experience in <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, our graphing calculator, has been significantly improved for touch devices.

#### Utilidades

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Contributors to <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE's versatile screenshot tool, focused on improving the Rectangular Region mode:

- Agora, no modo de rexión rectangular, hai unha lupa para axudar a debuxar un rectángulo de selección axustado a nivel de píxel.
- Agora pode mover e cambiar de tamaño o rectángulo de selección usando o teclado.
- A interface de usuario sigue o esquema de cores do usuario e mellorouse o texto de axuda da presentación.

Para facilitar compartir, agora cópianse automaticamente no portapapeis ligazóns a imaxes compartidas. Agora as capturas de pantalla poden gardarse automaticamente en subdirectorios indicados polos usuarios.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, our webcam recorder, was updated to avoid crashes with newer GStreamer versions.

### Destrución de fallos

Solucionáronse máis de 120 fallos en aplicacións como, entre outros, a colección de Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle e Umbrello!

### Historial completo de cambios
