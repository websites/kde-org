---
date: '2014-04-16'
description: KDE distribueix les aplicacions i la plataforma 4.13.
hidden: true
title: Les aplicacions del KDE 4.13 es beneficien de la nova cerca semàntica i introdueixen
  noves funcionalitats
---
La comunitat del KDE anuncia amb satisfacció la darrera actualització major de les aplicacions KDE, que aporta noves funcionalitats i esmenes d'errors. El Kontact (el gestor d'informació personal) ha rebut una activitat intensa, beneficiant-se de les millores de la tecnologia de cerca semàntica del KDE, i introduint funcionalitats noves. El visualitzador de documents Okular i l'editor avançat de text Kate han rebut millores relacionades amb la interfície i funcionalitats. En les àrees d'educació i jocs, s'ha introduït l'entrenador nou de pronunciació estrangera Artikulate; el Marble (el globus d'escriptori) ha implementat el Sol, la Lluna, els planetes, rutes en bicicleta i les milles nàutiques. El Palapeli (l'aplicació de trencaclosques) ha fet un salt sense precedents a noves dimensions i capacitats.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## El Kontact del KDE introdueix noves funcionalitats i més velocitat

El conjunt de programes Kontact del KDE ha introduït una sèrie de funcionalitats en els seus diversos components. El KMail introdueix l'emmagatzematge en el núvol i ha millorat la implementació del «sieve» per al filtratge en la banda del servidor. El KNotes ara pot generar alarmes i introdueix capacitats de cerca, i hi ha hagut moltes millores en la capa de memòria cau del Kontact, accelerant gairebé totes les operacions.

### Implementació de l'emmagatzematge en el núvol

El KMail introdueix la implementació del servei d'emmagatzematge, per tant, els annexos grans es poden emmagatzemar en serveis al núvol i incloure's com enllaços en els missatges de correu. Els serveis d'emmagatzematge implementats inclouen Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic i hi ha una opció genèrica WebDav. L'eina <em>storageservicemanager</em> ajuda amb la gestió dels fitxers en aquests serveis.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Implementació molt millorada del «sieve»

Els filtres «sieve», una tecnologia que permet al KMail gestionar filtres en el servidor, ara poden gestionar les vacances en servidors múltiples. L'eina KSieveEditor permet que els usuaris editin els filtres «sieve» sense haver d'afegir el servidor en el Kontact.

### Altres canvis

La barra de filtre ràpid té una petita millora d'interfície d'usuari i es beneficia molt de les capacitats de cerca millorada introduïdes en el llançament 4.13 de la plataforma de desenvolupament del KDE. La cerca ha esdevingut significativament més ràpida i exacta. L'editor introdueix un escurçador d'URL, augmentant les traduccions existents, i les eines de retall de text.

Les etiquetes i les anotacions de les dades del PIM ara s'emmagatzemen en l'Akonadi. En les versions futures, també s'emmagatzemaran en els servidors (en IMAP o Kolab), fent possible compartir etiquetes entre diversos ordinadors. L'Akonadi: s'ha afegit la implementació de l'API de Google Drive. S'ha implementat la cerca amb connectors de terceres parts (que significa que els resultats es poden recuperar molt ràpidament) i cerques en el servidor (cercant elements no indexats per un servei d'indexació local).

### KNotes, KAddressbook

S'ha fet força feina en el KNotes, esmenant una sèrie d'errors i inconvenients petits. La capacitat de definir alarmes en les notes és nova, així com la cerca per les notes. Llegiu més <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a>. El KAdressbook ara accepta impressió: més detalls <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>.

### Millores de rendiment

El rendiment del Kontact s'ha millorat notablement en aquesta versió. Algunes millores es deuen a la integració amb la nova versió de la infraestructura de <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>cerca semàntica</a> del KDE, i també s'ha treballat la capa de memòria cau de dades i la càrrega de dades en el mateix KMail. S'ha fet una feina notable per millorar la implementació de la base de dades PostgreSQL. Es pot trobar més informació i detalls en els canvis relacionats amb el rendiment en aquests enllaços:

- Optimitzacions d'emmagatzematge: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>informe de l'esprint</a>
- acceleració i reducció de la base de dades: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>llista de correu</a>
- optimització en l'accés de carpetes: <a href='https://git.reviewboard.kde.org/r/113918/'>taulell de revisió</a>

### KNotes, KAddressbook

S'ha fet força feina en el KNotes, esmenant una sèrie d'errors i inconvenients petits. La capacitat de definir alarmes en les notes és nova, així com la cerca per les notes. Llegiu més <a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a>. El KAdressbook ara accepta impressió: més detalls <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>.

## L'Okular refina la interfície d'usuari

Aquest llançament del lector de documents Okular aporta diverses millores. Ara podeu obrir fitxers PDF múltiples en una instància de l'Okular gràcies a la implementació de les pestanyes. Hi ha un mode de ratolí nou d'ampliació i s'utilitza els PPP de la pantalla actual per a la renderització dels PDF, millorant l'aspecte dels documents. S'inclou un botó nou de reproducció en el mode de presentació i hi ha millores a les accions desfer i refer.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## El Kate introdueix una barra d'estat millorada, concordança animada de parèntesis, i connectors millorats

L'última versió de l'editor avançat de text Kate introdueix la <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>concordança animada de parèntesis</a>, canvis per fer que els <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>teclats amb Alt Gr activat funcionin en el mode vim</a> i una sèrie de millores en els connectors del Kate, especialment en l'àrea de la implementació del Python i el <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>connector de construcció</a>. Hi ha una <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>barra d'estat millorada</a> nova que activa accions directes com el canvi en els paràmetres de sagnat, codificació, i ressaltat, una barra de pestanyes nova en cada vista, implementació de compleció de codi per al <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>llenguatge de programació D</a> i <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>molt més</a>. L'equip ha <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>demanat comentaris millorar el Kate</a> i està centrant part de la seva dedicació a l'adaptació dels Frameworks 5.

## Funcionalitats variades per tot arreu

El Konsole aporta una flexibilitat addicional permetent fulls d'estil personalitzats per controlar les barres de pestanyes. Els perfils ara poden emmagatzemar les mides desitjades de columna i fila. Vegeu més <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>aquí</a>.

L'Umbrello possibilita duplicar diagrames i introdueix menús contextuals intel·ligents que ajusten el seu contingut als ginys seleccionats. També s'han millorat la implementació de desfer i les propietats visuals. El Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>introdueix la implementació per a la vista prèvia dels RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

El mesclador de so KMix introdueix el control remot via el protocol de comunicacions entre processos D-Bus (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>detalls</a>), addicions al menú de so i un diàleg de configuració nou (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>detalls</a>), i una sèrie d'esmenes d'errors i petites millores.

La interfície de cerca del Dolphin s'ha modificat per aprofitar la nova infraestructura de cerca i ha rebut més millores de rendiment. Per als detalls, llegiu aquesta <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>visió general de la tasca d'optimització durant el darrer any</a>.

El KHelpcenter afegeix l'ordenació alfabètica dels mòduls i una reorganització de les categories per facilitar-ne l'ús.

## Aplicacions de jocs i educatives

Les aplicacions de jocs i educatives del KDE han rebut moltes actualitzacions en aquest llançament. L'aplicació trencaclosques del KDE, el Palapeli, ha obtingut <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>funcionalitats noves i enginyoses</a> que faciliten la solució de trencaclosques grans (fins a 10.000 peces) per aquells que acceptin el repte. El KNavalBattle mostra les posicions dels vaixells enemics després de finalitzar la partida, de manera que podreu veure a on anàveu malament.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Les aplicacions educatives del KDE han rebut funcionalitats noves. El KStars guanya una interfície per crear scripts via el D-BUS i pot utilitzar l'API de serveis de la web astrometry.net per optimitzar l'ús de la memòria. El Cantor ha guanyat el ressaltat de sintaxi en el seu editor de scripts, i ara s'han implementat en l'editor els dorsals de Scilab i Python 2. Els mapes educatius i l'eina de navegació Marble ara inclou les posicions del <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sol, Lluna</a> i <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planetes</a> i permet la <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>captura de pel·lícules durant els recorreguts virtuals</a>. S'ha millorat els recorreguts en bicicleta amb l'addició de la implementació de cyclestreets.net. Ara es permeten les milles nàutiques i en clicar un <a href='http://en.wikipedia.org/wiki/Geo_URI'>Geo URI</a> s'obrirà el Marble.

#### Instal·lació d'aplicacions del KDE

El programari KDE, incloses totes les seves biblioteques i les seves aplicacions, és disponible de franc d'acord amb les llicències de codi font obert (Open Source). El programari KDE s'executa en diverses configuracions de maquinari i arquitectures de CPU com ARM i x86, sistemes operatius i treballa amb qualsevol classe de gestor de finestres o entorn d'escriptori. A banda del Linux i altres sistemes operatius basats en UNIX, podeu trobar versions per al Windows de Microsoft de la majoria d'aplicacions del KDE al lloc web <a href='http://windows.kde.org'>programari de Windows</a> i versions per al Mac OS X d'Apple en el <a href='http://mac.kde.org/'>lloc web del programari KDE per al Mac</a>. Les compilacions experimentals d'aplicacions del KDE per a diverses plataformes mòbils com MeeGo, MS Windows Mobile i Symbian es poden trobar en la web, però actualment no estan suportades. El <a href='http://plasma-active.org'>Plasma Active</a> és una experiència d'usuari per a un espectre ampli de dispositius, com tauletes i altre maquinari mòbil.

El programari KDE es pot obtenir en codi font i en diversos formats executables des de <a href='https://download.kde.org/stable/4.13.0'>download.kde.org</a> i també es pot obtenir en <a href='/download'>CD-ROM</a> o amb qualsevol dels <a href='/distributions'>principals sistemes GNU/Linux i UNIX</a> que es distribueixen avui en dia.

##### Paquets

Alguns venedors de Linux/UNIX OS han proporcionat gentilment paquets executables de 4.13.0 per a algunes versions de la seva distribució, i en altres casos ho han fet els voluntaris de la comunitat.

##### Ubicació dels paquets

Per a una llista actual de paquets executables disponibles dels quals ha informat l'equip de llançament del KDE, si us plau, visiteu el <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>wiki de la comunitat</a>.

El codi font complet per a 4.13.0 es pot <a href='/info/4/4.13.0'>descarregar de franc</a>. Les instruccions per a compilar i instal·lar el programari KDE, versió 4.13.0, estan disponibles a la <a href='/info/4/4.13.0#binary'>pàgina d'informació de la versió 4.13.0</a>.

#### Requisits del sistema

Per tal d'aprofitar al màxim aquestes versions, es recomana utilitzar una versió recent de les Qt, com la 4.8.4. Això és necessari per a assegurar una experiència estable i funcional, ja que algunes millores efectuades en el programari KDE s'ha efectuat realment en la infraestructura subjacent de les Qt.

Per tal de fer un ús complet de les capacitats del programari KDE, també es recomana l'ús dels darrers controladors gràfics per al vostre sistema, ja que aquest pot millorar substancialment l'experiència d'usuari, tant per a les funcionalitats opcionals, com el rendiment i l'estabilitat en general.
