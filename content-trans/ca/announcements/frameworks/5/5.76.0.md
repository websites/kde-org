---
aliases:
- ../../kde-frameworks-5.76.0
date: 2020-11-07
layout: framework
libCount: 70
qtversion: 5.12
---
### Baloo

* Divideix els termes en CJK als caràcters de puntuació, optimitza el codi
* Refactoritza el codi en preparació de la integració ICU

### Icones Brisa

* Afegeix la icona «dialog-warning» de 48px
* Canvia l'estil de «media-repeat-single» per a usar el nombre 1
* Afegeix més fitxers ignorats del «git»
* Comprova si existeix el fitxer abans d'eliminar-lo
* Elimina sempre el fitxer de destinació abans de generar els enllaços simbòlics
* Afegeix diverses icones en mode color per a l'Okular
* Afegeix les icones «task-complete» (error 397996)
* Afegeix la icona «network-limited»
* Copia l'enllaç simbòlic del Kup de 32px a «apps/48» per corregir una fallada de la prova d'escalat
* Afegeix les icones «meeting-organizer» (error 397996)
* Afegeix la icona «fingerprint»
* Afegeix les icones «task-recurring» i «appointment-recurring» (error 392533)
* Desactiva temporalment la generació d'icones en el Windows
* Enllaç simbòlic de «kup.svg» a «preferences-system-backup.svg»

### Mòduls extres del CMake

* Fa que «androiddeployqt» trobi les biblioteques i els connectors QML sense instal·lació
* find-modules/FindReuseTool.cmake - Corregeix l'eina de cerca de «reuse»
* Millora les opcions predeterminades de format
* Inclou l'opció d'usar LLVM per als usuaris amb Qt < 5.14
* Afegeix la versió mínima que manca al paràmetre RENAME
* Documenta quan s'ha afegit FindGradle
* Afegeix FindGradle des de KNotification

### KAuth

* Converteix aviat el nom del dorsal a majúscules
* Afegeix un «helper» per a obtenir l'UID de l'invocant

### KCalendarCore

* Troba l'ambigüitat a ICalFormat::toString() en les proves
* Afegeix la serialització de la propietat COLOR a partir del RFC7986
* Fa que MemoryCalendar::rawEvents(QDate, QDate) funcioni per als límits oberts

### KCMUtils

* Adapta des de QStandardPaths::DataLocation a QStandardPaths::AppDataLocation
* Afegeix la implementació d'espai de noms a la macro CMake de KCModuleData
* Fa obsolet KSettings::PluginPage
* Elimina la referència a una capçalera no definida
* Mou el KCMUtilsGenerateModuleData al lloc correcte
* Publica totes les subpàgines precreades d'un KCM
* Afegeix la funció del CMake per a generar dades bàsiques de mòdul
* Millora la llegibilitat del QML en línia a «kcmoduleqml.cpp»
* Alçada adequada de la capçalera amb una variable de substitució
* [kcmoduleqml] Corregeix el marge superior per als KCM en QML

### KConfig

* Cerca la dependència de Qt5DBus que manca
* kconf_update: Permet proves repetides en «--testmode» ignorant «kconf_updaterc»

### KConfigWidgets

* Canvia «http:» a «https:»

### KContacts

* Corregeix l'error 428276 - KContacts no pot ser usat en cap projecte QMake (error 428276)

### KCoreAddons

* KJob: afegeix «setProgressUnit()», per triar com es calcularà el tant per cent
* Corregeix una fuita de memòria potencial a KAboutData::registerPluginData
* Divideix «suggestName()»; el mètode de divisió no comprova si el fitxer existeix
* KAboutData: fa obsolets «pluginData()» i «registerPluginData()»
* No surt del cicle d'esdeveniments a KJobTest::slotResult()
* Usa la sobrecàrrega basada en un functor de «singleShot()» a TestJob::start()

### KDeclarative

* [abstractkcm] Estableix un farciment explícit
* [simplekcm] Elimina la gestió del farciment personalitzat
* [kcmcontrols] Elimina codi duplicat
* Afegeix un origen a KDeclarativeMouseEvent
* Tornar a emparentar «overlaysheets» a l'arrel
* Fa que GridViewKCM i ScrollVieKCM heretin d'AbstractKCM
* Afegeix el mètode «getter» per a les «subPages»

### KDocTools

* Corregeix el format XML a «contributor.entities»
* Actualització del coreà: torna a donar format als fitxers HTML de la GPL, la FDL i afegeix la LGPL

### KFileMetaData

* [ExtractionResult] Restaura la compatibilitat binària
* [TaglibWriter|Extractor] Elimina el tipus MIME «raw speex»
* [TaglibWriter] Obre la lectura-escriptura també en el Windows
* [Extractor|WriterCollection] Filtra els fitxers no biblioteques
* [EmbeddedImageData] Intenta treballar al voltant de l'estupidesa del MSVC
* [ExtractionResult] Fa obsolet ExtractEverything, des de llavors s'ha solucionat
* [EmbeddedImageData] Llegeix només una vegada la imatge real de base per la prova
* [EmbeddedImageData] Elimina la implementació privada d'escriptura de la coberta
* [EmbeddedImageData] Mou la implementació d'escriptura al connector d'escriptura de la Taglib
* [EmbeddedImageData] Elimina la implementació privada d'extracció de la coberta
* [EmbeddedImageData] Mou la implementació al connector d'extracció de la Taglib

### KGlobalAccel

* Activació del DBus del Systemd

### KIconThemes

* Manté la relació d'aspecte en pujar l'escalat

### KIdleTime

* Fa obsolet l'argument únic del senyal KIdleTime::timeoutReached(int identificador)

### KImageFormats

* Afegeix la implementació per als fitxers PSD comprimits amb RLE, de 16 bits per canal
* Retorna no suportat en llegir fitxers PSD comprimits amb RLE de 16 bits
* Fet: afegeix la implementació del format de profunditat de color == 16 dels PSD

### KIO

* Compara condicionalment amb un QUrl blanc en lloc de / en el Windows per a «mkpathjob»
* KDirModel: dues correccions a QAbstractItemModelTester
* CopyJob: inclou els fitxers omesos en el càlcul del progrés en reanomenar
* CopyJob: no compta els fitxers omesos a la notificació (error 417034)
* Als diàlegs de fitxer, selecciona un directori existent en intentar crear-lo
* CopyJob: corregeix el nombre total de fitxers/directoris en el diàleg de progrés (en moure)
* Fa que FileJob::write() es comporti amb coherència
* Permet els «xattrs» en copiar/moure del KIO
* CopyJob: no compta les mides dels directoris dins la mida total
* KNewFileMenu: corregeix una fallada per usar «m_text» en lloc de «m_lineEdit->text()»
* FileWidget: mostra la vista prèvia del fitxer seleccionat quan el ratolí surt (error 418655)
* Exposa el camp d'ajuda contextual d'usuari en el «kpasswdserver»
* KNewFileMenu: usa NameFinderJob per aconseguir un nom de «Carpeta nova»
* S'incorpora NameFinderJob que suggereix noms de «Carpeta nova»
* No defineix explícitament les línies Exec per als KCM (error 398803)
* KNewFileMenu: divideix el codi de creació del diàleg a un mètode separat
* KNewFileMenu: comprova que el fitxer encara no existeixi amb un retard per millorar la usabilitat
* [PreviewJob] Assigna memòria suficient per al segment SHM (error 427865)
* Usa el mecanisme de versionat per afegir llocs nous per als usuaris existents
* Afegeix adreces d'interès per a imatges, música i vídeos (error 427876)
* kfilewidget: manté el text al quadre Nom en navegar (error 418711)
* Gestiona els KCM en OpenUrlJob amb l'API del KService
* Canonicalitza el camí dels fitxers en recuperar i crear miniatures
* KFilePlacesItem: oculta els muntatges «sshfs» del KDE Connect
* OpenFileManagerWindowJob: tria correctament la finestra des del treball principal
* Evita proves sense sentit per a les imatges de miniatures inexistents
* [BUG] Corregeix una regressió en seleccionar fitxers que contenen «#»
* KFileWidget: fa que els botons de zoom de les icones saltin a la mida estàndard més propera
* Situa «minimumkeepsize» realment en el KCM «netpref» (error 419987)
* KDirOperator: simplifica la lògica del control lliscant de zoom de les icones
* UDSEntry: documenta el format esperat de l'hora per a les claus horàries
* kurlnavigatortest: elimina «desktop:», cal «desktop.protocol» per funcionar
* KFilePlacesViewTest: no mostra la finestra, no cal
* OpenFileManagerWindowJob: corregeix una fallada en activar la reserva a l'estratègia del KRun (error 426282)
* Paraules clau d'Internet: corregeix una fallada i les proves que fallen si el delimitador és un espai
* Prefereix els «bangs» del DuckDuckGo abans que altres delimitadors
* KFilePlacesModel: ignora els llocs ocults en calcular «closestItem» (error 426690)
* SlaveBase: documenta el comportament d'ERR_FILE_ALREADY_EXIST amb «copy()»
* kio_trash: corregeix la lògica quan no s'ha definit cap límit de mida (error 426704)
* Als diàlegs de fitxer, crear un directori que ja existeix l'hauria de seleccionar
* KFileItemActions: afegeix una propietat per a comptadors mín/màx dels URL

### Kirigami

* [avatar]: Fa que els nombres siguin noms no vàlids
* [avatar]: Exposa la propietat memòria cau d'imatge
* També defineix una «maximumWidth» per a les icones al «drawer» global (error 428658)
* Fa que la drecera sortir sigui una acció i l'exposa com una propietat de només lectura
* Usa cursors de mà a ListItemDragHandle (error 421544)
* [controls/avatar]: permet els noms CJK per a les inicials
* Millora l'aspecte del FormLayout en el mòbil
* Corregeix els menús a «contextualActions»
* No altera Item en el codi cridat des del destructor d'Item (error 428481)
* No modifica l'altra disposició «reversetwins»
* Estableix/elimina el focus a l'OverlaySheet en obrir/tancar
* Només arrossega la finestra per la barra d'eines global quan es prem i s'arrossega
* Tanca OverlaySheet en prémer la tecla Esc
* Page: fa que funcionin les propietats «padding», «horizontalPadding» i «verticalPadding»
* ApplicationItem: Usa la propietat «background»
* AbstractApplicationItem: afegeix les propietats que manquen i el comportament des d'ApplicationWindow del QQC2
* Limita l'amplada dels elements a l'amplada de la disposició
* Corregeix el botó enrere que no es mostra a les capçaleres de la pàgina amb disposició en el mòbil
* Silencia l'avís quant a un bucle de vinculació «checkable» a ActionToolBar
* Intercanvia l'ordre de les columnes en disposicions RTL
* Solució temporal per assegurar que es crida «ungrabmouse» cada vegada
* Comprova l'existència de «startSystemMove»
* Corregeix el separador en disposicions emmirallades
* Arrossega la finestra en fer clic en àrees buides
* No es desplaça arrossegant amb el ratolí
* Corregeix els casos quan la resposta és nul·la
* Corregeix una refactorització defectuosa de Forward/BackButton.qml
* Assegura que la icona buida està «Ready» i no es pinta com la icona anterior
* Restringeix l'alçada dels botons enrere/endavant a PageRowGlobalToolBarUI
* Silencia la brossa de ContextDrawer a la consola
* Silencia la brossa d'ApplicationHeader a la consola
* Silencia la brossa del botó enrere/endavant a la consola
* Evita que l'arrossegament del ratolí arrossegui un OverlaySheet
* Elimina el prefix de la biblioteca en la compilació per al Windows
* Corregeix la gestió de l'alineació de «twinformlayouts»
* Millora la llegibilitat del QML incrustat en el codi del C++

### KItemModels

* KRearrangeColumnsProxyModel: corregeix una fallada sense model d'origen
* KRearrangeColumnsProxyModel: només té fills la columna 0

### KNewStuff

* Corregeix una lògica errònia introduïda a «e1917b6a»
* Corregeix una fallada de supressió doble a «kpackagejob» (error 427910)
* Fa obsolet Button::setButtonText() i corregeix la documentació de l'API, no s'anteposa res
* Posposa totes les escriptures de la memòria cau en disc fins que hi hagi un segon de repòs
* Soluciona una fallada quan la llista de fitxers instal·lats és buida

### KNotification

* Marca KNotification::activated() com a obsolet
* Aplica diverses comprovacions de sanejament a les tecles d'acció (error 427717)
* Usa FindGradle des de l'ECM
* Corregeix la condició per usar DBus
* Correcció: habilita la safata antiga en les plataformes sense DBus
* Reescriu «notifybysnore» per a proporcionar un suport més fiable per al Windows
* Afegeix comentaris per a descriure el camp DesktopEntry al fitxer «notifyrc»

### Framework del KPackage

* Fa que l'avís «no metadata» sigui només de depuració

### KPty

* Elimina la compatibilitat amb l'AIX, Tru64, Solaris, Irix

### KRunner

* Fa obsolets els mètodes de RunnerSyntax
* Fa obsolets «ignoreTypes» i «RunnerContext::Type»
* No estableix el tipus a File/Directory si no existeix (error 342876)
* Actualitza el mantenidor segons es va debatre a la llista de correu
* Fa obsolet un constructor sense ús per al RunnerManager
* Fa obsoleta la funcionalitat de les categories
* Elimina una comprovació no necessària si se suspèn l'executor
* Fa obsolets els mètodes «defaultSyntax» i «setDefaultSyntax»
* Neteja l'ús difunt de RunnerSyntax

### KService

* Permet «NotShowIn=KDE apps», llistat a «mimeapps.list», que s'usi (error 427469)
* Escriu el valor de reserva per a les línies KCM Exec amb l'executable apropiat (error 398803)

### KTextEditor

* [EmulatedCommandBar::switchToMode] No fa res quan els modes nou i antic són el mateix (error 368130 com segueix:)
* KateModeMenuList: elimina els marges especials per al Windows
* Esmena una fuita de memòria a KateMessageLayout
* Intenta evitar l'esborrat d'estils personalitzats per als ressaltats que no s'han modificat per res (error 427654)

### KWayland

* Proporciona mètodes de comoditat per «wl_data_offet_accept()»
* Marca les enumeracions en un Q_OBJECT, Q_ENUM

### KWidgetsAddons

* «setUsernameContextHelp» nou a KPasswordDialog
* KFontRequester: elimina, l'ara redundant, «helper» «nearestExistingFont»

### KWindowSystem

* xcb: corregeix la detecció de les mides de pantalla d'alta densitat de PPP

### NetworkManagerQt

* Afegeix una enumeració i declaracions per permetre passar capacitats en el procés de registre al NetworkManager

### Frameworks del Plasma

* Component BasicPlasmoidHeading
* Sempre mostra els botons ExpandableListitem, no només en passar-hi per sobre (error 428624)
* [PlasmoidHeading]: Estableix adequadament un amidament implícit
* Bloca els colors de la capçalera de Brisa fosca i Brisa clara (error 427864)
* Unifica la relació d'aspecte de les icones de bateria de 32px i 22px
* Afegeix consells de marge a «toolbar.svg» i refactoritza la ToolBar de PC3
* Afegeix AbstractButton i Pane a PC3
* Permet grups d'acció exclusius a les accions contextuals
* Corregeix la rotació de BusyIndicator inclús quan és invisible, un altre cop
* Corregeix els colors que no s'apliquen a la icona del commutador de tasques del mòbil
* Afegeix les icones del commutador de tasques del Plasma Mobile i de tancar l'aplicació (per al plafó de tasques)
* Menú millor a PlasmaComponents3
* Elimina les àncores innecessàries en el ComboBox.contentItem
* Arrodoneix la posició de la maneta del control lliscant
* [ExpandableListItem] Carrega la vista expandida a demanda
* Afegeix un «PlasmaCore.ColorScope.inherit: false» que manca
* Estableix el «colorGroup» de PlasmoidHeading a l'element arrel
* [ExpandableListItem] Fa 100% opac el text acolorit (error 427171)
* BusyIndicator: no gira si és invisible (error 426746)
* ComboBox3.contentItem cal que sigui un QQuickTextInput per a corregir la compleció automàtica (error 424076)
* FrameSvg: No reinicia la memòria cau en redimensionar
* Commuta els plasmoides quan s'activa la drecera (error 400278)
* TextField 3: afegeix un «import» que manca
* Corregeix els ID a la icona «plasmavault_error»
* PC3: corregeix el color de l'etiqueta TabButton
* Usa un «hint» en lloc d'un booleà
* Permet que els plasmoides ignorin els marges

### Purpose

* Afegeix la descripció al proveïdor YouTube del «kaccounts»

### QQC2StyleBridge

* Corregeix un bucle de vinculació de «contentWidth» a ToolBar
* Referencia directament l'etiqueta de la drecera per ID en lloc d'implícitament
* ComboBox.contentItem cal que sigui un QQuickTextInput per a corregir la compleció automàtica (error 425865)
* Simplifica les clàusules condicionals a Connections
* Corregeix un avís de Connections a ComboBox
* Afegeix la implementació per les icones «qrc» a StyleItem (error 427449)
* Indica apropiadament l'estat del focus de ToolButton
* Afegeix TextFieldContextMenu per al menú contextual del clic dret a TextField i TextArea
* Estableix el color de fons al ScrollView de ComboBox

### Solid

* Afegeix la implementació per «sshfs» al dorsal de «fstab»
* CMake: usa «pkg_search_module» en cercar la «plist»
* Corregeix el dorsal d'«imobiledevice»: comprova la versió de l'API per a DEVICE_PAIRED
* Corregeix la construcció del dorsal «imobiledevice»
* Afegeix el dorsal del Solid usant la «libimobiledevice» per a cercar dispositius iOS
* Usa QHash per al mapatge d'on no cal ordre

### Sonnet

* Usa una sintaxi de connexió moderna de senyal-sòcol

### Ressaltat de la sintaxi

* Mancava la funció central «compact»
* Comenta la comprovació, afegeix un comentari perquè aquí ja no funciona això
* Mancava el valor de «position:sticky»
* Corregeix la generació de php/* per al ressaltat nou de comentaris
* Funcionalitat: substitueix la sintaxi «Alerts w/ Special-Comments» i elimina Modelines
* Funcionalitat: afegeix el «comments.xml» com a sintaxi paraigües per a diverses classes de comentaris
* Correcció: la sintaxi del CMake ara marca «1» i «0» com a valors booleans especials
* Millora: inclou les regles de Modeline en els fitxers a on s'ha afegit Alerts
* Millora: afegeix més valors booleans diversos a «cmake.xml»
* Temes Solarized: millora el separador
* Millora: actualitzacions per al CMake 3.19
* Afegeix la implementació per als fitxers d'unitats del Systemd
* Debchangelog: afegeix Hirsute Hippo
* Funcionalitat: permet opcions «-s» múltiples per a l'eina «kateschema2theme»
* Millora: afegeix diverses proves al convertidor
* Mou més scripts d'utilitats a una ubicació millor
* Mou l'script «update-kate-editor-org.pl» a un lloc millor
* kateschema2theme: afegeix una eina en Python per a convertir fitxers antics d'esquema
* Disminueix l'opacitat en el separador dels temes Breeze i Dracula
* Actualitza el README amb la secció «Color themes files»
* Correcció: Usa «KDE_INSTALL_DATADIR» en instal·lar fitxers de sintaxi
* Corregeix la renderització de «--syntax-trace=region» amb gràfics múltiples en el mateix desplaçament
* Corregeix diversos problemes de l'intèrpret del «fish»
* Substitueix StringDetect per DetectChar / Detect2Chars
* Substitueix diverses RegExpr per StringDetect
* Substitueix «RegExpr="." + lookAhead» per «fallthroughContext»
* Substitueix \s* per DetectSpaces

### Informació de seguretat

El codi publicat s'ha signat amb GPG usant la clau següent: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org> Empremta digital de la clau primària: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
