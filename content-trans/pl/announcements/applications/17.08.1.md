---
aliases:
- ../announce-applications-17.08.1
changelog: true
date: 2017-09-07
description: KDE wydało Aplikacje KDE 17.08.1
layout: application
title: KDE wydało Aplikacje KDE 17.08.1
version: 17.08.1
---
7 wrzesień 2017. Dzisiaj KDE wydało pierwsze uaktualnienie stabilizujące <a href='../17.08.0'>Aplikacji KDE 17.08</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

More than 20 recorded bugfixes include improvements to Kontact, Gwenview, Kdenlive, Konsole, KWalletManager, Okular, Umbrello, KDE games, among others.

To wydanie zawiera także długoterminowo wspieraną wersję Platformy Programistycznej KDE 4.14.36.
