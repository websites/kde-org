---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: O KDE Lança as Aplicações do KDE 18.08.0
layout: application
title: O KDE Lança as Aplicações do KDE 18.08.0
version: 18.08.0
---
16 de Agosto de 2018. O KDE anuncia hoje o lançamento das Aplicações do KDE 18.08.0.

Trabalhamos de forma contínua na melhoria do 'software' incluído na nossa série de Aplicações do KDE, esperando que ache as novas melhorias e correcções de erros úteis!

### O que há de novo nas Aplicações do KDE 18.08

#### Sistema

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

O <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, o poderoso gestor de ficheiros do KDE, oferece muitas melhorias no seu funcionamento:

- A janela de 'Configuração' foi remodelada para seguir as nossas normas de desenho e ser mais intuitiva.
- Foram eliminadas várias fugas de memória que poderiam tornar o seu computador mais lento.
- Os itens de menu 'Criar um Novo' já não ficam mais disponíveis quando estiver a ver o caixote do lixo.
- A aplicação agora adapta-se melhor aos ecrãs de alta resolução.
- O menu de contexto agora inclui mais opções úteis, permitindo-lhe ordenar e mudar directamente o modo de visualização.
- A ordenação pela data de modificação agora é 12 vezes mais rápida. Da mesma forma, agora pode lançar o Dolphin de novo quando se autenticar com a conta do administrador ('root'). O suporte para modificar os ficheiros pertencentes ao 'root' ao executar o Dolphin com um utilizador normal ainda é um trabalho em curso.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Estão disponíveis diversas melhorias para o <a href='https://www.kde.org/applications/system/konsole/'>Konsole</a>, a aplicação de emulação de terminal do KDE:

- O elemento para 'Procurar' agora aparece no topo da janela, não interrompendo assim o seu trabalho.
- For adicionado o suporte para mais sequências de escape (DECSCUSR & Modo de Deslocamento Alternativo do XTerm).
- Poderá agora atribuir qualquer carácter como tecla para um atalho.

#### Gráficos

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

A versão 18.08 é uma versão importante para o <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, o visualizador e organizador de imagens do KDE. Ao longo dos últimos meses, os colaboradores trabalham num grande conjunto de melhorias, onde se destacam:

- A barra de estado do Gwenview agora possui um contador de imagens e mostra o número total de imagens.
- Agora é possível ordenar pela classificação e por ordem descendente. A ordenação por data agora separa as pastas e pacotes e corrigiu alguns problemas em certas situações.
- Foi melhorado o suporte para arrastar-e-largar, de forma a permitir arrastar ficheiros e pastas para o modo de Visualização, para as mostrar, assim como arrastar os itens visualizados para aplicações externas.
- A colagem de ficheiros copiados do Gwenview agora também funciona para as aplicações que só aceitam dados de imagens em bruto, mas não os nomes de ficheiros. Agora também é suportada a cópia das imagens modificadas.
- A janela de dimensionamento das imagens foi remodelada para uma melhor usabilidade e para adicionar uma opção para dimensionar as imagens com base numa percentagem.
- A ferramenta de Redução de Olhos Vermelhos viu a sua barra de tamanho e o cursor em mira corrigidos.
- A selecção do fundo transparente agora tem uma opção para 'Nenhum' e pode também ser configurada para os SVG's.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

A ampliação da imagem ficou mais conveniente:

- Activou-se a ampliação por deslocamento ou por 'click', assim como o deslocamento, quando as ferramentas de Recorte ou Redução de Olhos Vermelhos.
- O botão do meio do rato volta mais uma vez a alternar entre a ampliação para caber e a ampliação a 100%.
- Foram adicionados os atalhos Shift-botão-do-meio e o Shift+F para comutar a ampliação para Preencher.
- O Ctrl-botão-do-rato agora amplia de forma mais rápida e fiável.
- O Gwenview agora amplia para a posição actual do cursor nos modos para Ampliar/Reduzir, Ampliar para Preencher e a 100%, quando usar o rato e os atalhos de teclado.

O modo de comparação de imagens recebeu bastantes melhorias:

- Foi corrigido o tamanho e o alinhamento do realce da selecção.
- Foram corrigidos os SVG's que se sobrepunham ao realce da selecção.
- Para as imagens SVG pequenas, o realce da selecção corresponde ao tamanho da imagem.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Algumas melhorias menores foram introduzidas para tornar o trabalho dos utilizadores ainda mais agradável:

- Foram melhoradas as transições de desvanecimento entre as imagens de tamanhos e transparências variados.
- Foi corrigida a visibilidade dos ícones em alguns botões flutuantes, quando é usado um esquema de cores claro.
- Ao gravar uma imagem com um novo nome, o visualizador já não salta mais para uma imagem não relacionada.
- Quando carregar no botão de partilha e os 'plugins' do Kipi não estiverem instalado, o Gwenview irá perguntar ao utilizador se os deseja instalar. Após a instalação, os mesmos são apresentados imediatamente.
- A barra lateral agora deixa de ficar escondida sem querer ao dimensionar a janela, e recorda a sua largura.

#### Escritório

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

O <a href='https://www.kde.org/applications/internet/kmail/'>KMail</a>, o cliente de e-mail poderoso do KDE, recebeu algumas melhorias no motor de extracção de dados de viagens. Agora suporta os códigos de barras de bilhetes de comboio nos formatos UIC 918.3 e SNCF, a pesquisa dos locais das estações de comboios com base no Wikidata. Foi adicionado o suporte para itinerários multi-viajante e o KMail agora tem a integração com a aplicação de Itinerários do KDE.

O <a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, a plataforma de gestão de informações pessoais, ficou agora mais rápido devido aos conteúdos das notificações e suporta o XOAUTH para o SMTP, permitindo a autenticação nativa com o GMail.

#### Educação

O <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, a interface do KDE para aplicações matemáticas, agora grava o estado dos painéis (\"Variáveis\", \"Ajuda\", etc.) para cada sessão em separado. As sessões do Julia tornaram-se muito mais rápidas de criar.

A experiência do utilizador no <a href='https://www.kde.org/applications/education/kalgebra/'>KAlgebra</a>, a nossa calculadora gráfica, foi substancialmente melhorada para dispositivos tácteis.

#### Utilitários

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Os colaboradores do <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, a ferramenta de capturas do ecrã do KDE, focaram-se em melhorar o modo de Região Rectangular:

- No modo de Região Rectangular, agora existe uma lupa que o ajuda a desenhar um rectângulo de selecção com precisão ao pixel.
- Pode agora mover e dimensionar o rectângulo da selecção com o teclado.
- A interface do utilizador segue o esquema de cores do utilizador e a apresentação do texto de ajuda foi melhorada.

Para simplificar a partilha das suas capturas do ecrã com outros, são agora copiadas automaticamente ligações das imagens partilhadas para a área de transferência. As mesmas podem agora ser gravadas automaticamente em sub-pastas definidas pelo utilizador.

O <a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, o nosso gravador da Webcam, foi actualizado para evitar estoiros com as versões mais recentes do  GStreamer.

### Eliminação de Erros

Foram resolvidos mais de 120 erros nas aplicações, incluindo o pacote Kontact, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello e muito mais!

### Registo de Alterações Completo
