---
aliases:
- ../../kde-frameworks-5.46.0
date: 2018-05-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Evitar ciclos infinitos ao obter o URL do DocumentUrlDB (erro 393181)
- Adição de sinais DBus do Baloo para os ficheiros movidos ou removidos
- Instalação do ficheiro .pri para o suporte para o 'qmake' &amp; documentação do mesmo no 'metainfo.yaml'
- baloodb: Adição do comando 'clean'
- balooshow: Colorir apenas quando estiver associado ao terminal
- Remoção do FSUtils::getDirectoryFileSystem
- Evitar fixar os sistemas de ficheiros que suportam CoW
- Permitir a desactivação do CoW quando não for suportada pelo sistema de ficheiros
- databasesanitizer: Uso de opções para filtragem
- Correcção da junção de termos no AdvancedQueryParser
- Uso do QStorageInfo em vez de uma implementação 'caseira'
- sanitizer: Melhoria na listagem dos dispositivos
- Aplicação imediata do 'termInConstruction' quando o termo estiver completo
- Tratamento correcto dos caracteres especiais adjacentes (erro 392620)
- Adição de caso de testes para o processamento de parêntesis de abertura duplos '((' (erro 392620)
- Uso consistente do 'statbuf'

### Ícones do Brisa

- Adição do ícone da bandeja 'plasma-browser-integration'
- Adição do ícone do 'virt-manager', graças ao 'ndavis'
- Adição do 'video-card-inactive'
- 'overflow-menu' como 'view-more-symbolic' na horizontal
- Uso do ícone mais apropriado das "duas barras" para "configurar"

### Módulos Extra do CMake

- Inclusão do FeatureSummary antes de invocar o 'set_package_properties'
- Não instalar os 'plugins' dentro de uma biblioteca no Android
- Possibilidade de compilar vários APK's de um mesmo projecto
- Verificação se o pacote 'androiddeployqt' da aplicação tem um símbolo 'main()'

### KCompletion

- [KLineEdit] Uso da funcionalidade do botão de limpeza incorporado no Qt
- Correcção do KCompletionBox no Wayland

### KCoreAddons

- [KUser] Verificar se o ficheiro '.face.icon' é de facto legível antes de o devolver
- Tornar os sinais do KJob públicos para que a sintaxe do 'connect' do Qt5 possa funcionar

### KDeclarative

- Carregar o reinício dos gráficos NV com base na configuração
- [KUserProxy] Ajustes para o serviço de contas (erro 384107)
- Optimizações do Plasma para dispositivos móveis
- Criação de espaço para o cabeçalho e rodapé
- nova política de dimensionamento (erro 391910)
- suporte para a visibilidade das acções
- Suporte para as notificações de reinício na NVidia nas QtQuickViews

### KDED

- Adição da detecção de plataformas e ajustes ao 'kded' (configuração automática do $QT_QPA_PLATFORM)

### KFileMetaData

- Adição de descrição e objectivo para a dependência do Xattr
- extracções: Esconder os avisos dos ficheiros de inclusão do sistema
- correcção da detecção da 'taglib' ao compilar para o Android
- Instalação do ficheiro .pri para o suporte para o 'qmake' &amp; documentação do mesmo no 'metainfo.yaml'
- Fazer com que os textos concatenados possam mudar de linha
- ffmpegextractor: Silenciar os avisos de desactualização
- taglibextractor: Correcção de erro com géneros vazios
- tratamento de mais marcas no 'taglibextractor'

### KHolidays

- holidays/plan2/holiday_sk_sk - correcção do Dia do Professor (erro 393245)

### KI18n

- [Documentação da API] Novo marcador de UI @info:placeholder
- [Documentação da API] Novo marcador de UI @info:valuesuffix
- Não é mais necessário executar os comandos das iterações anteriores de novo (erro 393141)

### KImageFormats

- [Carregamento do XCF/GIMP] Elevação do tamanho máximo de imagem permitido para 32767x32767 nas plataformas a 64 bits (erro 391970)

### KIO

- Escala suavizadas das miniaturas no selector de ficheiros (erro 345578)
- KFileWidget: Alinhamento perfeito do nome do ficheiro com a área de ícones
- KFileWidget: Gravação da largura do painel de locais também após esconder o mesmo
- KFileWidget: Evitar que a largura do painel de locais cresça de forma iterativa em 1px
- KFileWidget: Desactivação dos botões de ampliação assim que tiverem atingido o máximo ou o mínimo
- KFileWidget: Definição do tamanho mínimo para a barra de ampliação
- Não seleccionar a extensão do ficheiro
- concatPaths: processamento correcto do 'path1' vazio
- Melhoria na disposição dos ícones em grelha na janela de selecção de ficheiros (erro 334099)
- Esconder o KUrlNavigatorProtocolCombo se só for suportado um protocolo
- Só mostrar os esquemas suportados no KUrlNavigatorProtocolCombo
- O selector de ficheiros lê a antevisão das miniaturas a partir da configuração do Dolphin (erro 318493)
- Adição da Área de Trabalho e das Transferências à lista de Locais predefinida
- O KRecentDocument agora guarda o 'QGuiApplication::desktopFileName' em vez do 'applicationName'
- [KUrlNavigatorButton] Não tentar executar o 'stat' também no MTP
- getxattr takes 6 parameters in macOS (bug 393304)
- Adição de um item de menu "Recarregar" ao menu de contexto do KDirOperator (erro 199994)
- Gravação da configuração da área da janela mesmo ao cancelar (erro 209559)
- [KFileWidget] Fixar o nome do utilizador de exemplo
- Não mostrar a opção da aplicação de topo "Abrir Com" para as pastas; só para os ficheiros
- Detecção de parâmetros incorrectos no 'findProtocol'
- Uso do texto "Outra Aplicação..." no submenu "Abrir Com"
- Codificação correcta do URL das miniaturas (erro 393015)
- Ajuste das larguras das colunas na vista em árvore das janelas para abrir/gravar ficheiros (erro 96638)

### Kirigami

- Não avisar quando tentar usar o Page {} fora de uma 'pageStack'
- Remodelação do InlineMessages para resolver um conjunto de problemas
- correcção no Qt 5.11
- basear-se em unidades para o tamanho dos botões de ferramentas
- coloração do ícone de fecho à passagem do cursor
- mostrar uma margem debaixo do rodapé quando necessário
- correcção do 'isMobile'
- também desvanecer ao animar a abertura/fecho
- inclusão dos elementos do 'dbus' apenas no UNIX e não no Android ou Apple
- monitorização do 'tabletMode' do KWin
- no modo de computador, mostrar as acções à passagem do cursor (erro 364383)
- tratamento da barra de ferramentas de topo
- uso de um botão de fecho cinzento
- menos dependências da 'applicationwindow'
- menos avisos de 'sem uma applicationwindow'
- funcionamento correcto sem a 'applicationWindow'
- Não ter um tamanho não-inteiro nos separadores
- Não mostrar as acções se estiverem desactivadas
- itens do FormLayout assinaláveis
- uso de ícones diferentes no exemplo de alteração de cores
- inclusão dos ícones apenas no Android
- capacidade de compilar com o Qt 5.7

### KNewStuff

- Correcção das margens duplas à volta do DownloadDialog
- Correcção de sugestões nos ficheiros UI sobre as sub-classes dos elementos gráficos personalizados
- Não oferecer um 'plugin' de QML como um destino de ligação

### Plataforma KPackage

- uso do KDE_INSTALL_DATADIR em vez do FULL_DATADIR
- Adição do URL's de doações aos dados de testes

### KPeople

- Correcção da filtragem do PersonSortFilterProxyModel

### Kross

- Tornar também opcional a instalação da documentação traduzida

### KRunner

- Suporte para caracteres especiais no nome do serviço de execução do DBus

### KTextEditor

- optimização do KTextEditor::DocumentPrivate::views()
- [ktexteditor] 'positionFromCursor' muito mais rápido
- Implementação de 'click' simples no número de linha para seleccionar uma linha de texto
- Correcção da formatação em negrito/itálico/... nas versões mais recentes do Qt (&gt;= 5.9)

### Plataforma do Plasma

- Correcção da marcação do evento não visível no calendário com os temas Air &amp; Oxygen
- Uso do "Configurar o %1..." para o texto da acção para configurar uma 'applet'
- [Estilos de Botões] Preencher a altura e alinhar na vertical (erro 393388)
- adição do ícone 'video-card-inactive' para a bandeja do sistema
- correcção da aparência dos botões planos
- [ContainmentInterface] Não entrar no modo de edição se for imutável
- garantia de que o 'largespacing' é um múltiplo perfeito do 'small'
- invocação do 'addContainment' com parâmetros adequados
- Não mostrar o fundo se a opção 'Button.flat' estiver activa
- garantia de que o contentor que foi criado tem a actividade pedida
- adição de um 'containmentForScreen' versionado com a actividade
- Não alterar a gestão de memória para esconder um item (erro 391642)

### Purpose

- Garantia de que existe algum espaço vertical nos 'plugins' de configuração
- Migração da configuração dos 'plugins' do KDEConnect para o QQC2
- Migração do AlternativesView para o QQC2

### QQC2StyleBridge

- exportação das margens de disposição do 'qstyle', a começar pelo Control
- [ComboBox] Correcção do tratamento da roda do rato
- fazer com que o 'mousearea' não interfira com o 'controls'
- correcção do 'acceptableInput'

### Solid

- Actualização do ponto de montagem após as operações de montagem (erro 370975)
- Invalidação da 'cache' de propriedades quando é removida uma interface
- Evitar a criação de itens de propriedades duplicadas na 'cache'
- [UDisks] Optimização de diversas validações de propriedades
- [UDisks] Correcção do tratamento dos sistemas de ficheiros removíveis (erro 389479)

### Sonnet

- Correcção da activação/desactivação do botão de remoção
- Correcção da activação/desactivação do botão de adição
- Pesquisa nas sub-pastas por dicionários

### Realce de Sintaxe

- Actualização do URL do projecto
- O 'Headline' é um comentário, como tal baseá-lo 'dsComment'
- Adição do realce de sintaxe dos ficheiros de listagens de comandos do GDB e do 'gdbinit'
- Adição do realce de sintaxe do Logcat

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Poderá discutir e partilhar ideias sobre esta versão na secção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-'>artigo do Dot</a>.
