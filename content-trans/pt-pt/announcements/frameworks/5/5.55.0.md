---
aliases:
- ../../kde-frameworks-5.55.0
date: 2019-02-09
layout: framework
libCount: 70
---
### Baloo

- [tags_kio] Desactivação do acesso com um URL com barras duplas, i.e. "tags://" (erro 400594)
- Criação de instância de QApplication antes do KCrash/KCatalog
- Ignorar todos os eventos 'deviceAdded' sem capacidades de armazenamento do Solid
- Uso do mais adequado K_PLUGIN_CLASS_WITH_JSON
- Remoção das verificações do Qt 5.1, já que é agora a versão mínima obrigatório

### Ícones do Brisa

- Adição de ícone do KCM de Cursores
- Adição e mudança de nome de alguns ícones e ligações simbólicas do YaST
- Melhoria do ícone da Campainha de notificação, usando o desenho do KAlarm (erro 400570)
- Adição do 'yast-upgrade'
- Melhoria dos ícones do 'weather-storm-*' (erro 403830)
- Adição de ligações simbólicas 'font-otf', como acontece nas ligações 'font-ttf'
- Adição de ícones 'edit-delete-shred' adequados (erro 109241)
- Adição de ícones para recorte das margens e da selecção (erro 401489)
- Remoção das ligações simbólicas do 'edit-delete-shred', como preparação para terem ícones próprios
- Mudança de nome do ícone do KCM de Actividades
- Adição do ícone do KCM de Actividades
- Adição do ícone de Ecrãs Virtuais do KCM
- Adição de ícones para os KCM's de Ecrãs Tácteis e Extremos do Ecrã
- Correcção dos nomes dos ícones relacionados com as preferências de partilha de ficheiros
- Adição de um ícone 'preferences-desktop-effects'
- Adição de um ícone de Preferências do Tema do Plasma
- Melhoria no contraste do 'preferences-system-time' (erro 390800)
- Adição de um novo ícone 'preferences-desktop-theme-global'
- Adição do ícone de ferramentas
- O ícone 'document-new' agora segue o 'ColorScheme-Text'
- adição de um ícone 'preferences-system-splash' para o KCM do Ecrã Inicial
- Inclusão do 'applets/22'
- Adição dos ícones de tipos MIME do Kotlin (.kt)
- Melhoria no ícone 'preferences-desktop-cryptography'
- Preenchimento do cadeado no 'preferences-desktop-user-password'
- Preenchimento consistente do cadeado no ícone de 'encriptado'
- Uso de um ícone do Kile que seja semelhante ao original

### Módulos Extra do CMake

- FindGperf: no 'ecm_gperf_generate', definir o SKIP_AUTOMOC para o ficheiro gerado
- Passagem do '-Wsuggest-override -Wlogical-op' para as configurações normais do compilador
- Correcção da geração de interfaces para as classes com construtores por cópia removidos
- Correcção da geração de módulos com QMake para o Qt 5.12.1
- Uso de mais HTTPS nas hiperligações
- Documentação da API: adição de elementos em falta para alguns módulos &amp; módulos de pesquisa
- FindGperf: melhoria da documentação da API: exemplo de utilização da formatação
- ECMGenerateQmlTypes: correcção da documentação da API: o título precisa de mais formatação '---'
- ECMQMLModules: correcção da documentação da API: título correspondente ao nome do módulo, adição de elementos 'Desde' em falta
- FindInotify: correcção da marca '.rst' da documentação da API, adição de elemento "Desde" em falta

### KActivities

- correcção do macOS

### KArchive

- Poupança de duas chamadas KFilterDev::compressionTypeForMimeType

### KAuth

- Remoção do suporte para a passagem de QVariant's da GUI aos utilitários do KAuth

### KBookmarks

- Compilação sem o D-Bus no Android
- Definição de constantes

### KCMUtils

- [kcmutils] Adição de reticências aos textos de pesquisa no KPluginSelector

### KCodecs

- nsSJISProber::HandleData: Não estoirar se o 'aLen' for igual a 0

### KConfig

- kconfig_compiler: remoção do operador de atribuição e construtor por cópia

### KConfigWidgets

- Compilação sem o KAuth e o D-Bus no Android
- Adição do KLanguageName

### KCrash

- Comentário de justificação por que é necessário o 'ptracer'
- [KCrash] Estabelecimento de 'socket' para permitir a mudança do 'ptracer'

### KDeclarative

- [Grelha de Controlos do KCM] Adição de animação na remoção

### Suporte para a KDELibs 4

- Correcção de algumas bandeiras de países para usarem toda a imagem

### KDESU

- tratamento de senhas erradas ao usar o 'sudo', o qual pede por outra senha (erro 389049)

### KFileMetaData

- exiv2extractor: adição do suporte para .bmp, .gif, .webp e .tga
- Correcção de teste em falha nos dados de GPS do EXIV
- Teste de dados de GPS vazios e nulos
- adição do suporte para mais tipos MIME no 'taglibwriter'

### KHolidays

- holidays/plan2/holiday_ua_uk - actualização para 2019

### KHTML

- Adição de meta-dados JSON ao executável do 'plugin' de 'khtmlpart'

### KIconThemes

- Compilação sem o D-Bus no Android

### KImageFormats

- xcf: Rectificação da correcção da opacidade quando fora dos limites
- Remoção dos comentários das inclusões do 'qdebug'
- tga: Correcção do 'Uso de valor não inicializado' nos ficheiros com problemas
- A opacidade máxima é igual a 255
- xcf: Correcção de verificação de ficheiros com dois PROP_COLORMAP
- ras: Correcção de verificação de um ColorMapLength demasiado grande
- pcx: Correcção de estoiro em ficheiro difuso
- xcf: Implementação mais robusta quando o PROP_APPLY_MASK não consta no ficheiro
- xcf: loadHierarchy: Uso do 'layer.type' e não do 'bpp'
- tga: Não suportar mais do que 8 'bits' no canal alfa
- ras: Devolver um valor 'false' se a alocação da imagem for mal-sucedida
- rgb: Correcção de ultrapassagem de valores inteiros em ficheiros difusos
- rgb: Correcção de esgotamento da memória de dados com um ficheiro difuso
- psd: Correcção de estoiro em ficheiros difusos
- xcf: Inicialização do 'x/y_offset'
- rgb: Correcção de estoiro em imagens difusas
- pcx: Correcção de estoiro em imagens difusas
- rgb: correcção de estoiro em ficheiros difusos
- xcf: inicialização do modo da camada
- xcf: inicialização da opacidade da camada
- xcf: configuração do 'buffer' como 0 se forem lidos menos dados que os esperados
- bzero -&gt; memset
- Correcção de diversas leituras e escritas OOB no 'kimg_tga' e 'kimg_xcf'
- pic: dimensionamento do ID do cabeçalho se não conseguir ler 4 'bytes' como seria de esperar
- xcf: limpeza do 'buffer' com 'bzero' se ler menos dados que os esperados
- xcf: Só invocar o 'setDotsPerMeterX/Y' se existir o PROP_RESOLUTION
- xcf: inicialização do 'num_colors'
- xcf: Inicialização da propriedade de visibilidade da camada
- xcf: Não converter um 'int' para 'enum' se não puder associar esse valor inteiro
- xcf: Não ultrapassar os limites do 'int' na chamada 'setDotsPerMeterX/Y'

### KInit

- KLauncher: tratamento da saída sem erros dos processos (erro 389678)

### KIO

- Melhoria dos controlos do teclado no elemento gráfico do código de validação
- Adição de função utilitária para desactivar os redireccionamentos (útil para o 'kde-open')
- Reversão do "Remodelação do SlaveInterface::calcSpeed" (erro 402665)
- Não configurar a política CMP0028 do CMake como antiga. Não temos alvos com '::' a menos que sejam importados.
- [kio] Adição de reticências ao texto de pesquisa na secção de 'Cookies'
- [KNewFileMenu] Não emitir um 'fileCreated' quando criar uma pasta (erro 403100)
- Uso (e sugestão do uso) do mais adequado K_PLUGIN_CLASS_WITH_JSON
- evitar o bloqueio do 'kio_http_cache_cleaner' e garantir uma saída com a sessão (erro 367575)
- Correcção do teste do 'knewfilemenu' em falta e da razão subjacente à sua falha

### Kirigami

- elevar apenas os botões coloridos (erro 403807)
- correcção da altura
- mesma política de dimensionamento das margens que a dos outros itens da lista
- operadores ===
- suporte para o conceito de itens expansíveis
- não limpar ao substituir todas as páginas
- o preenchimento horizontal é o dobro do vertical
- ter o preenchimento em conta ao calcular as sugestões de tamanho
- [kirigami] Não usar estilos de texto claros para os cabeçalhos (2/3) (erro 402730)
- Correcção do formato do AboutPage em dispositivos mais pequenos
- O 'stopatBounds' para o 'breadcrumb' era intermitente

### KItemViews

- [kitemviews] Mudança da pesquisa nas Actividades/Comportamento do Ecrã para estar mais alinhado com os outros textos de pesquisa

### KJS

- Definição do SKIP_AUTOMOC para alguns ficheiros gerados, por compatibilidade com o CMP0071

### KNewStuff

- Correcção da semântica do 'ghns_exclude' (erro 402888)

### KNotification

- Correcção de fuga de memória ao passar os dados do ícone ao Java
- Remoção de dependência da biblioteca de suporte AndroidX
- Adição do suporte para canais de notificações no Android
- Reposição do funcionamento das notificações no Android com nível da API &lt; 23
- Passagem das verificações do nível da API do Android para a fase de execução
- Remoção de declaração posterior não-usada
- Recompilação do AAR quando o código de Java for alterado
- Compilação do código de Java com o Gradle como AAR em vez de JAR
- Não basear-se na integração da área de trabalho do Plasma no Android
- Pesquisa pela configuração do evento de notificação nos recursos .qrc

### Plataforma KPackage

- Reposição do funcionamento das traduções

### KPty

- Correcção de falta de correspondência da estrutura/classe

### KRunner

- Remoção do uso explícito do ECM_KDE_MODULE_DIR, já que faz parte do ECM_MODULE_PATH

### KService

- Compilação sem o D-Bus no Android
- Sugestão às pessoas para usarem o K_PLUGIN_CLASS_WITH_JSON

### KTextEditor

- O Qt 5.12.0 tem problemas na implementação do 'regex' (expressões regulares) no QJSEngine, onde os módulos de indentação se podem comportar incorrectamente; o Qt 5.12.1 irá ter uma correcção
- KateSpellCheckDialog: Remoção da acção "Selecção da Verificação Ortográfica"
- Actualização da biblioteca de JavaScript 'underscore.js' para a versão 1.9.1
- Correcção do erro 403422: Possibilidade de mudar de novo o tamanho do marcador (erro 403422)
- Barra de Pesquisa: Adição de botão Cancelar' para parar tarefas em execução há muito tempo (erro 244424)
- Remoção do uso explícito do ECM_KDE_MODULE_DIR, já que faz parte do ECM_MODULE_PATH
- Revisão do KateGotoBar
- ViewInternal: Correcção do 'Ir para o parêntesis recto correspondente' no modo de substituição (erro 402594)
- Uso de HTTPS, se disponível, nas ligações visíveis aos utilizadores
- Revisão do KateStatusBar
- ViewConfig: Adição de opção para colar na posição do cursor, usando o rato (erro 363492)
- Uso do mais adequado K_PLUGIN_CLASS_WITH_JSON

### KWayland

- [servidor] gerar ID's de toque correctos
- Mudanças de conformidade do XdgTest com a especificação
- Adição de opção para usar o 'wl_display_add_socket_auto'
- [servidor] Envio do 'org_kde_plasma_virtual_desktop_management.rows' inicial
- Adição de informações das linhas ao protocolo de ecrãs virtuais do Plasma
- [cliente] Interface para o 'wl_shell_surface_set_{class,title}'
- Guarda da remoção de recursos no OuptutConfiguration::sendApplied

### KWidgetsAddons

- [KWidgetsAddons] Não usar estilos de texto claros nos cabeçalhos (3/3) (erro 402730)

### KXMLGUI

- Compilação sem o D-Bus no Android
- Tornar os KCheckAccelerators menos invasivos nas aplicações que não se associam directamente ao KXmlGui

### ModemManagerQt

- Correcção da implementação do operador &gt;&gt; do QVariantMapList

### Plataforma do Plasma

- [Modelos de papel de parede] Adição de elemento 'Comment=' no ficheiro .desktop
- Partilha das instâncias do Plasma::Theme entre vários elementos ColorScope
- Transformação das sombras em SVG do relógio mais correctas do ponto de vista lógico e apropriadas visualmente (erro 396612)
- [plataformas] Não usar tipos de letra claros nos cabeçalhos (1/3) (erro 402730)
- [Janela Dialog] Não alterar a visibilidade do 'mainItem'
- Reposição do 'parentItem' quando muda o 'mainItem'

### Purpose

- Uso do mais adequado K_PLUGIN_CLASS_WITH_JSON

### QQC2StyleBridge

- Correcção do tamanho inicial das listas pendentes 'combobox' (erro 403736)
- Mudança das áreas das listas pendentes 'combobox' para modais (erro 403403)
- Reversão parcial do '4f00b0cabc1230fdf'
- Mudança de linha das dicas compridas (erro 396385)
- ComboBox: correcção do método delegado predefinido
- Geração de evento 'mousehover' falso enquanto a lista 'combobox' está aberta
- Configuração da propriedade 'QStyleOptionState == On' do ComboBox em vez de 'Sunken' para corresponder aos 'qwidgets' (erro 403153)
- Correcção do ComboBox
- Desenhar sempre a dica em cima de tudo o resto
- Suporte de dicas sobre uma MouseArea
- não obrigar à apresentação do texto no ToolButton

### Solid

- Compilação sem o D-Bus no Android

### Sonnet

- Não invocar este código se só tivermos espaço

### Realce de Sintaxe

- Correcção do fim da região de dobragem nas regras com 'lookAhead=true'
- AsciiDoc: Correcção do realce da directiva 'include'
- Adição do suporte para AsciiDoc
- Correcção de erro que gerava um ciclo infinito no realce de ficheiros do KConfig
- verificar as mudanças de contexto intermináveis
- Ruby: Correcção da expressão regular após o ": " e correcção/melhoria da detecção do HEREDOC (erro 358273)
- Haskell: Transformação do '=' num símbolo especial

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
