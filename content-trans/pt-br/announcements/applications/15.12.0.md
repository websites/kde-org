---
aliases:
- ../announce-applications-15.12.0
changelog: true
date: 2015-12-16
description: O KDE Lança as Aplicações do KDE 15.12.
layout: application
release: applications-15.12.0
title: O KDE disponibiliza o KDE Applications 15.12.0
version: 15.12.0
---
16 de dezembro de 2015. Hoje o KDE lançou o KDE Applications 15.12.

A equipe do KDE está entusiasmada com o lançamento do KDE Applications 15.12, a atualização de dezembro de 2015 para o KDE Applications. Esta versão traz novos aplicativos e funcionalidades, assim como correções de erros nos aplicativos existentes. Nos esforçamos para que seu ambiente de trabalho e aplicativos tenham a melhor qualidade possível. Por esse motivo, contamos com suas críticas e sugestões.

Nesta versão, foi actualizado um grande número de aplicações para usarem as novas <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Plataformas do KDE 5</a>, incluindo o <a href='https://edu.kde.org/applications/all/artikulate/'>Artikulate</a>, o <a href='https://www.kde.org/applications/system/krfb/'>Krfb</a>, o <a href='https://www.kde.org/applications/system/ksystemlog/'>KSystemLog</a>, o <a href='https://games.kde.org/game.php?game=klickety'>Klickety</a> e mais alguns Jogos do KDE, para além da Interface de 'Plugins' de Imagens do KDE e das suas bibliotecas de suporte. Isto aumenta o número total de aplicações que usam as Plataformas do KDE 5 para 126.

### Uma nova adição "Espetacular"

Depois de 14 anos integrando as versões do KDE, o KSnapshot foi descontinuado e substituído por um novo aplicativo para obter capturas de tela, o Spectacle.

Com novas funcionalidades e com uma interface completamente nova, o Spectacle torna a captura de tela o mais simples e discreto possível. Além do que já podia ser feito com o KSnapshot, com o Spectacle você poderá obter capturas de tela compostas de menus <i>pop-up</i> em conjunto com suas janelas-mãe ou, então, imagens da tela inteira (ou da janela que estiver ativa) sem precisar iniciar o Spectacle, usando as combinações de teclas Shift+PrintScreen e Meta+PrintScreen, respectivamente.

Também melhoramos muito o tempo para início do aplicativo, de forma a minimizar o atraso na captura da tela.

### Melhorias para todo lado

Muitos aplicativos tiveram diversas melhorias com pequenos ajustes neste ciclo, além de algumas correções de erro e de estabilidade.

O <a href='https://userbase.kde.org/Kdenlive'>Kdenlive</a>, o editor de vídeo não-linear, viu diversas correcções importantes na sua interface de utilizador. Poderá agora copiar e colar itens na sua linha temporal, podendo também activar/desactivar a transparência de uma dada faixa. As cores dos ícones ajustam-se automaticamente ao tema principal da interface, tornando-se mais fáceis de ver.

{{<figure src="/announcements/applications/15.12.0/ark1512.png" class="text-center" width="600px" caption=`O Ark agora pode mostrar os comentários de arquivos ZIP`>}}

O <a href='https://www.kde.org/applications/utilities/ark/'>Ark</a>, o gestor de pacotes, consegue agora mostrar os comentários incorporados nos pacotes ZIP e RAR. Foi melhorado o suporte para caracteres Unicode nos nomes dos ficheiros nos pacotes ZIP, podendo agora detectar os pacotes danificados e recuperar o máximo de dados que for possível deles. Também poderá abrir os ficheiros arquivados no pacote nas suas aplicações predefinidas; foi também corrigido o modo de arrastamento para o ecrã, assim como as antevisões dos ficheiros XML.

### Diversão e nada de trabalho

{{<figure src="/announcements/applications/15.12.0/ksudoku1512.png" class="text-center" width="600px" caption=`Nova tela de boas-vindas do KSudoku (no Mac OS X)`>}}

Os programadores do KDE Games trabalharam bastante nos últimos meses para otimizar nossos jogos, proporcionando uma experiência mais suave e rica. Foram adicionadas diversas novidades nesta área, para diversão dos nossos usuários.

No <a href='https://www.kde.org/applications/games/kgoldrunner/'>KGoldrunner</a>, foram adicionados dois novos conjuntos de níveis, um que permite cavar enquanto se cai e outro sem essa possibilidade. Foram adicionadas soluções para os diversos conjuntos de níveis existentes. Para um desafio acrescido, agora foi desactivada a escavação em queda para alguns conjuntos de níveis mais antigos.

No <a href='https://www.kde.org/applications/games/ksudoku/'>KSudoku</a>, poderá agora imprimir 'puzzles' do Matedoku e do Sudoku Assassino. A nova disposição multi-colunas no Ecrã de Boas-Vindas do KSudoku torna mais fácil de ver mais sobre os diversos tipos de 'puzzles' disponíveis, sendo que as colunas se ajustam automaticamente à medida que a janela muda de tamanho. Foi feito o mesmo ao Palapeli, tornando mais fácil de ver mais sobre a sua colecção de 'puzzles' de uma vez.

Também foram incluídas correcções de estabilidade para jogos como o KNavalBattle, o Klickety, o <a href='https://www.kde.org/applications/games/kshisen/'>KShisen</a> e o <a href='https://www.kde.org/applications/games/ktuberling/'>KTuberling</a>, sendo que a experiência global ficou bastante melhor agora. O KTuberling, o Klickety e o KNavalBattle também foram actualizados para usar as novas Plataformas do KDE 5.

### Correções importantes

Não detesta quando a sua aplicação favorita estoira na altura mais inconveniente? Nós também e, para tentar remediar isso, trabalhámos com afinco para corrigir uma grande quantidade de erros para si, mas infelizmente poderão continuar a existir alguns menos óbvios, pelo que não se esqueça de <a href='https://bugs.kde.org'>os comunicar</a>!

No nosso gestor de ficheiros <a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, foram incluídas diversas correcções de estabilidade, bem como algumas correcções para tornar o deslocamento mais suave. No <a href='https://www.kde.org/applications/education/kanagram/'>Kanagram</a>, foi corrigido um problema incómodo com o texto branco sobre fundos brancos. No <a href='https://www.kde.org/applications/utilities/kate/'>Kate</a>, tentou-se corrigir um estoiro que estava a ocorrer no encerramento, para além da arrumação da interface e da adição de uma nova limpeza da 'cache'.

O <a href='https://userbase.kde.org/Kontact'>Pacote Kontact</a> viu diversas funcionalidades adicionadas, correcções de erros e optimizações de performance. De facto, existiu tanto desenvolvimento neste ciclo que avançámos o número da versão para 5.1. A equipa está a trabalhar ao máximo, e esta à espera de todas as suas reacções.

### Avanços

Como parte do esforço para modernizar o que oferecemos, removemos alguns aplicativos para o lançamento do KDE Applications 15.12.

Foram removidos 4 aplicativos desta versão - Amor, KTux, KSnapshot e SuperKaramba. Como dito acima, o KSnapshot foi substituído pelo Spectacle e o Plasma substitui, na sua essência, o SuperKaramba como mecanismo de controle de widgets. Foram removidos alguns protetores de tela independentes, porque o bloqueio da tela é tratado de forma muito diferente nos ambientes de trabalho modernos.

Também foram removidos 3 pacotes de arte gráfica (kde-base-artwork, kde-wallpapers e kdeartwork); faz muito tempo que o seu conteúdo não era alterado.

### Registro completo das alterações
