---
aliases:
- ../../kde-frameworks-5.47.0
date: 2018-06-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Terminar previamente a execução da pesquisa se o sub-termo devolver um conjunto vazio de resultados
- Evitar um estoiro ao ler dados corrompidos da BD de termos do documento (erro 392877)
- tratamento das listas de textos como dados de entrada
- Ignorar mais tipos de ficheiros de código (erro 382117)

### Ícones Breeze

- actualização do 'handles' e do 'overflow-menu'

### Módulos extra do CMake

- Ferramentas do Android: permitir definir bibliotecas-extra manualmente
- Android: Não definir o 'qml-import-paths' se estiver vazio

### KArchive

- tratamento dos ficheiros ZIP incorporados dentro de ficheiros ZIP (erro 73821)

### KCMUtils

- [KCModuleQml] Ignorar os controlos desactivados ao circular com o Tab

### KConfig

- kcfg.xsd - não obrigar ao uso de um elemento 'kcfgfile'

### KConfigWidgets

- Correcção do esquema de cores "Predefinição" para corresponder de novo ao Brisa

### KDeclarative

- Definição da propriedade 'context' do KCM para o contexto correcto
- [Gráficos] Não desenhar se o 'm_node' for nulo (erro 394283)

### KDocTools

- Actualização da lista de entidades Ucranianas
- adição da entidade OSD ao 'general.entities'
- adição das entidades CIFS, NFS, Samba, SMB ao 'general.entities'
- Adição do Falkon, Kirigami, macOS, Solid, USB, Wayland, X11, SDDM às entidade gerais

### KFileMetaData

- validação se a versão do 'ffmpeg' é pelo menos a 3.1, que introduz a API necessária por nós
- pesquisa pelas marcas 'album', 'artist' e 'albumartist' no 'taglibextractor'
- popplerextractor: não tentar adivinhar o título se o mesmo não existir

### KGlobalAccel

- Certificação de que o pedido de libertação do teclado é processado antes de emitir o atalho (erro 394689)

### KHolidays

- holiday_es_es - Correcção do dia da "Comunidade de Madrid"

### KIconThemes

- Validação se 'group &lt; LastGroup', dado que o KIconEffect não trata o UserGroup de qualquer forma

### KImageFormats

- Remoção dos tipos MIME duplicados nos ficheiros JSON

### KIO

- Validação se o destino existe também quando se colam dados binários (erro 394318)
- Suporte de autenticação: Devolve o tamanho actual do 'buffer' do 'socket'
- Suporte de autenticação: Unificação da API para a partilha de descritores de ficheiros
- Suporte de autenticação: Criação do ficheiro do 'socket' na pasta de execução do utilizador
- Suporte de autenticação: Remoção do ficheiro do 'socket' após o seu uso
- Suporte de autenticação: Mudança da tarefa de limpeza do ficheiro do 'socket' para o FdReceiver
- Suporte de autenticação: No Linux, não usar um 'socket' abstracto para partilhar o descritor do ficheiro
- [kcoredirlister] Remoção de tantos url.toString() quantos possíveis
- KFileItemActions: uso por omissão do tipo MIME predefinido ao seleccionar apenas ficheiros (erro 393710)
- Introdução do KFileItemListProperties::isFile()
- O KPropertiesDialogPlugin consegue agora definir vários protocolos suportados, usando o X-KDE-Protocols
- Preservação do fragmento ao redireccionar de HTTP para HTTPS
- [KUrlNavigator] Emissão de 'tabRequested' quando o selector de locais é carregado com o botão do meio
- Performance: uso da nova implementação do UDS
- Não direccionar o smb:/ para o smb:// e depois para o smb:///
- Permitir a aceitação do duplo-click na janela de gravação (erro 267749)
- Activar a antevisão por omissão na janela de selecção de ficheiros
- Esconder a antevisão do ficheiro quando o ícone é demasiado pequeno
- i18n: uso da forma plural de novo para a mensagem do 'plugin'
- Uso de uma janela normal em vez de uma janela de listas ao enviar um único ficheiro para o lixo ou ao removê-lo
- Fazer com que o texto de aviso nas operações de remoção dê ênfase à sua permanência e irreversibilidade
- Reverter a funcionalidade "Mostrar os botões do modo de visualização na barra da janela para abrir/gravar"

### Kirigami

- Mostrar o 'action.main' de forma mais proeminente no ToolBarApplicationHeader
- Possibilidade de o Kirigami compilar sem a dependência do modo de 'tablet' do KWin
- correcção do 'swipefilter' no RTL
- correcção do dimensionamento do 'contentItem'
- correcção do comportamento do '--reverse'
- partilha do 'contextobject' para aceder sempre ao 'i18n'
- validação de que a dica está escondida
- validação de que não são atribuídas variantes inválidas às propriedades acompanhadas
- não tratar um sinal dropped() do MouseArea
- sem efeitos à passagem em ecrãs móveis
- ícones adequados de 'overflow-menu-left' e 'overflow-menu-right'
- Pega de arrastamento para reordenar os itens numa ListView
- Uso de Mnemónicas nos botões da barra de ferramentas
- Adição de ficheiros em falta no .pri do QMake
- [Documentação da API] Correcção do Kirigami.InlineMessageType -&gt; Kirigami.MessageType
- correcção do 'applicationheaders' no 'applicationitem'
- Não permitir mostrar/esconder a gaveta sem existir uma pega (erro 393776)

### KItemModels

- KConcatenateRowsProxyModel: tratamento adequado dos dados de entrada

### KNotification

- Correcção dos estoiros no NotifyByAudio ao fechar as aplicações

### Plataforma KPackage

- kpackage*install**package: correcção de dependência em falta entre o .desktop e o .json
- validação de que as localizações no 'rcc' nunca derivam de localizações absolutas

### KRunner

- Processamento das respostas do DBus na tarefa ::match (erro 394272)

### KTextEditor

- Não usar a capitalização de títulos na opção "mostrar o número de palavras"
- Tornar o número de palavras/caracteres uma preferência global

### KWayland

- Aumento do número de versão da interface org_kde_plasma_shell
- Adição do "SkipSwitcher" à API
- Adição do Protocolo de Saída do XDG

### KWidgetsAddons

- [KCharSelect] Correcção do tamanho das células da tabela no Qt 5.11
- [Documentação da API] Remoção do uso do 'overload', resultando em documentação inválida
- [Documentação da API] Indicação ao doxygen que o "e.g." (p.ex.) não termina a frase, usando o ". "
- [Documentação da API] Remoção da formatação HTML desnecessária
- Não atribuir automaticamente os ícones predefinidos para cada estilo
- Fazer com que o KMessageWidget corresponda ao estilo inlineMessage do Kirigami (erro 381255)

### NetworkManagerQt

- Fazer da informação sobre propriedades não tratadas apenas uma mensagem de depuração
- WirelessSetting: implementação da propriedade 'assignedMacAddress'

### Plasma Framework

- Modelos: nomenclatura consistente, correcção dos nomes dos catálogos de traduções &etc;
- [Tema Brisa do Plasma] Correcção do ícone do Kleopatra para usar a folha de estilo de cores (erro 394400)
- [Janela] Tratamento ordeiro da janela a ser minimizada (erro 381242)

### Purpose

- Melhoria na integração do Telegram
- Tratamento das listas internas como restrições OU em vez de AND
- Possibilidade de restrição dos 'plugins' pela presença de um ficheiro 'desktop'
- Possibilidade de filtrar os 'plugins' pelo executável
- Realce do dispositivo seleccionado no 'plugin' do KDE Connect
- correcção de questões de 'i18n' no frameworks/purpose/plugins
- Adição do 'plugin' do Telegram
- kdeconnect: Notificação quando não é possível iniciar o processo (erro 389765)

### QQC2StyleBridge

- Uso da propriedade 'pallet' apenas quando usar o qtquickcontrols 2.4
- Funcionamento com o Qt&lt;5.10
- Correcção da altura da barra de páginas
- Uso do Control.palette
- [RadioButton] Mudança de nome de "control" para "controlRoot"
- Não definir um espaço explícito no RadioButton/CheckBox
- [FocusRect] Uso de colocação manual em vez de âncoras
- Acontece que o 'flickable' numa 'scrollview' é o 'contentItem'
- Mostrar o rectângulo de foco quando o CheckBox ou o RadioButton estão em primeiro plano
- correcção alternativa à detecção do 'scrollview'
- Não mudar o elemento-pai do 'flickable' para a 'mousearea'
- [Barra de Páginas] Mudança de páginas com a roda do rato
- O controlo não pode ter elementos-filhos (erro 394134)
- Restrição do deslocamento (erro 393992)

### Realce de sintaxe

- Perl6: Adição do suporte para as extensões .pl6, .p6, ou .pm6 (erro 392468)
- DoxygenLua: correcção do fecho dos blocos de comentários (erro 394184)
- Adição do 'pgf' aos formatos de ficheiros do tipo 'latex' (mesmo formato que o 'tikz')
- Adição das palavras-chave do 'postgresql'
- Realce de sintaxe do OpenSCAD
- debchangelog: adição do Cosmic Cuttlefish
- cmake: Correcção do aviso do DetectChar sobre a barra invertida escapada
- Pony: correcção do identificador e palavra-chave
- Lua: actualização para o Lua5.3

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
